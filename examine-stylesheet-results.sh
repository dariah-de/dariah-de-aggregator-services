#!/bin/bash
cat <<EOP
This debugging script assists in the progress that is neccessary after
stylesheet updates: It runs the html regression test to dump generated HTMLs to
the hard disk, generates canonicalized and formatted versions of them, and
allows you to compare the committed versions with the newly generated ones. 

EOP


mkdir -p target/diffable-html/{old,new,new-raw}
echo "Running the regression test (may take a while) ..."
if mvn test -Dtest=HtmlRegressionTest > target/diffable-html/mvn.log 2>&1
then
  echo ':-) Regression test succeeded -- you can probably ^C now ...'
else
  echo ':-( Test failed -- probably there are differences in the generated HTML'
fi

cp target/surefire-reports/info.textgrid.services.aggregator.html.HtmlRegressionTest.txt target/diffable-html/HtmlRegressionTest.txt
cat target/diffable-html/HtmlRegressionTest.txt

echo "Generating the new HTMLs now (may take a while) ..."
mvn test -Dtest=HtmlRegressionTest -Dregression.outputdir=target/diffable-html/new-raw > target/diffable-html/mvn2.log 2>&1
for f in src/test/resources/*.html
do
  xmlstarlet c14n --without-comments $f | xmlstarlet fo > target/diffable-html/old/`basename $f`
done
for f in target/diffable-html/new-raw/*.html
do
  xmlstarlet c14n --without-comments $f | xmlstarlet fo > target/diffable-html/new/`basename $f`
done
  cat <<EOP

Canonicalized, formatted HTML test files have been generated in subdirectories
of target/diffable-html:
 - old: As present in src/test/resources/*.html
 - new: As generated from the current version of the stylesheet
 - new-raw: Not canonicalized and formatted, i.e. ready for inclusion in the next commit.
EOP

cd target/diffable-html

choice=0
while [ $choice != x ]
do
  cat <<EOP

What do you want to do?
  (d)  view the diffs via colordiff
  (m)  view the diffs via meld
  (t)  view the test report with xmldiff information
  (l)  view the maven log
  (a)  accept the changes, i.e. copy stuff to src/test/resources

  (x)  exit this tool
EOP
read -p"dmtax > " -n1 choice

case $choice in
  D|d)
    colordiff old new | less -R
    ;;
  M|m)
    meld old new
    ;;
  T|t)
    echo "Running less ..."
    less HtmlRegressionTest.txt 
    ;;
  L|l)
    less mvn.log mvn2.log
    ;;
  A|a)

    for f in new-raw/*.html
    do
      if cmp -s old/`basename $f` new/`basename $f`
      then
	: # echo Skipping semantically unchanged file $f
      else
	cp -vp $f ../../src/test/resources
      fi
    done

    cd ../../
    git status
    echo You need to add and commit stuff now.
    exit 0
    ;;
esac

done
