<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" xmlns:xi="http://www.w3.org/2001/XInclude"
	xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tgs="http://www.textgrid.info/namespaces/middleware/tgsearch"
	exclude-result-prefixes="xs tei tgs xi"
	xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0"> 

<!-- 
	Contains common TextGrid specific TEI -> HTML conversions that are used for both
	HTML and EPUB output. Should not contain structural changes but rather formatting 
	stuff.	
-->
	
	<!-- subtler page references -->
	<xsl:template match="pb">
		<span class="pagebreak" title="{concat('Page ', @n)}">[<xsl:value-of select="@n"/>]</span>
	</xsl:template>
	
	<!-- milestones get rendered in 7.x!? -->
	<xsl:template match="milestone">
		<xsl:comment>milestone (<xsl:value-of select="@unit"/>) "<xsl:value-of select="@n"/>" #<xsl:value-of select="@xml:id"/></xsl:comment>
	</xsl:template>
	
        <!-- aus common-core.xsl, zur besseren Lesbarkeit Gedankenstrich zwischen aufeinanderfolgenden Überschriften eingefügt-->
        <doc xmlns="http://www.oxygenxml.com/ns/doc/xsl">
                <desc>Process element head in plain mode</desc>
        </doc>
        <xsl:template match="tei:head" mode="plain">
                <xsl:if test="preceding-sibling::tei:head">
                        <xsl:text> – </xsl:text>
                </xsl:if>
                <xsl:apply-templates mode="plain"/>
        </xsl:template>               
        
        
        <!-- 
                desc/title is used for headings from <lem> elements, which we suspected to be zeno.org navigation only.
                The contents is usually supressed in HTML output, however we construct an artificial title from it 
                iff its string contents is different from the first following head for cases where the original heading
                from the text has _not_ been explicitely encoded by zeno.org ... 
        -->
        <xsl:template match="desc">
                <xsl:choose>
                        <xsl:when test="normalize-space(.) = normalize-space(following::head[1])">
                                <xsl:comment>
                                        <xsl:apply-templates/>
                                </xsl:comment>                        
                        </xsl:when>
                        <xsl:otherwise>
                                <xsl:comment>unsupressed desc content</xsl:comment>
                                <xsl:apply-templates/>
                                <xsl:comment>/unsupressed desc content</xsl:comment>
                        </xsl:otherwise>
                </xsl:choose>
        </xsl:template>        
        <xsl:template match="desc/title">
                <xsl:param name="Depth" select="2 + count(ancestor::div)"/>
                <xsl:element name="h{if ($Depth > 6) then 6 else $Depth}">
                        <xsl:attribute name="class">desc</xsl:attribute>                   
                        <xsl:apply-templates/>
                </xsl:element>
        </xsl:template>
        

</xsl:stylesheet>
