<xsl:stylesheet xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:smil="http://www.w3.org/ns/SMIL"
	xmlns:opf="http://www.idpf.org/2007/opf" xmlns:iso="http://www.iso.org/ns/1.0"
	xmlns="http://www.w3.org/1999/xhtml" xmlns:html="http://www.w3.org/1999/xhtml"
	xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:teix="http://www.tei-c.org/ns/Examples"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:m="http://www.w3.org/1998/Math/MathML"
	xmlns:ncx="http://www.daisy.org/z3986/2005/ncx/" version="2.0"
	exclude-result-prefixes="iso tei teix dc html opf ncx smil xs html m">
	
	
	<xsl:import
		href="../tei-stylesheets/epub/tei-to-epub.xsl"/>
	<xsl:include href="tghtml-common.xsl"/>
	
	<!-- ************************* Variablen und Parameter ******************************* -->
	<xsl:variable name="numberHeadings">false</xsl:variable>
	<xsl:variable name="autoHead">true</xsl:variable>
	
	<xsl:param name="autoToc">false</xsl:param>
	<xsl:param name="publisher">TextGrid</xsl:param>
	
	<!-- aus html-param.xsl, hier angeben, da sonst der Fehler kommt, dass die Datei nicht gefunden wird -->
	<xsl:param name="cssFile" as="xs:string">../css/tei.css</xsl:param>
	<xsl:param name="cssPrintFile" as="xs:string">../css/epub-print.css</xsl:param> 
	
	<xsl:output indent="yes"/>
	
	
	<!-- aus common-param.xsl, Überschriften generieren, falls keine vorhanden sind -->
	<xsl:template name="autoMakeHead">
		<xsl:param name="display"/>
		<xsl:choose>
			<xsl:when test="tei:head and $display='full'">
				<xsl:apply-templates select="tei:head" mode="makeheading"/>
			</xsl:when>
			<xsl:when test="tei:head">
				<xsl:apply-templates select="tei:head" mode="plain"/>
			</xsl:when>
			<xsl:when test="tei:lg/tei:head">
				<xsl:apply-templates select="tei:lg/tei:head" mode="plain"/>
			</xsl:when>
			<xsl:when test="tei:front/tei:head">
				<xsl:apply-templates select="tei:front/tei:head" mode="plain"/>
				<xsl:text>.</xsl:text>
			</xsl:when>
			<!-- bei divs mit subtype:"work:no": title als überschrift anzeigen, Bsp. Szenen/Akte in Dramen -->
			<xsl:when test="tei:div[@subtype='work:no']">
				<xsl:value-of select="descendant::tei:title[1]/text()"/>
			</xsl:when>
			<xsl:when test="tei:div[@type='Dramatis_Personae']">
				<xsl:value-of select="descendant::tei:head[1]/text()"/>
			</xsl:when>
			<xsl:when test="tei:div[ancestor::tei:TEI//tei:titleStmt/tei:title ='[Widmung]']">
				<xsl:value-of select="ancestor::tei:TEI//tei:fileDesc/tei:titleStmt/tei:title"/>
			</xsl:when>
			<xsl:when test="tei:div[ancestor::tei:TEI//tei:titleStmt/tei:title ='[Motto]']">
				<xsl:value-of select="ancestor::tei:TEI//tei:fileDesc/tei:titleStmt/tei:title"/>
			</xsl:when>
			<xsl:when test="tei:lg">
				<xsl:value-of select="ancestor::tei:TEI//tei:fileDesc/tei:titleStmt/tei:title"/>
			</xsl:when>
			<!-- Die @ns enthalten Zenopfade -->
			<!-- <xsl:when test="@n">
                <xsl:value-of select="@n"/>
            </xsl:when> -->
			<!-- <xsl:when test="@type">
                <xsl:text>[</xsl:text>
                <xsl:value-of select="@type"/>
                <xsl:text>]</xsl:text>
            </xsl:when> -->
			<!-- Wenn wir irgendwo eine Überschrift finden, die nehmen -->
			<xsl:when test="descendant::tei:head and $display='plain'">				
				<xsl:apply-templates select="descendant::tei:head[1]" mode="plain"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:comment>OTHERWISE <xsl:value-of select="@xml:id"/></xsl:comment> 				
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- Aus common-linking.xsl. 
	
	Vorlage generiert bei unserer Gruppierung grade da, wo's in den ncx kommt, "[1]" als Überschrift 
	
	-->
	<xsl:template name="header">
		<xsl:param name="minimal">false</xsl:param>
		<xsl:param name="toc"/>
		<xsl:param name="display">full</xsl:param>
		<xsl:variable name="depth">
			<xsl:apply-templates mode="depth" select="."/>
		</xsl:variable>
		<xsl:variable name="headingNumber">
			<xsl:call-template name="formatHeadingNumber">
				<xsl:with-param name="toc">
					<xsl:value-of select="$toc"/>
				</xsl:with-param>
				<xsl:with-param name="text">
					<xsl:choose>
						<xsl:when test="local-name(.) = 'TEI'">
							<xsl:if test="@n">
								<xsl:value-of select="@n"/>
							</xsl:if>
						</xsl:when>
						<xsl:when test="$depth &gt; $numberHeadingsDepth"> </xsl:when>
						<xsl:when test="self::tei:text">
							<xsl:number/>
							<xsl:call-template name="headingNumberSuffix"/>
						</xsl:when>
						<xsl:when test="ancestor::tei:back">
							<xsl:if test="not($numberBackHeadings='')">
								<xsl:sequence select="tei:i18n('appendixWords')"/>
								<xsl:text> </xsl:text>
								<xsl:call-template name="numberBackDiv"/>
								<xsl:if test="$minimal='false'">
									<xsl:value-of select="$numberSpacer"/>
								</xsl:if>
							</xsl:if>
						</xsl:when>
						<xsl:when test="ancestor::tei:front">
							<xsl:if test="not($numberFrontHeadings='')">
								<xsl:call-template name="numberFrontDiv">
									<xsl:with-param name="minimal">
										<xsl:value-of select="$minimal"/>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:if>
						</xsl:when>
						<xsl:when test="$numberHeadings ='true'">
							<xsl:choose>
								<xsl:when test="$prenumberedHeadings='true'">
									<xsl:value-of select="@n"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="numberBodyDiv"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="$minimal='false'">
								<xsl:call-template name="headingNumberSuffix"/>
							</xsl:if>
						</xsl:when>
					</xsl:choose>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="$numberHeadings='true'">
			<xsl:copy-of select="$headingNumber"/>
		</xsl:if>
		<xsl:if test="$minimal='false'">
			<xsl:choose>
				<xsl:when test="local-name(.) = 'TEI'">
					<xsl:apply-templates select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[1]"/>
				</xsl:when>
				<xsl:when test="not($toc='')">
					<xsl:call-template name="makeInternalLink">
						<xsl:with-param name="dest">
							<xsl:value-of select="$toc"/>
						</xsl:with-param>
						<xsl:with-param name="class">
							<xsl:value-of select="$class_toc"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="($class_toc,$depth)"
								separator="_"/>
						</xsl:with-param>
						<xsl:with-param name="body">
							<xsl:choose>
								<xsl:when test="self::tei:text">
									<!-- Erzeugt [1] an der NCX-relevanten Stelle -->
									<!--<xsl:value-of select="if (@n) then @n else concat('[',position(),']')"/>-->
									<xsl:call-template name="autoMakeHead">
										<xsl:with-param name="display">plain</xsl:with-param>
									</xsl:call-template>
								</xsl:when>
								<xsl:when test="not(tei:head) and tei:front/tei:head">
									<xsl:apply-templates  mode="plain" select="tei:front/tei:head"/>
								</xsl:when>
								<xsl:when test="not(tei:head) and tei:body/tei:head">
									<xsl:apply-templates mode="plain" select="tei:body/tei:head"/>
								</xsl:when>
								<xsl:when test="not(tei:head) and tei:front//tei:titlePart/tei:title">
									<xsl:apply-templates mode="plain" select="tei:front//tei:titlePart/tei:title"/>
								</xsl:when>	
								<xsl:when test="tei:head and count(tei:head/*)=1 and tei:head/tei:figure">
									<xsl:text>[</xsl:text>
									<xsl:sequence select="tei:i18n('figureWord')"/>
									<xsl:text>]</xsl:text>
								</xsl:when>
								<xsl:when test="tei:head[not(.='')] and
									not(tei:head[count(*)=1 and
									tei:figure])">
									<xsl:apply-templates mode="plain" select="tei:head"/>
								</xsl:when>
								<xsl:when test="@type='title_page'">Title page</xsl:when>
								<xsl:when test="@type='index'">Index</xsl:when>
								<xsl:when test="@type='section'">§</xsl:when>
								<xsl:when test="$autoHead='true'">
									<xsl:call-template name="autoMakeHead">
										<xsl:with-param name="display" select="$display"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:number/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="tei:head and ($display='plain' or $display='simple')">
					<xsl:for-each select="tei:head">
						<xsl:apply-templates mode="plain"/>
					</xsl:for-each>
				</xsl:when>
				<xsl:when test="tei:head">
					<xsl:apply-templates mode="makeheading" select="tei:head"/>
				</xsl:when>
				<xsl:when test="tei:front/tei:head">
					<xsl:apply-templates  mode="plain" select="tei:front/tei:head"/>
				</xsl:when>
				<xsl:when test="tei:body/tei:head">
					<xsl:apply-templates mode="plain" select="tei:body/tei:head"/>
				</xsl:when>
				<xsl:when test="self::tei:index">
					<xsl:value-of select="substring(tei:term,1,10)"/>
					<xsl:text>…</xsl:text>
				</xsl:when>
				<xsl:when test="$autoHead='true'">
					<xsl:call-template name="autoMakeHead">
						<xsl:with-param name="display" select="$display"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="not(self::tei:div) and $headingNumber=''">
					<xsl:value-of select="(normalize-space(substring(.,1,20)),'…')"/>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	
	<!-- aus epub-common.xsl, Punkte zur besseren Lesbarkeit -->
	<xsl:template match="tei:title" mode="metadata" priority="99">
		<i>
			<xsl:if test="preceding-sibling::tei:title">
				<xsl:text>. </xsl:text>
			</xsl:if>
			<xsl:apply-templates/>
			<xsl:if test="not(following-sibling::tei:title)">
				<xsl:text>.</xsl:text>
			</xsl:if>
		</i>
	</xsl:template>
	
	<!-- bei mehreren <title>-Elementen Punkte als Trennzeichen einfügen, zur besseren Lesbarkeit -->
	<xsl:template match="tei:title" mode="simple">
		<xsl:if test="preceding-sibling::tei:title">
			<xsl:text>. </xsl:text>
		</xsl:if>
		<xsl:value-of select="."/>
		<xsl:if test="not(following-sibling::tei:title)">
			<xsl:text>.</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<!-- aus tei-to-epub.xsl, Anpassung der Metadaten (Dublin-Core-Header in content.opf, 
  korrekte Metadaten gewährleisten korrekte Sortierung o.ä. des Epub -->
	<xsl:template name="opfmetadata">
		<xsl:param name="author"/>
		<xsl:param name="printAuthor"/>
		<xsl:param name="coverImageOutside"/>
		<metadata xmlns="http://www.idpf.org/2007/opf" xmlns:dc="http://purl.org/dc/elements/1.1/"
			xmlns:dcterms="http://purl.org/dc/terms/"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:opf="http://www.idpf.org/2007/opf">
			<dc:title>
				<xsl:sequence select="tei:generateSimpleTitle(.)"/>
			</dc:title>
			<dc:language xsi:type="dcterms:RFC3066">
				<xsl:call-template name="generateLanguage"/>
			</dc:language>
			<xsl:call-template name="generateSubject"/>
			<dc:identifier id="dcidid" opf:scheme="URI">
				<xsl:call-template name="generateID"/>
			</dc:identifier>
			<dc:description>
				<xsl:sequence select="tei:generateSimpleTitle(.)"/>
				<xsl:text> / </xsl:text>
				<xsl:value-of select="$author"/>
			</dc:description>
			<dc:creator>
				<xsl:choose>
					<xsl:when test="$printAuthor=''">Anonymous/Unknown</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$printAuthor"/>
					</xsl:otherwise>
				</xsl:choose>
			</dc:creator>
			<dc:publisher>
				<xsl:sequence select="tei:generatePublisher(.,$publisher)"/>
			</dc:publisher>
			<xsl:for-each select="tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@notAfter]">
				<dc:date opf:event="creation">
					<xsl:value-of select="@notAfter"/>
				</dc:date>
			</xsl:for-each>
			<!--Attribut @when-iso statt @when -->
			<xsl:for-each select="tei:teiHeader/tei:fileDesc/tei:sourceDesc//tei:date[@when-iso][1]">
				<dc:date opf:event="original-publication">
					<xsl:value-of select="@when-iso"/>
				</dc:date>
			</xsl:for-each>
			<dc:date opf:event="epub-publication" xsi:type="dcterms:W3CDTF">
				<xsl:sequence select="tei:generateDate(.)"/>
			</dc:date>
			<dc:rights>
				<xsl:call-template name="generateLicence"/>
			</dc:rights>
			<xsl:if test="not($coverImageOutside='')">
				<meta name="cover" content="cover-image"/>
			</xsl:if>
		</metadata>
	</xsl:template>
	
	<xsl:template name="generateLanguage">
		<xsl:choose>
			<xsl:when test="@xml:lang">
				<xsl:value-of select="@xml:lang"/>
			</xsl:when>
			<xsl:when test="tei:text/@xml:lang">
				<xsl:value-of select="tei:text/@xml:lang"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>de</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	
	<!-- DEBUG ONLY
	<xsl:template name="processTEIHook">
		<xsl:result-document href="{$directory}/TOC.xml">
			<xsl:for-each select="*">
				<xsl:call-template name="mainTOC"/>
			</xsl:for-each>
		</xsl:result-document>
	</xsl:template>
	-->
	
</xsl:stylesheet>
