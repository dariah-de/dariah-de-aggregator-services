<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:smil="http://www.w3.org/ns/SMIL"
  xmlns:opf="http://www.idpf.org/2007/opf" xmlns:iso="http://www.iso.org/ns/1.0"
  xmlns="http://www.w3.org/1999/xhtml" xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:teix="http://www.tei-c.org/ns/Examples"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:ncx="http://www.daisy.org/z3986/2005/ncx/" version="2.0"
  exclude-result-prefixes="iso tei teix dc html opf ncx smil xs html m">

  
  <xsl:import
    href="../tei-stylesheets/epub/tei-to-epub.xsl"/>
	
  <xsl:import href="tghtml-common.xsl"/>

  <!-- ************************* Variablen und Parameter ******************************* -->
  <xsl:variable name="numberHeadings">false</xsl:variable>
  <xsl:variable name="autoHead">true</xsl:variable>

  <xsl:param name="autoToc">false</xsl:param>
  <xsl:param name="publisher">TextGrid</xsl:param>

  <!-- aus html-param.xsl, hier angeben, da sonst der Fehler kommt, dass die Datei nicht gefunden wird -->
  <xsl:param name="cssFile" as="xs:string">../tei-stylesheets/css/tei.css</xsl:param>
  <xsl:param name="cssPrintFile" as="xs:string">../tei-stylesheets/css/epub-print.css</xsl:param>
        
        <xsl:output indent="yes"/>

  <!-- aus common-param.xsl, Überschriften generieren, falls keine vorhanden sind -->
  <xsl:template name="autoMakeHead">
    <xsl:param name="display"/>
    <xsl:choose>
      <xsl:when test="tei:head and $display='full'">
        <xsl:apply-templates select="tei:head" mode="makeheading"/>
      </xsl:when>
      <xsl:when test="tei:head">
        <xsl:apply-templates select="tei:head" mode="plain"/>
      </xsl:when>
      <xsl:when test="tei:lg/tei:head">
        <xsl:apply-templates select="tei:lg/tei:head" mode="plain"/>
      </xsl:when>
      <xsl:when test="tei:front/tei:head">
        <xsl:apply-templates select="tei:front/tei:head" mode="plain"/>
        <xsl:text>.</xsl:text>
      </xsl:when>
      <!-- bei divs mit subtype:"work:no": title als überschrift anzeigen, Bsp. Szenen/Akte in Dramen -->
      <xsl:when test="tei:div[@subtype='work:no']">
        <xsl:value-of select="descendant::tei:title[1]/text()"/>
      </xsl:when>
      <xsl:when test="tei:div[@type='Dramatis_Personae']">
        <xsl:value-of select="descendant::tei:head[1]/text()"/>
      </xsl:when>
      <xsl:when test="tei:div[ancestor::tei:TEI//tei:titleStmt/tei:title ='[Widmung]']">
        <xsl:value-of select="ancestor::tei:TEI//tei:fileDesc/tei:titleStmt/tei:title"/>
      </xsl:when>
      <xsl:when test="tei:div[ancestor::tei:TEI//tei:titleStmt/tei:title ='[Motto]']">
        <xsl:value-of select="ancestor::tei:TEI//tei:fileDesc/tei:titleStmt/tei:title"/>
      </xsl:when>
      <xsl:when test="tei:lg">
        <xsl:value-of select="ancestor::tei:TEI//tei:fileDesc/tei:titleStmt/tei:title"/>
      </xsl:when>
      <!-- <xsl:when test="@n">
                <xsl:value-of select="@n"/>
            </xsl:when> -->
      <!-- <xsl:when test="@type">
                <xsl:text>[</xsl:text>
                <xsl:value-of select="@type"/>
                <xsl:text>]</xsl:text>
            </xsl:when> -->
      <xsl:otherwise>
        <xsl:text>OTHERWISE &#160;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <!-- aus epub-common.xsl, Punkte zur besseren Lesbarkeit -->
  <xsl:template match="tei:title" mode="metadata" priority="99">
    <i>
      <xsl:if test="preceding-sibling::tei:title">
        <xsl:text>. </xsl:text>
      </xsl:if>
      <xsl:apply-templates/>
      <xsl:if test="not(following-sibling::tei:title)">
        <xsl:text>.</xsl:text>
      </xsl:if>
    </i>
  </xsl:template>

  <!-- aus common-core.xsl, Anpassung der Markierungen für Seitenumbrüche, 'page' entfernen -->
  <xsl:template match="tei:pb">
    <xsl:choose>
      <xsl:when test="$filePerPage='true'">
        <PAGEBREAK>
          <xsl:attribute name="name">
            <xsl:apply-templates select="." mode="ident"/>
          </xsl:attribute>
          <xsl:copy-of select="@facs"/>
        </PAGEBREAK>
      </xsl:when>
      <xsl:when test="@facs and not(@rend='none')">
        <xsl:variable name="IMG">
          <xsl:choose>
            <xsl:when test="starts-with(@facs,'#')">
              <xsl:for-each select="id(substring(@facs,2))">
                <xsl:value-of select="tei:graphic[1]/@url"/>
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="@facs"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:element name="{if (tei:isInline(..)) then 'span' else 'div'}">
          <xsl:call-template name="makeRendition"/>
          <img src="{$IMG}" alt="page image"/>
        </xsl:element>
      </xsl:when>
      <xsl:when test="$pagebreakStyle='active'">
        <div>
          <xsl:call-template name="makeRendition">
            <xsl:with-param name="default">pagebreak</xsl:with-param>
          </xsl:call-template>
        </div>
      </xsl:when>
      <xsl:when
        test="$pagebreakStyle='visible' and (parent::tei:body         or parent::tei:front or parent::tei:back or parent::tei:group)">
        <div class="pagebreak">
          <xsl:call-template name="makeAnchor"/>
          <xsl:text> [</xsl:text>
          <!-- <xsl:call-template name="i18n">
            <xsl:with-param name="word">page</xsl:with-param>
          </xsl:call-template> -->
          <xsl:if test="@n">
            <xsl:text> </xsl:text>
            <xsl:value-of select="@n"/>
          </xsl:if>
          <xsl:text>] </xsl:text>
        </div>
      </xsl:when>
      <xsl:when test="$pagebreakStyle='visible'">
        <span class="pagebreak">
          <xsl:call-template name="makeAnchor"/>
          <xsl:text> [</xsl:text>
          <!--<xsl:call-template name="i18n">
            <xsl:with-param name="word">page</xsl:with-param>
          </xsl:call-template> -->
          <xsl:if test="@n">
            <xsl:value-of select="@n"/>
          </xsl:if>
          <xsl:text>] </xsl:text>
        </span>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- aus common-linking.xsl, kleine Anpassungen, damit keine Zeno-Pfade angezeigt werden -->
  <xsl:template name="header">
    <xsl:param name="minimal">false</xsl:param>
    <xsl:param name="toc"/>
    <xsl:param name="display">full</xsl:param>
    <xsl:variable name="depth">
      <xsl:apply-templates mode="depth" select="."/>
    </xsl:variable>
    <xsl:call-template name="formatHeadingNumber">
      <xsl:with-param name="toc">
        <xsl:value-of select="$toc"/>
      </xsl:with-param>
      <xsl:with-param name="text">
        <xsl:choose>
          <!-- rausnehmen, da sonst Zenopfade angezeigt werden
                       <xsl:when test="local-name(.) = 'TEI'">
                        <xsl:if test="@n">
                            <xsl:value-of select="@n"/>
                        </xsl:if>
                    </xsl:when> -->
          <xsl:when test="$depth &gt; $numberHeadingsDepth"> </xsl:when>
          <xsl:when test="self::tei:text">
            <xsl:number/>
            <xsl:call-template name="headingNumberSuffix"/>
          </xsl:when>
          <xsl:when test="ancestor::tei:back">
            <xsl:if test="$numberBackHeadings='true'">
              <xsl:call-template name="i18n">
                <xsl:with-param name="word">appendixWords</xsl:with-param>
              </xsl:call-template>
              <xsl:text> </xsl:text>
              <xsl:call-template name="numberBackDiv"/>
              <xsl:if test="$minimal='false'">
                <xsl:value-of select="$numberSpacer"/>
              </xsl:if>
            </xsl:if>
          </xsl:when>
          <xsl:when test="ancestor::tei:front">
            <xsl:if test="$numberFrontHeadings='true'">
              <xsl:call-template name="numberFrontDiv">
                <xsl:with-param name="minimal">
                  <xsl:value-of select="$minimal"/>
                </xsl:with-param>
              </xsl:call-template>
            </xsl:if>
          </xsl:when>
          <xsl:when test="$numberHeadings ='true'">
            <xsl:choose>
              <xsl:when test="$prenumberedHeadings='true'">
                <xsl:value-of select="@n"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="numberBodyDiv"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="$minimal='false'">
              <xsl:call-template name="headingNumberSuffix"/>
            </xsl:if>
          </xsl:when>
        </xsl:choose>
      </xsl:with-param>
    </xsl:call-template>
    <xsl:if test="$minimal='false'">
      <xsl:choose>
        <!--rausnehmen, da sonst überflüssige Ebenen im Index.html angezeigt werden
                    <xsl:when test="local-name(.) = 'TEI'">
                    <xsl:apply-templates select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[1]"/>
                </xsl:when>-->
        <xsl:when test="not($toc='')">
          <xsl:call-template name="makeInternalLink">
            <xsl:with-param name="dest">
              <xsl:value-of select="$toc"/>
            </xsl:with-param>
            <xsl:with-param name="class">
              <xsl:value-of select="$class_toc"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="concat($class_toc,'_',$depth)"/>
            </xsl:with-param>
            <xsl:with-param name="body">
              <xsl:choose>
                <xsl:when test="not(tei:head) and tei:body/tei:head">
                  <xsl:apply-templates mode="plain" select="tei:body/tei:head"/>
                </xsl:when>
                <xsl:when test="not(tei:head) and tei:front//tei:titlePart/tei:title">
                  <xsl:apply-templates mode="plain" select="tei:front//tei:titlePart/tei:title"/>
                </xsl:when>
                <xsl:when test="tei:head">
                  <xsl:apply-templates mode="plain" select="tei:head"/>
                </xsl:when>
                <xsl:when test="$autoHead='true'">
                  <xsl:call-template name="autoMakeHead">
                    <xsl:with-param name="display" select="$display"/>
                  </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:number/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="$autoHead='true'">
          <xsl:choose>
            <xsl:when
              test="($outputTarget='epub' or $outputTarget='epub3') and
                            not(tei:head)"/>
            <xsl:otherwise>
              <xsl:call-template name="autoMakeHead">
                <xsl:with-param name="display" select="$display"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$display='plain'">
          <xsl:for-each select="tei:head">
            <xsl:apply-templates mode="plain"/>
          </xsl:for-each>
        </xsl:when>
        <xsl:when test="$display='simple'">
          <xsl:for-each select="tei:head">
            <xsl:apply-templates mode="plain"/>
          </xsl:for-each>
        </xsl:when>
        <xsl:when test="self::tei:index">
          <xsl:value-of select="substring(tei:term,1,10)"/>
          <xsl:text>…</xsl:text>
        </xsl:when>
        <xsl:when test="not(tei:head)">
          <xsl:value-of select="substring(normalize-space(.),1,10)"/>
          <xsl:text>…</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="tei:head" mode="makeheading"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <!-- ************************ Anpassungen im Inhaltsverzeichnis ******************************* -->
  <!-- from html.xsl -->
  <xsl:template match="tei:*" mode="maketoc">
    <xsl:param name="forcedepth"/>
    <xsl:variable name="myName">
      <xsl:value-of select="local-name(.)"/>
    </xsl:variable>
    <xsl:if test="tei:head or $autoHead='true'">
      <xsl:variable name="Depth">
        <xsl:choose>
          <xsl:when test="not($forcedepth='')">
            <xsl:value-of select="$forcedepth"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$tocDepth"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:variable name="thislevel">
        <xsl:choose>
          <xsl:when test="$myName = 'div'">
            <xsl:value-of select="count(ancestor::tei:div)"/>
          </xsl:when>
          <xsl:when test="starts-with($myName,'div')">
            <xsl:value-of select="number(substring-after($myName,'div')) - 1"/>
          </xsl:when>
          <xsl:otherwise>99</xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:variable name="pointer">
        <xsl:apply-templates mode="generateLink" select="."/>
      </xsl:variable>
      <xsl:choose>
        <!-- testen, ob für die jeweilige stelle ein <li> angelegt werden soll -->
        <xsl:when
          test="tei:head or tei:lg or tei:lg/tei:head or tei:front/tei:head or tei:div[@subtype='work:no'] or tei:div[@type='Dramatis_Personae'] or tei:div[ancestor::tei:TEI//tei:titleStmt/tei:title ='[Widmung]'] or tei:div[ancestor::tei:TEI//tei:titleStmt/tei:title ='[Motto]']">
          <li>
            <xsl:attribute name="class">
              <xsl:text>toc</xsl:text>
              <xsl:if test="not($autoHead='true') and not(tei:head or @n)"> headless</xsl:if>
              <xsl:if test=".//m:math and  $outputTarget='epub3'">
                <xsl:attribute
                  name="class"> contains-mathml</xsl:attribute>
              </xsl:if>
            </xsl:attribute>
            <xsl:call-template name="header">
              <xsl:with-param name="toc" select="$pointer"/>
              <xsl:with-param name="minimal">false</xsl:with-param>
              <xsl:with-param name="display">plain</xsl:with-param>
            </xsl:call-template>
            <xsl:if test="$thislevel &lt; $Depth">
              <xsl:call-template name="continuedToc"/>
            </xsl:if>
          </li>
        </xsl:when>
        <xsl:otherwise>
          <xsl:if test="$thislevel &lt; $Depth">
            <xsl:call-template name="continuedToc"/>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <!-- aus html_textstructure.xsl -->
 <xsl:template name="mainTOC">
    <xsl:choose>
      <xsl:when test="self::tei:teiCorpus">
        <ul>
          <xsl:for-each select="tei:TEI">
            <!-- Listenpunkte verbergen -->
            <li style="list-style-type:none;">
              <a>
                <xsl:attribute name="href">
                  <xsl:apply-templates mode="generateLink" select="."/>
                </xsl:attribute>
                <xsl:call-template name="header">
                  <xsl:with-param name="minimal">false</xsl:with-param>
                  <xsl:with-param name="display">plain</xsl:with-param>
                </xsl:call-template>
              </a>
              <xsl:for-each select="tei:text/tei:front">
                <xsl:call-template name="partTOC">
                  <xsl:with-param name="part">front</xsl:with-param>
                </xsl:call-template>
              </xsl:for-each>
              <xsl:for-each select="tei:text/tei:body">
                <xsl:call-template name="partTOC">
                  <xsl:with-param name="part">body</xsl:with-param>
                </xsl:call-template>
              </xsl:for-each>
              <xsl:for-each select="tei:text/tei:back">
                <xsl:call-template name="partTOC">
                  <xsl:with-param name="part">back</xsl:with-param>
                </xsl:call-template>
              </xsl:for-each>
            </li>
          </xsl:for-each>
        </ul>
      </xsl:when>
      <xsl:when
        test="ancestor-or-self::tei:TEI/tei:text/tei:group and
                $splitLevel=0">
        <xsl:for-each select="ancestor-or-self::tei:TEI/tei:text/tei:front">
          <xsl:call-template name="partTOC">
            <xsl:with-param name="part">front</xsl:with-param>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="ancestor-or-self::tei:TEI/tei:text/tei:group">
          <xsl:call-template name="groupTOC"/>
        </xsl:for-each>
        <xsl:for-each select="ancestor-or-self::tei:TEI/tei:text/tei:back">
          <xsl:call-template name="partTOC">
            <xsl:with-param name="part">back</xsl:with-param>
          </xsl:call-template>
        </xsl:for-each>
      </xsl:when>
      <xsl:when test="ancestor-or-self::tei:TEI/tei:text/tei:group">
        <xsl:for-each select="ancestor-or-self::tei:TEI/tei:text/tei:group/tei:text">
          <h3>
            <xsl:number/>
            <xsl:choose>
              <xsl:when test="tei:body/tei:head">
                <xsl:text>. </xsl:text>
                <xsl:apply-templates select="tei:body/tei:head" mode="plain"/>
              </xsl:when>
              <xsl:when test="tei:front/tei:titlePage//tei:title">
                <xsl:apply-templates select="tei:front/tei:titlePage//tei:title[1]" mode="plain"/>
              </xsl:when>
            </xsl:choose>
          </h3>
          <xsl:for-each select="tei:front">
            <xsl:call-template name="partTOC">
              <xsl:with-param name="part">front</xsl:with-param>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:for-each select="tei:body">
            <xsl:call-template name="partTOC">
              <xsl:with-param name="part">body</xsl:with-param>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:for-each select="tei:back">
            <xsl:call-template name="partTOC">
              <xsl:with-param name="part">back</xsl:with-param>
            </xsl:call-template>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="$tocFront">
          <xsl:for-each select="ancestor-or-self::tei:TEI/tei:text/tei:front">
            <xsl:call-template name="partTOC">
              <xsl:with-param name="part">front</xsl:with-param>
            </xsl:call-template>
          </xsl:for-each>
        </xsl:if>
        <xsl:for-each select="ancestor-or-self::tei:TEI/tei:text/tei:body">
          <xsl:call-template name="partTOC">
            <xsl:with-param name="part">body</xsl:with-param>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:if test="$tocBack">
          <xsl:for-each select="ancestor-or-self::tei:TEI/tei:text/tei:back">
            <xsl:call-template name="partTOC">
              <xsl:with-param name="part">back</xsl:with-param>
            </xsl:call-template>
          </xsl:for-each>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- ************************************** Metadaten ******************************************* -->

  <!-- aus tei-to-epub.xsl, Anpassung der Metadaten (Dublin-Core-Header in content.opf, 
  korrekte Metadaten gewährleisten korrekte Sortierung o.ä. des Epub -->
  <xsl:template name="opfmetadata">
    <xsl:param name="author"/>
    <xsl:param name="printAuthor"/>
    <xsl:param name="coverImageOutside"/>
    <metadata xmlns="http://www.idpf.org/2007/opf" xmlns:dc="http://purl.org/dc/elements/1.1/"
      xmlns:dcterms="http://purl.org/dc/terms/"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:opf="http://www.idpf.org/2007/opf">
      <dc:title>
        <xsl:sequence select="tei:generateSimpleTitle(.)"/>
      </dc:title>
      <dc:language xsi:type="dcterms:RFC3066">
        <xsl:call-template name="generateLanguage"/>
      </dc:language>
      <xsl:call-template name="generateSubject"/>
      <dc:identifier id="dcidid" opf:scheme="URI">
        <xsl:call-template name="generateID"/>
      </dc:identifier>
      <dc:description>
        <xsl:sequence select="tei:generateSimpleTitle(.)"/>
        <xsl:text> / </xsl:text>
        <xsl:value-of select="$author"/>
      </dc:description>
      <dc:creator>
        <xsl:choose>
          <xsl:when test="$printAuthor=''">Anonymous/Unknown</xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$printAuthor"/>
          </xsl:otherwise>
        </xsl:choose>
      </dc:creator>
      <dc:publisher>
        <xsl:sequence select="tei:generatePublisher(.,$publisher)"/>
      </dc:publisher>
      <xsl:for-each select="tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@notAfter]">
        <dc:date opf:event="creation">
          <xsl:value-of select="@notAfter"/>
        </dc:date>
      </xsl:for-each>
      <!--Attribut @when-iso statt @when -->
      <xsl:for-each select="tei:teiHeader/tei:fileDesc/tei:sourceDesc//tei:date[@when-iso][1]">
        <dc:date opf:event="original-publication">
          <xsl:value-of select="@when-iso"/>
        </dc:date>
      </xsl:for-each>
      <dc:date opf:event="epub-publication" xsi:type="dcterms:W3CDTF">
        <xsl:sequence select="tei:generateDate(.)"/>
      </dc:date>
      <dc:rights>
        <xsl:call-template name="generateLicence"/>
      </dc:rights>
      <xsl:if test="not($coverImageOutside='')">
        <meta name="cover" content="cover-image"/>
      </xsl:if>
    </metadata>
  </xsl:template>

  <!-- bei mehreren <title>-Elementen Punkte als Trennzeichen einfügen, zur besseren Lesbarkeit -->
  <xsl:template match="tei:title" mode="simple">
    <xsl:if test="preceding-sibling::tei:title">
      <xsl:text>. </xsl:text>
    </xsl:if>
    <xsl:value-of select="."/>
    <xsl:if test="not(following-sibling::tei:title)">
      <xsl:text>.</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:function name="tei:generateAuthor">
    <xsl:param name="context"/>
    <xsl:for-each select="$context">
      <xsl:choose>
        <xsl:when
          test="$useHeaderFrontMatter='true' and ancestor-or-self::tei:TEI/tei:text/tei:front//tei:docAuthor">
          <xsl:apply-templates mode="author"
            select="ancestor-or-self::tei:TEI/tei:text/tei:front//tei:docAuthor"/>
        </xsl:when>
        <!-- nicht in titleStmt, sondern in sourceDesc nach Autor suchen -->
        <xsl:when
          test="ancestor-or-self::tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc//tei:author">
          <xsl:for-each
            select="ancestor-or-self::tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc//tei:author">
            <xsl:apply-templates select="*[not(self::tei:email)]|text()"/>
            <xsl:choose>
              <xsl:when test="count(following-sibling::tei:author)=1">
                <xsl:if test="count(preceding-sibling::tei:author)&gt;1">
                  <xsl:text>,</xsl:text>
                </xsl:if>
                <xsl:text> </xsl:text>
                <xsl:sequence select="tei:i18n('and')"/>
                <xsl:text> </xsl:text>
              </xsl:when>
              <xsl:when test="following-sibling::tei:author">, </xsl:when>
            </xsl:choose>
          </xsl:for-each>
        </xsl:when>
        <!-- wenn im header vom teiCorpus keine sourceDesc vorhanden ist, im header des ersten TEI-Elements suchen -->
        <xsl:when
          test="ancestor-or-self::tei:teiCorpus/tei:TEI[1]/tei:teiHeader/tei:fileDesc/tei:sourceDesc//tei:author">
          <xsl:for-each
            select="ancestor-or-self::tei:teiCorpus/tei:TEI[1]/tei:teiHeader/tei:fileDesc/tei:sourceDesc//tei:author">
            <xsl:apply-templates/>
            <xsl:choose>
              <xsl:when test="count(following-sibling::tei:author)=1">
                <xsl:if test="count(preceding-sibling::tei:author)>1">
                  <xsl:text>,</xsl:text>
                </xsl:if>
                <xsl:call-template name="i18n">
                  <xsl:with-param name="word">and</xsl:with-param>
                </xsl:call-template>
              </xsl:when>
              <xsl:when test="following-sibling::tei:author">, </xsl:when>
            </xsl:choose>
          </xsl:for-each>
        </xsl:when>
        <xsl:when
          test="ancestor-or-self::tei:TEI/tei:teiHeader/tei:revisionDesc/tei:change/tei:respStmt[tei:resp='author']">
          <xsl:apply-templates
            select="ancestor-or-self::tei:TEI/tei:teiHeader/tei:revisionDesc/tei:change/tei:respStmt[tei:resp='author'][1]/tei:name"
          />
        </xsl:when>
        <xsl:when test="ancestor-or-self::tei:TEI/tei:text/tei:front//tei:docAuthor">
          <xsl:apply-templates mode="author"
            select="ancestor-or-self::tei:TEI/tei:text/tei:front//tei:docAuthor"/>
        </xsl:when>
        <!-- Fallback: Take any author we can get -->
        <xsl:when test="ancestor-or-self::tei:TEI/tei:teiHeader//tei:author">
                <xsl:apply-templates mode="author"
                select="ancestor-or-self::tei:TEI/tei:teiHeader//tei:author"/>
        </xsl:when>
      </xsl:choose>
    </xsl:for-each>
  </xsl:function>
	
	<xsl:template name="generateLanguage">
		<xsl:choose>
			<xsl:when test="@xml:lang">
				<xsl:value-of select="@xml:lang"/>
			</xsl:when>
			<xsl:when test="tei:text/@xml:lang">
				<xsl:value-of select="tei:text/@xml:lang"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>de</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
  <!-- *********************** Anpassungen in manifest und spine (OPF-Datei) ********************** -->
  <xsl:template name="processTEI">
    <xsl:variable name="stage1">
      <xsl:apply-templates mode="preflight"/>
    </xsl:variable>

    <xsl:for-each select="$stage1">
      <xsl:call-template name="processTEIHook"/>
      <xsl:variable name="coverImageOutside">
        <xsl:choose>
          <xsl:when test="/tei:TEI/tei:text/tei:front/tei:titlePage[@facs]">
            <xsl:for-each select="/tei:TEI/tei:text/tei:front/tei:titlePage[@facs][1]">
              <xsl:for-each select="id(substring(@facs,2))">
                <xsl:choose>
                  <xsl:when test="count(tei:graphic)=1">
                    <xsl:value-of select="tei:graphic/@url"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="tei:graphic[2]/@url"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </xsl:for-each>
          </xsl:when>
          <xsl:when test="not($coverimage='')">
            <xsl:value-of select="tokenize($coverimage,'/')[last()]"/>
          </xsl:when>
        </xsl:choose>
      </xsl:variable>
      <xsl:variable name="coverImageInside">
        <xsl:choose>
          <xsl:when test="/tei:TEI/tei:text/tei:front/tei:titlePage[@facs]">
            <xsl:for-each select="/tei:TEI/tei:text/tei:front/tei:titlePage[@facs][1]">
              <xsl:for-each select="id(substring(@facs,2))">
                <xsl:value-of select="tei:graphic[1]/@url"/>
              </xsl:for-each>
            </xsl:for-each>
          </xsl:when>
          <xsl:when test="not($coverimage='')">
            <xsl:value-of select="tokenize($coverimage,'/')[last()]"/>
          </xsl:when>
        </xsl:choose>
      </xsl:variable>
      <xsl:choose>
        <xsl:when test="$splitLevel='-1'">
          <xsl:apply-templates/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates mode="split"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:for-each select="*">
        <xsl:variable name="TOC">
          <TOC xmlns="http://www.w3.org/1999/xhtml">
            <xsl:call-template name="mainTOC"/>
          </TOC>
        </xsl:variable>
        
<!--	    <xsl:result-document href="/tmp/TOC">
	    <xsl:copy-of select="$TOC"/>
	    </xsl:result-document>
-->	
        <xsl:for-each select="tokenize($javascriptFiles,',')">
          <xsl:variable name="file" select="normalize-space(.)"/>
          <xsl:variable name="name" select="tokenize($file,'/')[last()]"/>
          <xsl:if test="$verbose='true'">
            <xsl:message>write Javascript file <xsl:value-of select="$name"/></xsl:message>
          </xsl:if>
          <xsl:result-document method="text" href="{concat($directory,'/OPS/',$name)}">
            <xsl:for-each select="unparsed-text($file)">
              <xsl:copy-of select="."/>
            </xsl:for-each>
          </xsl:result-document>
        </xsl:for-each>
        <xsl:if test="$verbose='true'">
          <xsl:message>write file OPS/stylesheet.css</xsl:message>
        </xsl:if>
        <xsl:result-document method="text" href="{concat($directory,'/OPS/stylesheet.css')}">
          <xsl:if test="not($cssFile='')">
            <xsl:if test="$verbose='true'">
              <xsl:message>reading file <xsl:value-of select="$cssFile"/></xsl:message>
            </xsl:if>
            <xsl:for-each select="tokenize(unparsed-text($cssFile),     '\r?\n')">
              <xsl:call-template name="purgeCSS"/>
            </xsl:for-each>
          </xsl:if>
          <xsl:if test="not($cssSecondaryFile='')">
            <xsl:if test="$verbose='true'">
              <xsl:message>reading secondary file <xsl:value-of select="$cssSecondaryFile"
                /></xsl:message>
            </xsl:if>
            <xsl:for-each select="tokenize(unparsed-text($cssSecondaryFile),       '\r?\n')">
              <xsl:call-template name="purgeCSS"/>
            </xsl:for-each>
          </xsl:if>
          <xsl:if test="$odd='true'">
            <xsl:if test="$verbose='true'">
              <xsl:message>reading file <xsl:value-of select="$cssODDFile"/></xsl:message>
            </xsl:if>
            <xsl:for-each select="tokenize(unparsed-text($cssODDFile),         '\r?\n')">
              <xsl:call-template name="purgeCSS"/>
            </xsl:for-each>
          </xsl:if>
          <xsl:if test="$odd='true'">
            <xsl:if test="$verbose='true'">
              <xsl:message>reading file <xsl:value-of select="$cssODDFile"/></xsl:message>
            </xsl:if>
            <xsl:for-each select="tokenize(unparsed-text($cssODDFile),         '\r?\n')">
              <xsl:call-template name="purgeCSS"/>
            </xsl:for-each>
          </xsl:if>
          <xsl:if test="$filePerPage='true'">
            <xsl:text>body { width: </xsl:text>
            <xsl:value-of select="number($viewPortWidth)-100"/>
            <xsl:text>px;
 height: </xsl:text>
            <xsl:value-of select="$viewPortHeight"/>
            <xsl:text>px;
}
img.fullpage {
position: absolute;
height: </xsl:text>
            <xsl:value-of select="$viewPortHeight"/>
            <xsl:text>px; left:0px; top:0px;}
</xsl:text>
          </xsl:if>
        </xsl:result-document>
        <xsl:if test="$verbose='true'">
          <xsl:message>write file OPS/print.css</xsl:message>
        </xsl:if>
        <xsl:result-document method="text" href="{concat($directory,'/OPS/print.css')}">
          <xsl:if test="$verbose='true'">
            <xsl:message>reading file <xsl:value-of select="$cssPrintFile"/></xsl:message>
          </xsl:if>
          <xsl:for-each select="tokenize(unparsed-text($cssPrintFile),     '\r?\n')">
            <xsl:call-template name="purgeCSS"/>
          </xsl:for-each>
        </xsl:result-document>
        <xsl:if test="$verbose='true'">
          <xsl:message>write file mimetype</xsl:message>
        </xsl:if>
        <xsl:result-document method="text" href="{concat($directory,'/mimetype')}">
          <xsl:value-of select="$epubMimetype"/>
        </xsl:result-document>
        <xsl:if test="$verbose='true'">
          <xsl:message>write file META-INF/container.xml</xsl:message>
        </xsl:if>
        <xsl:result-document method="xml" href="{concat($directory,'/META-INF/container.xml')}">
          <container xmlns="urn:oasis:names:tc:opendocument:xmlns:container" version="1.0">
            <rootfiles>
              <rootfile full-path="OPS/content.opf" media-type="application/oebps-package+xml"/>
            </rootfiles>
          </container>
        </xsl:result-document>
        <xsl:if test="$verbose='true'">
          <xsl:message>write file OPS/content.opf</xsl:message>
        </xsl:if>
        <xsl:result-document href="{concat($directory,'/OPS/content.opf')}" method="xml">
          <xsl:variable name="A">
            <xsl:sequence select="tei:generateAuthor(.)"/>
          </xsl:variable>
          <xsl:variable name="printA">
            <xsl:analyze-string select="$A" regex="([^,]+), ([^,]+), (.+)">
              <xsl:matching-substring>
                <xsl:value-of select="regex-group(1)"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="regex-group(2)"/>
              </xsl:matching-substring>
              <xsl:non-matching-substring>
                <xsl:value-of select="."/>
              </xsl:non-matching-substring>
            </xsl:analyze-string>
          </xsl:variable>

          <package xmlns="http://www.idpf.org/2007/opf" unique-identifier="dcidid"
            version="{$opfPackageVersion}">
            <xsl:call-template name="opfmetadata">
              <xsl:with-param name="author" select="$A"/>
              <xsl:with-param name="printAuthor" select="$printA"/>
              <xsl:with-param name="coverImageOutside" select="$coverImageOutside"/>
            </xsl:call-template>
            <manifest>
              <!-- deal with intricacies of overlay files -->
              <xsl:variable name="TL" select="key('Timeline',1)"/>
              <xsl:if test="$mediaoverlay='true' and        key('Timeline',1)">
                <xsl:if test="$verbose='true'">
                  <xsl:message>write file SMIL files</xsl:message>
                </xsl:if>
                <xsl:for-each select="key('Timeline',1)">
                  <xsl:variable name="TLnumber">
                    <xsl:number level="any"/>
                  </xsl:variable>
                  <xsl:variable name="audio">
                    <xsl:text>media/audio</xsl:text>
                    <xsl:number level="any"/>
                    <xsl:text>.</xsl:text>
                    <xsl:value-of select="tokenize(@corresp,'\.')[last()]"/>
                  </xsl:variable>
                  <item id="timeline-audio{$TLnumber}" href="{$audio}">
                    <xsl:attribute name="media-type">
                      <xsl:choose>
                        <xsl:when test="contains($audio,'.m4a')">audio/m4a</xsl:when>
                        <xsl:otherwise>audio/m4a</xsl:otherwise>
                      </xsl:choose>
                    </xsl:attribute>
                  </item>
                  <xsl:for-each select="key('PB',1)">
                    <xsl:variable name="page">
                      <xsl:value-of select="generate-id()"/>
                    </xsl:variable>
                    <xsl:variable name="target">
                      <xsl:apply-templates select="." mode="ident"/>
                    </xsl:variable>
                    <xsl:if test="count(key('objectOnPage',$page))&gt;0">
                      <item id="{$target}-audio" href="{$target}-overlay.smil"
                        media-type="application/smil+xml"/>
                      <xsl:result-document
                        href="{concat($directory,'/OPS/',$target,'-overlay.smil')}" method="xml">
                        <smil xmlns="http://www.w3.org/ns/SMIL" version="3.0"
                          profile="http://www.ipdf.org/epub/30/profile/content/">
                          <body>
                            <xsl:for-each select="key('objectOnPage',$page)">
                              <xsl:variable name="object" select="@xml:id"/>
                              <xsl:for-each select="$TL">
                                <xsl:for-each select="key('Object',$object)">
                                  <par id="{@xml:id}">
                                    <text src="{$target}.html{@corresp}"/>
                                    <audio src="{$audio}" clipBegin="{@from}{../@unit}"
                                      clipEnd="{@to}{../@unit}"/>
                                  </par>
                                </xsl:for-each>
                              </xsl:for-each>
                            </xsl:for-each>
                          </body>
                        </smil>
                      </xsl:result-document>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:for-each>
              </xsl:if>
              <xsl:if test="not($coverImageOutside='')">
                <item href="{$coverImageOutside}" id="cover-image" media-type="image/jpeg"/>
              </xsl:if>
              <xsl:for-each select="tokenize($javascriptFiles,',')">
                <xsl:variable name="name" select="tokenize(normalize-space(.),'/')[last()]"/>
                <item href="{$name}" id="javascript{position()}" media-type="text/javascript"/>
              </xsl:for-each>
              <item href="stylesheet.css" id="css" media-type="text/css"/>
              <item href="titlepage.html" id="titlepage" media-type="application/xhtml+xml"/>
              <xsl:if test="$filePerPage='true'">
                <item href="titlepageverso.html" id="titlepageverso"
                  media-type="application/xhtml+xml"/>
              </xsl:if>
              <xsl:for-each select="tei:text/tei:front/tei:titlePage">
                <xsl:variable name="N" select="position()"/>
                <item href="titlepage{$N}.html" id="titlepage{$N}"
                  media-type="application/xhtml+xml"/>
              </xsl:for-each>
              <item href="titlepageback.html" id="titlepageback" media-type="application/xhtml+xml"/>
              <item id="print.css" href="print.css" media-type="text/css"/>
              <item id="apt" href="page-template.xpgt"
                media-type="application/vnd.adobe-page-template+xml" fallback-style="css"/>
              <item id="start" href="index.html" media-type="application/xhtml+xml"/>
              <xsl:choose>
                <xsl:when test="$filePerPage='true'">
                  <xsl:for-each select="key('PB',1)">
                    <xsl:variable name="target">
                      <xsl:apply-templates select="." mode="ident"/>
                    </xsl:variable>
                    <xsl:if test="@facs">
                      <xsl:variable name="facstarget">
                        <xsl:apply-templates select="." mode="ident"/>
                        <xsl:text>-facs.html</xsl:text>
                      </xsl:variable>
                      <item href="{$facstarget}" media-type="application/xhtml+xml">
                        <xsl:attribute name="id">
                          <xsl:text>pagefacs</xsl:text>
                          <xsl:number level="any"/>
                        </xsl:attribute>
                      </item>
                    </xsl:if>
                    <item href="{$target}.html" media-type="application/xhtml+xml">
                      <xsl:if test="$mediaoverlay='true'">
                        <xsl:attribute name="media-overlay">
                          <xsl:value-of select="$target"/>
                          <xsl:text>-audio</xsl:text>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:attribute name="id">
                        <xsl:text>page</xsl:text>
                        <xsl:number level="any"/>
                      </xsl:attribute>
                    </item>
                  </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:for-each select="$TOC/html:TOC">
                    <!--/html:ul/html:li"-->
                    <xsl:choose>
                      <xsl:when test="not(html:a)"/>
                      <xsl:when test="starts-with(html:a/@href,'#')"/>
                      <xsl:otherwise>
                        <item href="{html:a[1]/@href}" media-type="application/xhtml+xml">
                          <xsl:attribute name="id">
                            <xsl:text>section</xsl:text>
                            <xsl:number count="html:li" level="any"/>
                          </xsl:attribute>
                        </item>
                      </xsl:otherwise>
                    </xsl:choose>
                    <!-- item-Einträge für jede html-Inhaltsdatei im manifest erzeugen -->
                    <xsl:if test="html:ul">
                      <!-- <xsl:for-each select="html:ul//html:li[html:a          and          not(contains(html:a/@href,'#'))]">-->
                      <xsl:for-each select="descendant::html:li">
                        <xsl:choose>
                          <!-- Einträge auf HTML-Dateien, die schon weiter vorn (zB mit einem anderen Fragment) referenziert worden sind, überspringen -->
                          <xsl:when test="html:a and preceding::html:a[substring-before(@href, '#') = substring-before(current()/html:a/@href, '#')]"/>                                                           
                          <xsl:when test="html:a and contains(html:a/@href, '#')">  
                            <item href="{substring-before(html:a[1]/@href,'#')}" media-type="application/xhtml+xml">                              
                              <xsl:attribute name="id">
                                <xsl:text>section</xsl:text>
                                <xsl:number count="html:li" level="any"/>
                              </xsl:attribute>                              
                            </item>
                          </xsl:when>
                          <xsl:otherwise>
                            <item href="{html:a[1]/@href}" media-type="application/xhtml+xml">
                              <xsl:attribute name="id">
                                <xsl:text>section</xsl:text>
                                <xsl:number count="html:li" level="any"/>
                              </xsl:attribute>
                            </item>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:for-each>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:otherwise>
              </xsl:choose>
              <!-- images -->
              <xsl:for-each select="key('GRAPHICS',1)">
                <xsl:variable name="img" select="@url|@facs"/>
                <xsl:if test="not($img=$coverImageOutside)">
                  <xsl:variable name="ID">
                    <xsl:number level="any"/>
                  </xsl:variable>
                  <xsl:variable name="mimetype">
                    <xsl:choose>
                      <xsl:when test="@mimeType != ''">
                        <xsl:value-of select="@mimeType"/>
                      </xsl:when>
                      <xsl:when test="contains($img,'.gif')">image/gif</xsl:when>
                      <xsl:when test="contains($img,'.png')">image/png</xsl:when>
                      <xsl:when test="contains($img,'.mpeg')">video/mpeg4</xsl:when>
                      <xsl:when test="contains($img,'.mp4')">video/mpeg4</xsl:when>
                      <xsl:when test="contains($img,'.m4v')">video/mpeg4</xsl:when>
                      <xsl:otherwise>image/jpeg</xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <item href="{$img}" id="image-{$ID}" media-type="{$mimetype}"/>
                </xsl:if>
              </xsl:for-each>
              <!-- page images -->
              <xsl:for-each select="key('PBGRAPHICS',1)">
                <xsl:variable name="img" select="@facs"/>
                <xsl:variable name="ID">
                  <xsl:number level="any"/>
                </xsl:variable>
                <xsl:variable name="mimetype">
                  <xsl:choose>
                    <xsl:when test="@mimeType != ''">
                      <xsl:value-of select="@mimeType"/>
                    </xsl:when>
                    <xsl:when test="contains($img,'.gif')">image/gif</xsl:when>
                    <xsl:when test="contains($img,'.png')">image/png</xsl:when>
                    <xsl:otherwise>image/jpeg</xsl:otherwise>
                  </xsl:choose>
                </xsl:variable>
                <item href="{$img}" id="pbimage-{$ID}" media-type="{$mimetype}"/>
              </xsl:for-each>
              <item id="ncx" href="toc.ncx" media-type="application/x-dtbncx+xml"/>
              <xsl:call-template name="epubManifestHook"/>
            </manifest>
            <spine toc="ncx">
              <itemref idref="titlepage" linear="yes"/>
              <xsl:if test="$filePerPage='true'">
                <itemref idref="titlepageverso" linear="yes"/>
              </xsl:if>
              <xsl:for-each select="tei:text/tei:front/tei:titlePage">
                <xsl:variable name="N" select="position()"/>
                <itemref idref="titlepage{$N}" linear="yes"/>
              </xsl:for-each>
              <itemref idref="start" linear="yes"/>
              <xsl:choose>
                <xsl:when test="$filePerPage='true'">
                  <xsl:for-each select="key('PB',1)">
                    <xsl:if test="@facs">
                      <itemref>
                        <xsl:attribute name="idref">
                          <xsl:text>pagefacs</xsl:text>
                          <xsl:number level="any"/>
                        </xsl:attribute>
                        <xsl:attribute name="linear">yes</xsl:attribute>
                      </itemref>
                    </xsl:if>
                    <itemref>
                      <xsl:attribute name="idref">
                        <xsl:text>page</xsl:text>
                        <xsl:number level="any"/>
                      </xsl:attribute>
                      <xsl:attribute name="linear">yes</xsl:attribute>
                    </itemref>
                  </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:for-each select="$TOC/html:TOC">
                    <!--/html:ul/html:li"-->
                    <xsl:choose>
                      <xsl:when test="not(html:a)"/>
                      <xsl:when test="starts-with(html:a/@href,'#')"/>
                      <xsl:otherwise>
                        <itemref>
                          <xsl:attribute name="idref">
                            <xsl:text>section</xsl:text>
                            <xsl:number count="html:li" level="any"/>
                          </xsl:attribute>
                          <xsl:attribute name="linear">yes</xsl:attribute>
                        </itemref>
                      </xsl:otherwise>
                    </xsl:choose>
                    <!-- itemref-Einträge für jede html-Inhaltsdatei erzeugen, analog zu item-Einträgen im manifest -->
                    <xsl:if test="html:ul">
                      <xsl:for-each select="descendant::html:li">
                        <xsl:choose>
                        <xsl:when test="html:a and preceding::html:a[substring-before(@href, '#') = substring-before(current()/html:a/@href, '#')]"/>                                       
                          <xsl:when test="html:a and contains(html:a/@href, '#')">
                            <itemref>
                              <xsl:attribute name="idref">
                                <xsl:text>section</xsl:text>
                                <xsl:number count="html:li" level="any"/>
                              </xsl:attribute>
                              <xsl:attribute name="linear">yes</xsl:attribute>
                            </itemref>
                          </xsl:when>
                          <xsl:otherwise>
                            <itemref>
                              <xsl:attribute name="idref">
                                <xsl:text>section</xsl:text>
                                <xsl:number count="html:li" level="any"/>
                              </xsl:attribute>
                              <xsl:attribute name="linear">yes</xsl:attribute>
                            </itemref>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:for-each>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:otherwise>
              </xsl:choose>
              <itemref idref="titlepageback">
                <xsl:attribute name="linear">
                  <xsl:choose>
                    <xsl:when test="$filePerPage='true'">yes</xsl:when>
                    <xsl:otherwise>no</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
              </itemref>
              <xsl:call-template name="epubSpineHook"/>
            </spine>
            <guide>
              <reference type="text" href="titlepage.html" title="Cover"/>
              <reference type="text" title="Start" href="index.html"/>
              <xsl:choose>
                <xsl:when test="$filePerPage='true'"/>
                <xsl:otherwise>
                  <xsl:for-each select="$TOC/html:TOC/html:ul/html:li">
                    <xsl:if test="html:a and not (starts-with(html:a[1]/@href,'#'))">
                      <reference type="text" href="{html:a[1]/@href}">
                        <xsl:attribute name="title">
                          <xsl:value-of select="normalize-space(html:a[1])"/>
                        </xsl:attribute>
                      </reference>
                    </xsl:if>
                    <xsl:if test="contains(parent::html:ul/@class,'group')">
                      <xsl:for-each select="html:ul//html:li">
                        <xsl:choose>
                          <xsl:when test="not(html:a)"/>
                          <xsl:when test="contains(html:a/@href,'#')"/>
                          <xsl:otherwise>
                            <reference type="text" href="{html:a/@href}">
                              <xsl:attribute name="title">
                                <xsl:value-of select="normalize-space(html:a[1])"/>
                              </xsl:attribute>
                            </reference>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:for-each>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:otherwise>
              </xsl:choose>
              <reference href="titlepageback.html" type="text" title="About this book"/>
            </guide>
          </package>
        </xsl:result-document>
        <xsl:if test="$verbose='true'">
          <xsl:message>write file OPS/titlepage.html</xsl:message>
        </xsl:if>
        <xsl:result-document href="{concat($directory,'/OPS/titlepage.html')}" method="xml">
          <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
            <head>
              <xsl:call-template name="metaHTML">
                <xsl:with-param name="title">Title page</xsl:with-param>
              </xsl:call-template>
              <meta name="calibre:cover" content="true"/>
              <title>Title page</title>
              <style type="text/css" title="override_css">
                @page {
                    padding:0pt;
                    margin:0pt
                }
                body{
                    text-align:center;
                    padding:0pt;
                    margin:0pt;
                }
                div.titlepage{
                    text-align:center;
                    padding:0pt;
                    margin:0pt;
                    font-size:225%;
                    font-family:Arial, Helvetica, sans-serif;
                }
                p.covertitle{
                    font-weight:bold;
                    color:#FFFFFF;
                    background-color:#012148;
                    margin:5%;
                    padding:5%
                }
                p.covertitle{
                    margin:10%
                }</style>
            </head>
            <body>
              <xsl:choose>
                <xsl:when test="$coverImageInside=''">
                  <div>
                    <xsl:attribute name="style"> -webkit-hyphens:none; font-family: serif;
                      height:860; font-size:30pt; font-weight: bold; padding-top: 15pt; margin:
                      12pt; border: solid red 1pt; text-align:center; </xsl:attribute>
                    <div class="titlepage">
                      <p class="covertitle">
                        <xsl:sequence select="tei:generateTitle(.)"/>
                      </p>
                      <!--<p class="coverauthor">
                        <xsl:sequence select="tei:generateAuthor(.)"/>
                      </p>-->
                    </div>
                  </div>
                </xsl:when>
                <xsl:otherwise>
                  <div>
                    <img width="{$viewPortWidth}" height="{$viewPortHeight}" alt="cover picture"
                      src="{$coverImageInside}"/>
                  </div>
                </xsl:otherwise>
              </xsl:choose>
            </body>
          </html>
        </xsl:result-document>
        <xsl:for-each select="tei:text/tei:front/tei:titlePage">
          <xsl:variable name="N" select="position()"/>
          <xsl:if test="$verbose='true'">
            <xsl:message>write file OPS/titlepage<xsl:value-of select="$N"/>.html</xsl:message>
          </xsl:if>
          <xsl:result-document href="{concat($directory,'/OPS/titlepage',$N,'.html')}" method="xml">
            <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
              <head>
                <xsl:call-template name="metaHTML">
                  <xsl:with-param name="title">Title page</xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name="linkCSS">
                  <xsl:with-param name="file">stylesheet.css</xsl:with-param>
                </xsl:call-template>
                <title>Title page</title>
              </head>
              <body>
                <div class="titlePage">
                  <xsl:apply-templates/>
                </div>
              </body>
            </html>
          </xsl:result-document>
        </xsl:for-each>
        <xsl:if test="$filePerPage='true'">
          <xsl:if test="$verbose='true'">
            <xsl:message>write file OPS/titlepageverso.html</xsl:message>
          </xsl:if>
          <xsl:result-document href="{concat($directory,'/OPS/titlepageverso.html')}" method="xml">
            <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
              <head>
                <xsl:call-template name="metaHTML">
                  <xsl:with-param name="title">title page verso</xsl:with-param>
                </xsl:call-template>
                <title>title page verso</title>
              </head>
              <body>
                <p/>
              </body>
            </html>
          </xsl:result-document>
        </xsl:if>
        <xsl:if test="$verbose='true'">
          <xsl:message>write file OPS/titlepageback.html</xsl:message>
        </xsl:if>
        <xsl:result-document href="{concat($directory,'/OPS/titlepageback.html')}" method="xml">
          <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
            <head>
              <xsl:call-template name="metaHTML">
                <xsl:with-param name="title">About this book</xsl:with-param>
              </xsl:call-template>
              <title>About this book</title>
            </head>
            <body>
              <div style="text-align: left; font-size: larger">
                <h2>Information about this book</h2>
                <xsl:for-each select="/*/tei:teiHeader/tei:fileDesc">
                  <xsl:apply-templates mode="metadata"/>
                </xsl:for-each>
                <xsl:for-each select="/*/tei:teiHeader/tei:encodingDesc">
                  <xsl:apply-templates mode="metadata"/>
                </xsl:for-each>
              </div>
            </body>
          </html>
        </xsl:result-document>
        <xsl:if test="$verbose='true'">
          <xsl:message>write file OPS/toc.ncx</xsl:message>
        </xsl:if>
        <xsl:result-document href="{concat($directory,'/OPS/toc.ncx')}" method="xml">
          <ncx xmlns="http://www.daisy.org/z3986/2005/ncx/" version="2005-1">
            <head>
              <meta name="dtb:uid">
                <xsl:attribute name="content">
                  <xsl:call-template name="generateID"/>
                </xsl:attribute>
              </meta>
              <meta name="dtb:totalPageCount" content="0"/>
              <meta name="dtb:maxPageNumber" content="0"/>
            </head>
            <docTitle>
              <text>
                <xsl:sequence select="tei:generateSimpleTitle(.)"/>
              </text>
            </docTitle>
            <navMap>
              <xsl:variable name="navPoints">
                <navPoint>
                  <navLabel>
                    <text>[Cover]</text>
                  </navLabel>
                  <content src="titlepage.html"/>
                </navPoint>
                <xsl:for-each select="tei:text/tei:front/tei:titlePage[1]">
                  <xsl:variable name="N" select="position()"/>
                  <navPoint>
                    <navLabel>
                      <text>[Title page]</text>
                    </navLabel>
                    <content src="titlepage{$N}.html"/>
                  </navPoint>
                </xsl:for-each>
                <!-- navPoint für das Inhaltsverzeichnis eingefügt -->
                <navPoint>
                  <navLabel>
                    <text>[Table of Contents]</text>
                  </navLabel>
                  <content src="index.html"/>
                </navPoint>
                <!-- für jedes <li> im Inhaltsverzeichnis ein navPoint anlegen -->
                <xsl:for-each select="$TOC/html:TOC//html:li">
                  <xsl:apply-templates select="."/>
                </xsl:for-each>
                <!-- <xsl:choose>
                  <xsl:when test="not($TOC/html:TOC/html:ul[@class='toc toc_body']/html:li)">
                    <xsl:for-each select="$TOC/html:TOC/html:ul[@class='toc toc_front']">
                      <xsl:apply-templates select="html:li"/>
                    </xsl:for-each>
                    <navPoint>
                      <navLabel>
                        <text>[The book]</text>
                      </navLabel>
                      <content src="index.html"/>
                    </navPoint>
                    <xsl:for-each select="$TOC/html:TOC/html:ul[contains(@class,'group')]">
                      <xsl:apply-templates select=".//html:li[not(contains(html:a/@href,'#'))]"/>
                    </xsl:for-each>
                    <xsl:for-each select="$TOC/html:TOC/html:ul[@class='toc toc_back']">
                      <xsl:apply-templates select="html:li"/>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:for-each select="$TOC/html:TOC/html:ul">
                      <xsl:apply-templates select="html:li"/>
                    </xsl:for-each>
                  </xsl:otherwise>
                </xsl:choose> -->
                <navPoint>
                  <navLabel>
                    <text>[About this book]</text>
                  </navLabel>
                  <content src="titlepageback.html"/>
                </navPoint>
              </xsl:variable>
              <xsl:for-each select="$navPoints/ncx:navPoint">
                <xsl:variable name="pos" select="position()"/>
                <navPoint id="navPoint-{$pos}" playOrder="{$pos}">
                  <xsl:copy-of select="*"/>
                </navPoint>
              </xsl:for-each>
            </navMap>
          </ncx>
        </xsl:result-document>
        <xsl:if test="$verbose='true'">
          <xsl:message>write file OPS/page-template.xpgt</xsl:message>
        </xsl:if>
        <xsl:result-document method="xml" href="{concat($directory,'/OPS/page-template.xpgt')}">
          <ade:template xmlns="http://www.w3.org/1999/xhtml"
            xmlns:ade="http://ns.adobe.com/2006/ade" xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
              <fo:simple-page-master master-name="single_column">
                <fo:region-body margin-bottom="3pt" margin-top="0.5em" margin-left="3pt"
                  margin-right="3pt"/>
              </fo:simple-page-master>
              <fo:simple-page-master master-name="single_column_head">
                <fo:region-before extent="8.3em"/>
                <fo:region-body margin-bottom="3pt" margin-top="6em" margin-left="3pt"
                  margin-right="3pt"/>
              </fo:simple-page-master>
              <fo:simple-page-master master-name="two_column" margin-bottom="0.5em"
                margin-top="0.5em" margin-left="0.5em" margin-right="0.5em">
                <fo:region-body column-count="2" column-gap="10pt"/>
              </fo:simple-page-master>
              <fo:simple-page-master master-name="two_column_head" margin-bottom="0.5em"
                margin-left="0.5em" margin-right="0.5em">
                <fo:region-before extent="8.3em"/>
                <fo:region-body column-count="2" margin-top="6em" column-gap="10pt"/>
              </fo:simple-page-master>
              <fo:simple-page-master master-name="three_column" margin-bottom="0.5em"
                margin-top="0.5em" margin-left="0.5em" margin-right="0.5em">
                <fo:region-body column-count="3" column-gap="10pt"/>
              </fo:simple-page-master>
              <fo:simple-page-master master-name="three_column_head" margin-bottom="0.5em"
                margin-top="0.5em" margin-left="0.5em" margin-right="0.5em">
                <fo:region-before extent="8.3em"/>
                <fo:region-body column-count="3" margin-top="6em" column-gap="10pt"/>
              </fo:simple-page-master>
              <fo:page-sequence-master>
                <fo:repeatable-page-master-alternatives>
                  <fo:conditional-page-master-reference master-reference="three_column_head"
                    page-position="first" ade:min-page-width="80em"/>
                  <fo:conditional-page-master-reference master-reference="three_column"
                    ade:min-page-width="80em"/>
                  <fo:conditional-page-master-reference master-reference="two_column_head"
                    page-position="first" ade:min-page-width="50em"/>
                  <fo:conditional-page-master-reference master-reference="two_column"
                    ade:min-page-width="50em"/>
                  <fo:conditional-page-master-reference master-reference="single_column_head"
                    page-position="first"/>
                  <fo:conditional-page-master-reference master-reference="single_column"/>
                </fo:repeatable-page-master-alternatives>
              </fo:page-sequence-master>
            </fo:layout-master-set>
            <ade:style>
              <ade:styling-rule selector=".title_box" display="adobe-other-region"
                adobe-region="xsl-region-before"/>
            </ade:style>
          </ade:template>
        </xsl:result-document>
        <xsl:if test="$filePerPage='true'">
          <xsl:if test="$verbose='true'">
            <xsl:message>write file META-INF/com.apple.ibooks.display-options.xml</xsl:message>
          </xsl:if>
          <xsl:result-document
            href="{concat($directory,'/META-INF/com.apple.ibooks.display-options.xml')}">
            <display_options xmlns="">
              <platform name="*">
                <option name="fixed-layout">true</option>
              </platform>
            </display_options>
          </xsl:result-document>
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>
  
  <!-- Links im Inhaltverzeichnis anpassen -->
  <!-- XXX Wenn aktiviert, stimmen die namen/links nicht mehr -->
<!--  <xsl:template match="*" mode="generateLink">
    <xsl:variable name="parentTEI" select="ancestor::tei:TEI[last()]/@xml:id"/>
    <xsl:variable name="ident">
      <xsl:apply-templates mode="ident" select="."/>
    </xsl:variable>
    <xsl:variable name="depth">
      <xsl:apply-templates mode="depth" select="."/>
    </xsl:variable>
    <xsl:variable name="keep" select="tei:keepDivOnPage(.)"/>
    <xsl:variable name="LINK">
      <xsl:choose>
        <xsl:when test="$filePerPage='true'">
          <xsl:choose>
            <xsl:when test="preceding::tei:pb">
              <xsl:apply-templates select="preceding::tei:pb[1]"
                mode="ident"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>index</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:value-of select="$standardSuffix"/>
        </xsl:when>
        <xsl:when test="ancestor::tei:elementSpec and not($STDOUT='true')">
          <xsl:sequence select="concat('ref-',ancestor::tei:elementSpec/@ident,$standardSuffix,'#',$ident)"/>
        </xsl:when>
        <xsl:when test="ancestor::tei:classSpec and not($STDOUT='true')">
          <xsl:sequence select="concat('ref-',ancestor::tei:classSpec/@ident,$standardSuffix,'#',$ident)"/>
        </xsl:when>
        <xsl:when test="not ($STDOUT='true') and ancestor::tei:back and not($splitBackmatter='true')">
          <xsl:value-of select="concat($masterFile,$standardSuffix,'#',$ident)"/>
        </xsl:when>
        <xsl:when test="not($STDOUT='true') and ancestor::tei:front
          and not($splitFrontmatter='true')">
          <xsl:value-of select="concat($masterFile,$standardSuffix,'#',$ident)"/>
        </xsl:when>
        <xsl:when test="not($keep) and $STDOUT='true' and
          number($depth) &lt;= number($splitLevel)">
          <xsl:sequence select="concat($masterFile,$standardSuffix,$urlChunkPrefix,$ident)"/>
        </xsl:when>
        <xsl:when test="self::tei:text and $splitLevel=0">
          <xsl:value-of select="concat($ident,$standardSuffix)"/>
        </xsl:when>
        <xsl:when test="number($splitLevel)= -1 and
          ancestor::tei:teiCorpus">
          <xsl:value-of select="$masterFile"/>
          <xsl:call-template name="addCorpusID"/>
          <xsl:value-of select="$standardSuffix"/>
          <xsl:value-of select="concat('#',$ident)"/>
        </xsl:when>
        <xsl:when test="number($splitLevel)= -1">
          <xsl:value-of select="concat('#',$ident)"/>
        </xsl:when>
        <xsl:when test="number($depth) &lt;= number($splitLevel) and not($keep)">
          <xsl:value-of select="concat($ident,$standardSuffix)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:variable name="parent">
            <xsl:call-template name="locateParentDiv"/>
          </xsl:variable>
          <xsl:choose>
            <xsl:when test="$STDOUT='true'">
              <xsl:sequence select="concat($masterFile,$urlChunkPrefix,$parent,'#',$ident)"/>
            </xsl:when>
            <xsl:when test="ancestor::tei:group">
              <xsl:sequence select="concat($parent,$standardSuffix,'#',$ident)"/>
            </xsl:when>
            <xsl:when test="ancestor::tei:floatingText">
              <xsl:sequence select="concat($parent,$standardSuffix,'#',$ident)"/>
            </xsl:when>
            <xsl:when test="$keep and number($depth=0)">
              <xsl:sequence select="concat('#',$ident)"/> 
            </xsl:when>
            <xsl:when test="$keep">
              <xsl:sequence select="concat($masterFile,$standardSuffix,'#',$ident)"/> 
            </xsl:when>
            <xsl:when test="ancestor::tei:div and tei:keepDivOnPage(ancestor::tei:div[last()])">
              <xsl:sequence select="concat($parent,$standardSuffix,'#',$ident)"/>
            </xsl:when>
           <xsl:otherwise>
             <xsl:sequence select="concat($ident,$standardSuffix)"/>
           </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <!-\-
      <xsl:message>GENERATELINK <xsl:value-of
      select="(name(),$ident,$depth,string($keep),$LINK)"
	  separator="|"/></xsl:message>
      -\->
    <xsl:value-of select="$LINK"/>
    
  </xsl:template>
-->  
</xsl:stylesheet>
