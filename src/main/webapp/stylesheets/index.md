Aggregator Stylesheets
======================

---------------------------------------------------------------------------------------- -----------------------------------------------------------------------
[corpus2groups.xsl](corpus2groups.xsl)                           		         Converts a teiCorpus document into a TEI document organized with groups
[tohtml.xsl](tohtml.xsl)                                     			 The main TextGrid specific HTML conversion
[epub-agg.xsl](epub-agg.xsl)
[epub.xsl](epub.xsl)                                             			 The main EPUB conversion
[extractbody.xsl](extractbody.xsl)                               		         Post-processing filter used by ?embedded=true
[frame.xsl](frame.xsl)                                           			 Post-processing filter used by ?simulate=true
[preview.xsl](preview.xsl)                                       			 Preview stylesheet, effect similar to ?simulate=true
[tghtml-common.xsl](tghtml-common.xsl)                           		         Common rules for TEI to HTML
[wadl_documentation-2009-02.xsl](wadl_documentation-2009-02.xsl) 	                 WADL to HTML
---------------------------------------------------------------------------------------- -----------------------------------------------------------------------
