<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns="http://www.w3.org/1999/xhtml"
        xpath-default-namespace="http://www.w3.org/1999/xhtml"
        exclude-result-prefixes="xs"
        version="2.0">

<!-- 
        Extracts the contents of the body of an XHTML document and wraps it in a <div class="body">.
        Used by the html export when embedded=true is specified.        
 -->

<xsl:output method="xhtml" omit-xml-declaration="yes" indent="no"/>
<xsl:param name="extraclass"/>
        
<xsl:template match="node()|@*">
        <xsl:copy>
                <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
</xsl:template>
        
<xsl:template match="/">
        <xsl:choose>
                <xsl:when test="//body">
                        <div class="body {$extraclass}">
                                <xsl:apply-templates select="//style"/>
                                <xsl:apply-templates/>
                        </div>
                </xsl:when>
                <xsl:otherwise>
                        <xsl:copy-of select="/*"/>
                </xsl:otherwise>
        </xsl:choose>
</xsl:template>

<xsl:template match="html">
        <xsl:apply-templates select="body/*"/>
</xsl:template>

<xsl:template match="style">
        <xsl:copy>
                <xsl:copy-of select="@*"/>
                <xsl:if test="not(@scoped)">
                        <xsl:attribute name="scoped">scoped</xsl:attribute>
                </xsl:if>
                <xsl:copy-of select="node()"/>
        </xsl:copy>
</xsl:template>
        
</xsl:stylesheet>
