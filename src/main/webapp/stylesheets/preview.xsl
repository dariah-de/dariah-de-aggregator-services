<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns="http://www.w3.org/1999/xhtml"
        xpath-default-namespace="http://www.w3.org/1999/xhtml"
        exclude-result-prefixes="xs"
        version="2.0">

<!-- 
        Extracts the contents of the body of an XHTML document and wraps it in a textgridrep.de frame
 -->

<xsl:output method="xhtml" />
<xsl:param name="repurl">https://textgridrep.org</xsl:param>
<xsl:param name="extraclass"/>
<xsl:include href="tohtml.xsl"/>

<xsl:template match="/" priority="10">
        <xsl:variable name="embeddedhtml">
                <xsl:next-match/>
        </xsl:variable>
<html class="html aui ltr" dir="ltr" lang="de-DE" xmlns="http://www.w3.org/1999/xhtml" xml:lang="de-DE">
<head>
  <title>Explore - TextGridRep</title>
  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
  <meta http-equiv="language" content="de" />
  <xsl:comment>[if lt IE 9]> &lt;script src="<xsl:value-of select="$repurl"/>/textgrid-theme/js/html5shiv.js" type="text/javascript">&lt;/script> &lt;![endif]</xsl:comment>
  <meta content="text/html; charset=utf-8" http-equiv="content-type" />
  <link href="{$repurl}/textgrid-theme/images/favicon.ico" rel="Shortcut Icon" />
  <link class="lfr-css-file" href=
  "{$repurl}/textgrid-theme/css/aui.css?browserId=other&amp;themeId=textgrid_WAR_textgridtheme&amp;minifierType=css&amp;languageId=de_DE&amp;b=6204&amp;t=1455237879000"
  rel="stylesheet" type="text/css" />
  <link href=
  "{$repurl}/html/css/main.css?browserId=other&amp;themeId=textgrid_WAR_textgridtheme&amp;minifierType=css&amp;languageId=de_DE&amp;b=6204&amp;t=1447839116000"
  rel="stylesheet" type="text/css" />
  <link href=
  "{$repurl}/liferay-tgrep-portlet/css/main.css?browserId=other&amp;themeId=textgrid_WAR_textgridtheme&amp;minifierType=css&amp;languageId=de_DE&amp;b=6204&amp;t=1471649659000"
  rel="stylesheet" type="text/css" />
  <link href=
  "{$repurl}/liferay-tgrep-portlet/css/tg-tei.css?browserId=other&amp;themeId=textgrid_WAR_textgridtheme&amp;minifierType=css&amp;languageId=de_DE&amp;b=6204&amp;t=1471649659000"
  rel="stylesheet" type="text/css" />
  <script src=
	  "{$repurl}/html/js/barebone.jsp?browserId=other&amp;themeId=textgrid_WAR_textgridtheme&amp;colorSchemeId=01&amp;minifierType=js&amp;minifierBundleId=javascript.barebone.files&amp;languageId=de_DE&amp;b=6204&amp;t=1447842062000"
  type="text/javascript">
  </script>
  <script type="text/javascript">
  //<![CDATA[
  Liferay.Portlet.list=["tgrepbrowse_WAR_liferaytgrepportlet","tgrepbasketcounterjs_WAR_liferaytgrepportlet"];
  //]]>
  </script>
  <link class="lfr-css-file" href=
  "{$repurl}/textgrid-theme/css/main.css?browserId=other&amp;themeId=textgrid_WAR_textgridtheme&amp;minifierType=css&amp;languageId=de_DE&amp;b=6204&amp;t=1455237879000"
  rel="stylesheet" type="text/css" />
  <style type="text/css">
  </style>
  <link rel="apple-touch-icon" sizes="57x57" href=
  "{$repurl}/textgrid-theme/images/favicons/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon" sizes="60x60" href=
  "{$repurl}/textgrid-theme/images/favicons/apple-touch-icon-60x60.png" />
  <link rel="apple-touch-icon" sizes="72x72" href=
  "{$repurl}/textgrid-theme/images/favicons/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon" sizes="76x76" href=
  "{$repurl}/textgrid-theme/images/favicons/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon" sizes="114x114" href=
  "{$repurl}/textgrid-theme/images/favicons/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon" sizes="120x120" href=
  "{$repurl}/textgrid-theme/images/favicons/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon" sizes="144x144" href=
  "{$repurl}/textgrid-theme/images/favicons/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon" sizes="152x152" href=
  "{$repurl}/textgrid-theme/images/favicons/apple-touch-icon-152x152.png" />
  <link rel="apple-touch-icon" sizes="180x180" href=
  "{$repurl}/textgrid-theme/images/favicons/apple-touch-icon-180x180.png" />
  <link rel="icon" type="image/png" href="{$repurl}/textgrid-theme/images/favicons/favicon-32x32.png" sizes=
  "32x32" />
  <link rel="icon" type="image/png" href="{$repurl}/textgrid-theme/images/favicons/favicon-194x194.png" sizes=
  "194x194" />
  <link rel="icon" type="image/png" href="{$repurl}/textgrid-theme/images/favicons/favicon-96x96.png" sizes=
  "96x96" />
  <link rel="icon" type="image/png" href="{$repurl}/textgrid-theme/images/favicons/android-chrome-192x192.png" sizes=
  "192x192" />
  <link rel="icon" type="image/png" href="{$repurl}/textgrid-theme/images/favicons/favicon-16x16.png" sizes=
  "16x16" />
  <link rel="manifest" href="{$repurl}/textgrid-theme/images/favicons/manifest.json" />
  <link rel="shortcut icon" href="{$repurl}/textgrid-theme/images/favicons/favicon.ico" />
  <meta name="msapplication-TileColor" content="#ffffff" />
  <meta name="msapplication-TileImage" content="{$repurl}/textgrid-theme/images/favicons/mstile-144x144.png" />
  <meta name="msapplication-config" content="{$repurl}/textgrid-theme/images/favicons/browserconfig.xml" />
  <meta name="theme-color" content="#ffffff" />
</head>
<body class="html_body yui3-skin-sam controls-visible signed-out public-page site">
  <a class="sr-only" href="#content" id="skip-to-content">Zum Inhalt wechseln</a>
  <div class="tg site">
    <header class="tg header">
      <div class="tg header_logo" role="banner">
        <a href="{$repurl}" title="Go to TextGridRep"><img src=
        "{$repurl}/textgrid-theme/images/textgrid-repository-logo.svg" alt="DARIAH Repository" /></a>
      </div>
      <nav class="tg topbox">
        <button class="tg topbox_toggle-nav"><span class="sr-only">Navigation</span></button>
        <ul>
          <li>
            <a href="/de/shelf" class="tg topbox_link -shelf">Regal </a>
          </li>
          <li>
            <a class="tg topbox_link -login" href="{$repurl}/c/portal/login?p_l_id=31372">Anmelden</a>
          </li>
          <li class="tg topbox_language">
            <a class="tg topbox_language" href="#"><span class="sr-only">Switch language to</span>
            English</a>
          </li>
        </ul>
      </nav>
      <nav class="tg nav -has-search" role="navigation">
        <h2 class="sr-only">Navigation</h2>
        <ul aria-label="Site-Seiten" role="menubar">
          <li class="tg nav_item -search">
            <form class="tg search -header" action="{$repurl}/search">
              <fieldset class="tg search_fake-input">
                <div class="tg search_filter"></div>
                <div class="tg search_search">
                  <label class="sr-only" for="search-query-2">Suchbegriff:</label> <input class="tg search_input -header" id=
                  "search-query-2" name="query" placeholder="Suchbegriff" type="search" /> <button class="tg search_submit" type=
                  "submit"><span class="sr-only">Suchen</span></button>
                </div>
              </fieldset><a class="tg search_advanced-search-link" href="/advanced-search">Erweiterte Suche</a>
            </form>
          </li>
          <li class="tg nav_item -current -has-dropdown" id="layout_4" role="presentation">
            <a aria-labelledby="layout_4" href="{$repurl}/browse" class="tg dropdown_toggle -nav" aria-haspopup=
            "true" role="menuitem">Explore</a>
            <ul class="tg dropdown_menu -nav" role="menu">
              <li class="" id="layout_18" role="presentation">
                <a aria-labelledby="layout_18" href="{$repurl}/repository.html" role="menuitem">Repository</a>
              </li>
              <li class="" id="layout_5" role="presentation">
                <a aria-labelledby="layout_5" href="{$repurl}/autor" role="menuitem">…nach Autor</a>
              </li>
              <li class="" id="layout_6" role="presentation">
                <a aria-labelledby="layout_6" href="{$repurl}/genre" role="menuitem">…nach Genre</a>
              </li>
              <li class="" id="layout_7" role="presentation">
                <a aria-labelledby="layout_7" href="{$repurl}/dateityp" role="menuitem">…nach Dateityp</a>
              </li>
              <li class="" id="layout_8" role="presentation">
                <a aria-labelledby="layout_8" href="{$repurl}/projekt" role="menuitem">…nach Projekt</a>
              </li>
            </ul>
          </li>
          <li class="tg nav_item -has-dropdown" id="layout_9" role="presentation">
            <a aria-labelledby="layout_9" href="{$repurl}/hilfe" class="tg dropdown_toggle -nav" aria-haspopup="true"
            role="menuitem">Hilfe</a>
            <ul class="tg dropdown_menu -nav" role="menu">
              <li class="" id="layout_10" role="presentation">
                <a aria-labelledby="layout_10" href="{$repurl}/syntax" role="menuitem">Syntax</a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
    </header>
    <main class="tg main -default" id="content">
      <h1 class="sr-only">Explore</h1>
      <div class="columns-1" id="main-content" role="main">
        <div class="portlet-layout row-fluid">
          <div class="portlet-column portlet-column-only span12" id="column-1">
            <div class="portlet-dropzone portlet-column-content portlet-column-content-only" id="layout-column_column-1">
              <div class=
              "portlet-boundary portlet-boundary_tgrepbrowse_WAR_liferaytgrepportlet_ portlet-static portlet-static-end portlet-borderless"
              id="p_p_id_tgrepbrowse_WAR_liferaytgrepportlet_">
                <span id="p_tgrepbrowse_WAR_liferaytgrepportlet"></span>
                <div class="portlet-borderless-container" style="">
                  <div class="portlet-body">
                    <script type="text/javascript">
                    //<![CDATA[

                    document.addEventListener("DOMContentLoaded", function(event) {
                    $(document).on('click', '.digivoy-button', function(e) {

                    e.preventDefault();

                    var toolUrl = 'https://textgridlab.org/voyant';
                    var documentLinks = [];
                    var settings_serialized = "target=http%3A%2F%2Ftextgridrep.de%2Fvoyant%2F&e=castList&e=desc&e=figure&e=head&e=note&e=speaker&e=stage&e=title";

                    documentLinks.push(filteredTGCrudDataLink('textgrid:jmzc.0', settings_serialized));
                    sendToVoyant(toolUrl, documentLinks.join('\n'));

                    });
                    });

                    //]]>
                    </script>
                    <div class="tgrep wrap">
                      <div class="tgrep sidebar-toggle">
                        <button class="tgrep sidebar-toggle_button -show">Seitenleiste anzeigen</button> <button class=
                        "tgrep sidebar-toggle_button -hide">Seitenleiste verbergen</button>
                      </div>
                      <aside class="tgrep sidebar">
                        <!--
                        <h:panelGroup rendered="false" layout="block" class="egal">
                                <strong>Metadata</strong><br/> <a href="https://textgridlab.org/1.0/tgcrud-public/rest//metadata">raw metadata</a> <hr/> <strong>Tools</strong><br/> <a href="#">Voyant</a><br/> <a href="#">Digilib</a><br/> <a href="#">Mirador</a><br/> <a href="#">DFG-Viewer</a><br/> </h:panelGroup> -->
                        <section class="tgrep sidebar_panel">
                          <h3 class="tgrep sidebar_subheading">Metadaten</h3><!-- TODO: each for agent, pid, etc -->
                          <dl>
                            <dt>Dateityp</dt>
                            <dd>text/xml</dd>
                            <dt>PID</dt>
                            <dd>
                              hdl:11858/00-1734-0000-0001-DCC2-7 <a href=
                              "http://hdl.handle.net/hdl:11858/00-1734-0000-0001-DCC2-7">resolve PID</a> / <a href=
                              "http://hdl.handle.net/hdl:11858/00-1734-0000-0001-DCC2-7?noredirect">PID-metadata</a>
                            </dd>
                          </dl>
                        </section>
                        <section class="tgrep sidebar_panel">
                          <h3 class="tgrep sidebar_subheading">Herunterladen</h3>
                          <ul class="tgrep sidebar_list">
                            <li>
                              <a href="https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:jmzc.0/data">Objekt (TEI)</a>
                            </li>
                            <li>
                              <a href="https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:jmzc.0/metadata">Metadaten (XML)</a>
                            </li>
                            <li>
                              <a href="https://textgridlab.org/1.0/aggregator/epub/textgrid:jmzc.0">E-Book (epub)</a>
                            </li>
                            <li>
                              <a href="https://textgridlab.org/1.0/aggregator/html/textgrid:jmzc.0">HTML</a>
                            </li>
                            <li>
                              <a href="https://textgridlab.org/1.0/aggregator/zip/textgrid:jmzc.0">ZIP</a>
                            </li>
                          </ul>
                        </section>
                        <section class="tgrep sidebar_panel">
                          <h3 class="tgrep sidebar_subheading">Werkzeug</h3>
                          <ul class="tgrep sidebar_list">
                            <li class="tgrep sidebar_item">
                              <a href="#" rel="noindex nofollow" class="tgrep sidebar_link digivoy-button">DigiVoy Basic</a>
                            </li>
                          </ul>
                        </section>
                        <section class="tgrep sidebar_panel">
                          <h3 class="tgrep sidebar_subheading">Inhaltsverzeichnis</h3>
                          <ul xmlns="http://www.w3.org/1999/xhtml" class="toc">
                            <li>
				Not supported
                            </li>
                          </ul>
                        </section>
                      </aside>
                      <main class="tgrep main">
                        <!-- <h1 class="tgrep main_heading">Browse</h1> -->
                        <ul class="tgrep breadcrumbs">
                          <li class="tgrep breadcrumbs_item">
                            <a class="tgrep breadcrumbs_link" href="">Breadcrumbs are not supported in the preview.</a>
                          </li>
                        </ul>
                        <div class="teiXsltView">
                          <xsl:for-each select="$embeddedhtml">
                            <xsl:call-template name="process-tei"/>
                          </xsl:for-each>                          
                        </div>
                      </main>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <form action="#" id="hrefFm" method="post" name="hrefFm"></form>
    </main>
    <footer class="tg footer" role="contentinfo">
      <div class="tg footer_left">
        <ul>
          <li>
            <a class="tg footer_contact" href="https://textgrid.de/de/kontakt/">Kontakt</a>
          </li>
          <li>
            <a href="https://textgrid.de/de/impressum">Impressum</a>
          </li>
          <li>
            <a href="https://creativecommons.org/licenses/by/4.0/deed.de">CC BY 4.0</a>
          </li>
        </ul>
        <ul>
          <li><span>© TextGrid 2016</span></li>
        </ul>
      </div>
      <div class="tg footer_right">
        <div class="tg footer_logos">
          <ul>
            <li>
              <a href="https://de.dariah.eu/" target="_blank"><img class="tg footer_logo" src=
              "{$repurl}/textgrid-theme/images/dariah-de-logo.svg" alt="DARIAH-DE" width="147" height="65" /></a>
            </li>
            <li>
              <a href="http://www.d-grid.de/" target="_blank"><img class="tg footer_logo" src=
              "{$repurl}/textgrid-theme/images/d-grid-logo.png" alt="D-GRID" width="106" height="65" /></a>
            </li>
          </ul>
          <ul>
            <li>
              <a href="http://www.tei-c.org/" target="_blank"><img class="tg footer_logo" src=
              "{$repurl}/textgrid-theme/images/tei-logo.svg" alt="TEI - Text Encoding Initiative" width="59" height=
              "65" /></a>
            </li>
            <li>
              <a href="http://bmbf.de/" target="_blank"><img class="tg footer_logo -tall" src=
              "{$repurl}/textgrid-theme/images/bmbf-logo.svg" alt=
              "Gefördert vom BMBF - Bundesministerium für Bildung und Forschung" width="124" height="86" /></a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  </div>
  <script type="text/javascript">
  //<![CDATA[
  Liferay.Util.addInputFocus();
  //]]>
  </script> 
  <script src=
  "{$repurl}/textgrid-theme/js/main.js?browserId=other&amp;minifierType=js&amp;languageId=de_DE&amp;b=6204&amp;t=1455237879000"
  type="text/javascript">
  </script> 
  <script type="text/javascript">
  </script> 
  <script src="{$repurl}/textgrid-theme/js/jquery.js" type="text/javascript">
  </script> 
  <script src="{$repurl}/textgrid-theme/js/theme.js" type="text/javascript">
  </script> <!-- Piwik --> 
  <script type="text/javascript">
  //<![CDATA[
  var pkBaseURL=(("https:"==document.location.protocol)?"https://piwik.gwdg.de/":"http://piwik.gwdg.de/");document.write(unescape("%3Cscript src='"+pkBaseURL+"piwik.js' type='text/javascript'%3E%3C/script%3E"));
  //]]>
  </script>
  <script type="text/javascript">
  //<![CDATA[
  try{var piwikTracker=Piwik.getTracker(pkBaseURL+"piwik.php",168);piwikTracker.trackPageView();piwikTracker.enableLinkTracking()}catch(err){};
  //]]>
  </script><noscript>
  <p><img src="http://piwik.gwdg.de/piwik.php?idsite=168" style="border:0" alt="" /></p></noscript> 
  <!-- End Piwik Tracking Code -->
  <div class=
  "portlet-boundary portlet-boundary_tgrepbasketcounterjs_WAR_liferaytgrepportlet_ portlet-static portlet-static-end portlet-borderless"
  id="p_p_id_tgrepbasketcounterjs_WAR_liferaytgrepportlet_">
    <span id="p_tgrepbasketcounterjs_WAR_liferaytgrepportlet"></span>
    <div class="portlet-borderless-container" style="">
      <div class="portlet-body">
        <script>
        <![CDATA[
        document.addEventListener("DOMContentLoaded",function(a){setBasketCount(0)});function setBasketCount(a){[].forEach.call(document.getElementsByClassName("topbox_shelf-count"),function(b){b.textContent=a})};
        ]]>
        </script>
      </div>
    </div>
  </div>
</body>
</html>
      
</xsl:template>
        
  
<xsl:template name="process-tei">
        <xsl:choose>
                <xsl:when test="//body">
                        <div class="body {$extraclass}">
                                <xsl:apply-templates select="//style" mode="embed"/>                                
                                <xsl:copy-of select="//body/*"/>
                        </div>
                </xsl:when>
                <xsl:otherwise>
                        <xsl:copy-of select="/*"/>
                </xsl:otherwise>
        </xsl:choose>
</xsl:template>
        
<xsl:template match="style" mode="embed">
        <xsl:copy>
                <xsl:copy-of select="@*"/>                
                <xsl:if test="not(@scoped)">
                        <xsl:attribute name="scoped">scoped</xsl:attribute>
                </xsl:if>
                <xsl:copy-of select="node()"/>
        </xsl:copy>
</xsl:template>
        
        
</xsl:stylesheet>