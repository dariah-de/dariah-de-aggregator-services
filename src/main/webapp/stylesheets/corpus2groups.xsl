<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.tei-c.org/ns/1.0"
        xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs"
        version="2.0">

        <xsl:output method="xml" encoding="UTF-8"/>

        <xsl:template match="node()|@*">
                <xsl:copy>
                        <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>
        </xsl:template>

        <xsl:template match="/teiCorpus">
                <TEI>
                        <xsl:apply-templates select="@*"/>
                        <xsl:apply-templates select="teiHeader"/>
                        <text>
                                <group>
                                <xsl:apply-templates select="teiHeader/following-sibling::*"/>
                                </group>
                        </text>
                </TEI>
        </xsl:template>

        <xsl:template match="teiCorpus|TEI">
                        <group>
                                <xsl:apply-templates select="@*"/>
                                <xsl:for-each select="(teiHeader//title)[1]">
                                        <head>
                                                <xsl:apply-templates/>
                                        </head>
                                </xsl:for-each>
                                <xsl:apply-templates select="teiHeader/following-sibling::*"/>
                        </group>
        </xsl:template>


</xsl:stylesheet>
