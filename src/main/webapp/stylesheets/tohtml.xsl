<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" 
        xmlns:xi="http://www.w3.org/2001/XInclude" 
        xmlns:tei="http://www.tei-c.org/ns/1.0"
        xmlns:ge="http://www.tei-c.org/ns/geneticEditions" 
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:tgs="http://www.textgrid.info/namespaces/middleware/tgsearch"
        xmlns:a="http://textgrid.de/namespaces/aggregator"
        exclude-result-prefixes="xs tei tgs xi f ge a"
        xpath-default-namespace="http://www.tei-c.org/ns/1.0" 
        xmlns:f="http://www.faustedition.net/ns"        
        version="2.0">
   
   <xsl:import href="../tei-stylesheets/html/html.xsl"/>
   <xsl:include href="tghtml-common.xsl"/>
   
                
        

    <xsl:param name="graphicsURLPattern">http://textgridlab.org/1.0/tgcrud/rest/@URI@/data</xsl:param>
    <xsl:param name="linkURLPattern">http://textgridlab.org/1.0/tgcrud/rest/@URI@/data</xsl:param>      
    <xsl:function name="a:resolve-uri">
        <xsl:param name="uri"/>
        <xsl:param name="pattern"/>        
        <xsl:choose>
                <xsl:when test="starts-with($uri, 'textgrid:')">
                        <xsl:variable name="split" select="tokenize($uri, '#')"/>                        
                        <xsl:variable name="uriResolved" select="replace($pattern, '@URI@', $split[1])"/>
                        <xsl:choose>
                                <xsl:when test="contains($pattern,'@ID@')">
                                        <xsl:value-of select="replace($uriResolved, '@ID@', $split[2])"/>
                                </xsl:when>
                                <xsl:when test="$split[2]">
                                        <xsl:value-of select="concat($uriResolved, '#', $split[2])"/>
                                </xsl:when>
                                <xsl:otherwise>
                                        <xsl:value-of select="$uriResolved"/>
                                </xsl:otherwise>
                        </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                        <xsl:value-of select="$uri"/>
                </xsl:otherwise>
        </xsl:choose>        
    </xsl:function>

    <!-- FIXME TODO document params to stylesheet -->
    <xsl:param name="verbose">true</xsl:param>
    <xsl:param name="graphicsPrefix"/>
    <xsl:param name="graphicsSuffix"/>
    <xsl:param name="parentURL"/>
    <xsl:param name="URLPREFIX"/>
    <xsl:param name="documentationLanguage"/>
    <xsl:param name="homeLabel">TextGridRep</xsl:param>
    <xsl:param name="homeURL">http://www.textgridrep.de/</xsl:param>
    <xsl:param name="homeWords">TextGrid</xsl:param>
    <xsl:param name="institution"/>
    <xsl:param name="searchURL">/tgrep/index.html</xsl:param>
    <xsl:param name="linkPanel">false</xsl:param>
    <xsl:param name="numberHeadings">false</xsl:param>
    <xsl:param name="autoToc">false</xsl:param>
    <xsl:param name="autoHead">false</xsl:param>
    <xsl:param name="showTitleAuthor">false</xsl:param>
    <xsl:param name="cssFile"/>
    <xsl:param name="onlyTOC"/>

    <xsl:output method="xhtml"/>
        
    <xsl:param name="requestedID">            
            <xsl:if test="id($ID)">
                    <xsl:value-of select="$ID"/>
            </xsl:if>            
    </xsl:param>
	
    <xsl:variable name="topNavigationPanel">false</xsl:variable>
        
    <!-- Copied from textstucture.xsl to remove the extra heading from the titleStmt -->
    <xsl:template name="stdheader">
        <xsl:param name="title">(no title)</xsl:param>
    </xsl:template>
        
    <xsl:template match="/">
            <xsl:choose>
                    <xsl:when test="$onlyTOC">
                            <xsl:message>Tunelling ...</xsl:message>
                            <ul class="toc">
                                    <xsl:apply-templates mode="toc">
                                            <xsl:with-param name="currentDepth" tunnel="yes" select="0"/>
                                    </xsl:apply-templates>
                            </ul>
                    </xsl:when>
                    <xsl:otherwise>
                            <xsl:next-match/>
                    </xsl:otherwise>
            </xsl:choose>            
    </xsl:template>
        
    <xsl:template mode="toc" match="node()">
            <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template mode="toc" match="div[head]">
            <xsl:param name="currentDepth" tunnel="yes"/>            
            <li>
                    <a href="#{if (@xml:id) then (@xml:id) else (head/@xml:id)[1]}">
                            <xsl:apply-templates select="head[1]/node()"/>
                    </a>
                    <xsl:if test="$currentDepth &lt; $tocDepth and (descendant::div[head] or descendant::TEI)">
                            <ul>
                                    <xsl:apply-templates mode="#current">
                                            <xsl:with-param name="currentDepth" tunnel="yes" select="$currentDepth + 1"/>
                                    </xsl:apply-templates>
                            </ul>
                    </xsl:if>
            </li>
    </xsl:template>
        
    <xsl:template mode="toc" match="TEI[parent::teiCorpus][@xml:id]|teiCorpus[parent::teiCorpus][@xml:id]">
            <xsl:param name="currentDepth" as="xs:integer" tunnel="yes"></xsl:param>
            <li>
                    <a href="#{@xml:id}">
                            <xsl:call-template name="generateTitle"/>
                    </a>
                    <xsl:if test="$currentDepth &lt; $tocDepth and (descendant::div[head] or descendant::TEI)">
                            <ul>
                                    <xsl:apply-templates mode="#current">
                                            <xsl:with-param name="currentDepth" tunnel="yes" select="$currentDepth + 1"/>
                                    </xsl:apply-templates>
                            </ul>
                    </xsl:if>
            </li>
    </xsl:template>
       
    <xsl:template name="generateTitle">
        <xsl:variable name="result">
            <xsl:choose>
                <xsl:when test="$useHeaderFrontMatter='true' and ancestor-or-self::tei:TEI/tei:text/tei:front//tei:docTitle">
                    <xsl:apply-templates select="ancestor-or-self::tei:TEI/tei:text/tei:front//tei:docTitle/tei:titlePart"/>
                </xsl:when>
                <xsl:when test="$useHeaderFrontMatter='true' and ancestor-or-self::tei:teiCorpus/tei:text/tei:front//tei:docTitle">
                    <xsl:apply-templates select="ancestor-or-self::tei:teiCorpus/tei:text/tei:front//tei:docTitle/tei:titlePart"/>
                </xsl:when>
                <xsl:when test="self::tei:teiCorpus">
                    <xsl:apply-templates select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[not(@type='subordinate')]"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:for-each select="ancestor-or-self::tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt">
                        <xsl:choose>
                            <xsl:when test="tei:title[@type='main']">
                                <xsl:apply-templates select="tei:title"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:apply-templates select="tei:title"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="normalize-space(string($result)) = ''">(no title)</xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="$result"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
                
    <xsl:template name="copyrightStatement">
        <xsl:choose>
            <xsl:when test="/*/teiHeader/fileDesc/publicationStmt/availability">
                <div class="copyrightStatement">
                    <xsl:apply-templates select="/*/teiHeader/fileDesc/publicationStmt/availability"/>
                </div>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- copied and simplyfied from TEI's xhtml2/textstructure.xsl  to adapt for our needs -->
    <xsl:template name="stdfooter">
        <xsl:param name="style" select="'plain'"/>
        <xsl:param name="file"/>
        <div class="stdfooter">
            <xsl:if test="$linkPanel='true'">
                <div class="footer">
                    <xsl:if test="not($parentURL='')">
                        <a class="{$style}" href="{$parentURL}">
                            <xsl:value-of select="$parentWords"/>
                        </a> | </xsl:if>
                    <a class="{$style}" href="{$homeURL}">
                        <xsl:value-of select="$homeWords"/>
                    </a>
                    <xsl:if test="$searchURL">
                        <xsl:text>| </xsl:text>
                        <a class="{$style}" href="{$searchURL}">
                            <xsl:call-template name="i18n">
                                <xsl:with-param name="word">searchWords</xsl:with-param>
                            </xsl:call-template>
                        </a>
                    </xsl:if>
                    <xsl:if test="$feedbackURL"> | <a class="{$style}" href="{$feedbackURL}">
                            <xsl:call-template name="i18n">
                                <xsl:with-param name="word">feedbackWords</xsl:with-param>
                            </xsl:call-template>
                        </a>
                    </xsl:if>
                </div>
            </xsl:if>
            <address>
                <xsl:call-template name="copyrightStatement"/>
                <xsl:comment>
                    <xsl:text>
	  Generated </xsl:text>
                    <xsl:if test="not($masterFile='index')">
                        <xsl:text>from </xsl:text>
                        <xsl:value-of select="$masterFile"/>
                    </xsl:if>
                    <xsl:text> using an XSLT version </xsl:text>
                    <xsl:value-of select="system-property('xsl:version')"/> stylesheet
	  based on <xsl:value-of select="$teixslHome"/>
	  processed using <xsl:value-of select="system-property('xsl:vendor')"/>
	  on <xsl:sequence select="tei:whatsTheDate()"/>.
                </xsl:comment>
            </address>
        </div>
    </xsl:template>
      
    <xsl:template match="tei:head">
        <xsl:choose>
                <xsl:when test="$ID != '' and starts-with(local-name(..), 'div') and ../@xml:id = $ID">
                        <h2>
                                <xsl:call-template name="makeRendition"/>
                                <xsl:apply-templates select="." mode="makeheading"/>
                        </h2>
                </xsl:when>
                <xsl:otherwise>
                        <xsl:next-match/>
                </xsl:otherwise>
        </xsl:choose>            
    </xsl:template>
        
        
        <!-- 
                taken from html_textstructure.xsl in 7.32.
                
                There is an issue with rendering <h[1-6]> elements: They don't inherit the @rend attributes etc.,
                instead a <span> is rendered which bears the class etc. 
                
                We want the classes on our <h?> instead so we can do our runIn trick via css, so there.
        
        -->
        <xsl:template name="divContents">
                <xsl:param name="depth"/>
                <xsl:param name="nav">false</xsl:param>
                <xsl:variable name="ident">
                        <xsl:apply-templates mode="ident" select="."/>
                </xsl:variable>
                <xsl:variable name="headertext">
                        <xsl:call-template name="header">
                                <xsl:with-param name="display">full</xsl:with-param>
                        </xsl:call-template>
                </xsl:variable>
                
                <xsl:choose>
                        <xsl:when test="parent::tei:*/tei:match(@rend,'multicol')">
                                <td style="vertical-align:top;">
                                        <xsl:if test="not($depth = '')">
                                                <xsl:element name="h{$depth + $divOffset}">
                                                        <xsl:for-each select="tei:head[1]">		
                                                                <xsl:call-template name="makeRendition">
                                                                        <xsl:with-param name="default">false</xsl:with-param>
                                                                </xsl:call-template>
                                                        </xsl:for-each>
                                                        <xsl:if test="@xml:id">
                                                                <xsl:call-template name="makeAnchor"/>
                                                        </xsl:if>
                                                        <xsl:call-template name="header">
                                                                <xsl:with-param name="display">full</xsl:with-param>
                                                        </xsl:call-template>
                                                        <xsl:call-template name="sectionHeadHook"/>
                                                </xsl:element>
                                        </xsl:if>
                                        <xsl:apply-templates/>
                                </td>
                        </xsl:when>
                        <xsl:when test="tei:match(@rend,'multicol')">
                                <xsl:apply-templates select="*[not(local-name(.)='div')]"/>
                                <table>
                                        <tr>
                                                <xsl:apply-templates select="tei:div"/>
                                        </tr>
                                </table>
                        </xsl:when>
                        <xsl:when test="tei:match(@rend,'nohead') or $headertext=''">
                                <xsl:apply-templates/>
                        </xsl:when>
                        <xsl:otherwise>
                                <xsl:if test="not($depth = '')">
                                        <xsl:variable name="Heading">
                                                <xsl:variable name="ename" select="if (number($depth)+$divOffset &gt;6) then 'div'
                                                        else
                                                        concat('h',number($depth)+$divOffset)">
                                                </xsl:variable>
                                                <xsl:call-template name="splitHTMLBlocks">
                                                        <xsl:with-param name="element" select="$ename"/>
                                                        <xsl:with-param name="content">
                                                                <xsl:apply-templates select="tei:head"/>
                                                                <xsl:call-template name="sectionHeadHook"/>
                                                                <xsl:copy-of select="$headertext"/>	 
                                                        </xsl:with-param>
                                                        <xsl:with-param name="copyid">false</xsl:with-param>
                                                        <xsl:with-param name="class">
                                                                <!-- 
                                                                        This param wasn't there in Rahtz' stylesheets, which lets it default to
                                                                        false in the called template, which causes makeRendition to not use @rend.
                                                                        So explicitely hack this in here.
                                                                        
                                                                        -tv
                                                                
                                                                -->
                                                                <xsl:variable name="tmp">
                                                                        <tmp>
                                                                                <xsl:for-each select="(tei:head)[1]">
                                                                                        <xsl:call-template name="makeRendition"/>
                                                                                </xsl:for-each>
                                                                        </tmp>
                                                                </xsl:variable>
                                                                <xsl:value-of select="$tmp/*/@class"/>
                                                        </xsl:with-param>
                                                </xsl:call-template>
                                        </xsl:variable>
                                        <xsl:choose>
                                                <xsl:when test="$outputTarget='html5' and number($depth)  &lt; 1">
                                                        <header>
                                                                <xsl:copy-of select="$Heading"/>
                                                        </header>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                        <xsl:copy-of select="$Heading"/>
                                                </xsl:otherwise>
                                        </xsl:choose>
                                        
                                        <xsl:if test="$topNavigationPanel='true' and
                                                $nav='true'">
                                                <xsl:element name="{if ($outputTarget='html5') then 'nav'
                                                        else 'div'}">
                                                        <xsl:call-template name="xrefpanel">
                                                                <xsl:with-param name="homepage" select="concat($masterFile,$standardSuffix)"/>
                                                                <xsl:with-param name="mode" select="local-name(.)"/>
                                                        </xsl:call-template>
                                                </xsl:element>
                                        </xsl:if>
                                </xsl:if>
                                <xsl:apply-templates/>
                                <xsl:if test="$bottomNavigationPanel='true' and
                                        $nav='true'">
                                        <xsl:element name="{if ($outputTarget='html5') then 'nav' else
                                                'div'}">	       
                                                <xsl:call-template name="xrefpanel">
                                                        <xsl:with-param name="homepage" select="concat($masterFile,$standardSuffix)"/>
                                                        <xsl:with-param name="mode" select="local-name(.)"/>
                                                </xsl:call-template>
                                        </xsl:element>
                                </xsl:if>
                        </xsl:otherwise>
                </xsl:choose>
        </xsl:template>
     
        
        
        <!-- This has been taken from xhtml2/figures.xsl to allow handling textgrid: and hdl: URIs in graphics references. I have only adapted the definition of $File at the beginning of the template, the rest has been copypasted verbatim. -->
        <doc xmlns="http://www.oxygenxml.com/ns/doc/xsl">
                <desc>[html] display graphic file</desc>
        </doc>
        <xsl:template name="showGraphic">
                <xsl:variable name="File">
                        <xsl:choose>
                                <xsl:when test="self::tei:binaryObject"/>
                                <xsl:when test="@url and ( starts-with(@url, 'textgrid:') or starts-with(@url, 'hdl:'))">
                                        <xsl:value-of select="replace($graphicsURLPattern, '@URI@', @url)"/>
                                </xsl:when>
                                <xsl:when test="@url">
                                        <xsl:value-of select="@url"/>
                                        <xsl:if test="not(contains(@url,'.'))">
                                                <xsl:value-of select="$graphicsSuffix"/>
                                        </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>
                                        <xsl:message>Cannot work out how to do a graphic, needs a URL</xsl:message>
                                </xsl:otherwise>
                        </xsl:choose>
                </xsl:variable>
                <xsl:variable name="Alt">
                        <xsl:choose>
                                <xsl:when test="tei:desc">
                                        <xsl:for-each select="tei:desc">
                                                <xsl:apply-templates mode="plain"/>
                                        </xsl:for-each>
                                </xsl:when>
                                <xsl:when test="tei:figDesc">
                                        <xsl:for-each select="tei:figDesc">
                                                <xsl:apply-templates mode="plain"/>
                                        </xsl:for-each>
                                </xsl:when>
                                <xsl:when test="tei:head">
                                        <xsl:value-of select="tei:head/text()"/>
                                </xsl:when>
                                <xsl:when test="parent::tei:figure/tei:figDesc">
                                        <xsl:for-each select="parent::tei:figure/tei:figDesc">
                                                <xsl:apply-templates mode="plain"/>
                                        </xsl:for-each>
                                </xsl:when>
                                <xsl:when test="parent::tei:figure/tei:head">
                                        <xsl:value-of select="parent::tei:figure/tei:head/text()"/>
                                </xsl:when>
                        </xsl:choose>
                </xsl:variable>
                <xsl:choose>
                        <xsl:when test="$showFigures='true'">
                                <xsl:choose>
                                        <xsl:when test="@type='thumbnail'"/>
                                        <xsl:when test="starts-with(@mimeType, 'video')">
                                                <video src="{$graphicsPrefix}{$File}"
                                                        controls="controls">
                                                        <xsl:if test="../tei:graphic[@type='thumbnail']">
                                                                <xsl:attribute name="poster">
                                                                        <xsl:value-of select="../tei:graphic[@type='thumbnail']/@url"/>
                                                                </xsl:attribute>
                                                        </xsl:if>
                                                </video>
                                        </xsl:when>
                                        <xsl:otherwise>
                                                <xsl:variable name="sizes">
                                                        <xsl:if test="@width">
                                                                <xsl:text> width:</xsl:text>
                                                                <xsl:value-of select="@width"/>
                                                                <xsl:text>;</xsl:text>
                                                        </xsl:if>
                                                        <xsl:if test="@height">
                                                                <xsl:text> height:</xsl:text>
                                                                <xsl:value-of select="@height"/>
                                                                <xsl:text>;</xsl:text>
                                                        </xsl:if>
                                                </xsl:variable>
                                                <xsl:variable name="i">
                                                        <img>
                                                                <xsl:attribute name="src">
                                                                        <xsl:choose>
                                                                                <xsl:when test="self::tei:binaryObject">
                                                                                        <xsl:text>data:</xsl:text>
                                                                                        <xsl:value-of select="@mimetype"/>
                                                                                        <xsl:variable name="enc" select="if (@encoding) then @encoding else 'base64'"/>
                                                                                        <xsl:text>;</xsl:text>
                                                                                        <xsl:value-of select="$enc"/>
                                                                                        <xsl:text>,</xsl:text>
                                                                                        <xsl:copy-of select="text()"/>
                                                                                </xsl:when>
                                                                                <xsl:otherwise>
                                                                                        <xsl:value-of
                                                                                                select="concat($graphicsPrefix,$File)"/>
                                                                                </xsl:otherwise>
                                                                        </xsl:choose>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="alt">
                                                                        <xsl:value-of select="$Alt"/>
                                                                </xsl:attribute>
                                                                <xsl:call-template name="imgHook"/>
                                                                <xsl:if test="@xml:id">
                                                                        <xsl:attribute name="id">
                                                                                <xsl:value-of select="@xml:id"/>
                                                                        </xsl:attribute>
                                                                </xsl:if>
                                                                <xsl:call-template name="makeRendition"/>
                                                        </img>
                                                </xsl:variable>
                                                <xsl:for-each select="$i/*">
                                                        <xsl:copy>
                                                                <xsl:copy-of select="@*[not(name()='style')]"/>
                                                                <xsl:choose>
                                                                        <xsl:when test="$sizes=''">
                                                                                <xsl:copy-of select="@style"/>
                                                                        </xsl:when>
                                                                        <xsl:when test="not(@style)">
                                                                                <xsl:attribute name="style" select="$sizes"/>
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                                                <xsl:attribute name="style"
                                                                                        select="concat(@style,';' ,$sizes)"/>
                                                                        </xsl:otherwise>
                                                                </xsl:choose>
                                                        </xsl:copy>
                                                </xsl:for-each>
                                        </xsl:otherwise>
                                </xsl:choose>	  
                        </xsl:when>
                        <xsl:otherwise>
                                <div class="altfigure">
                                        <xsl:call-template name="i18n">
                                                <xsl:with-param name="word">figureWord</xsl:with-param>
                                        </xsl:call-template>
                                        <xsl:text> </xsl:text>
                                        <xsl:for-each select="self::tei:figure|parent::tei:figure">
                                                <xsl:number count="tei:figure[tei:head]" level="any"/>
                                        </xsl:for-each>
                                        <xsl:text> </xsl:text>
                                        <xsl:value-of select="$File"/>
                                        <xsl:text> [</xsl:text>
                                        <xsl:value-of select="$Alt"/>
                                        <xsl:text>] </xsl:text>
                                </div>
                        </xsl:otherwise>
                </xsl:choose>
        </xsl:template>
        
        


    <!-- Spezialbehandlung für die Zeno-Variante der Fußnoten -->
    <xsl:template match="div[@type = 'footnotes']//note">
        <div class="note">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="note[@target]//ref[not(@target or @url)]">
        <a href="{ancestor::note[@target][1]/@target}">
            <xsl:if test="@xml:id">
                <xsl:attribute name="xml:id" select="@xml:id"/>
            </xsl:if>
            <xsl:apply-templates/>
        </a>
    </xsl:template>


    <xsl:template match="/tei:teiCorpus">
        <html>
            <xsl:call-template name="addLangAtt"/>
            <head>
                <title>
                    <xsl:apply-templates select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/text()"/>
                </title>
                <xsl:call-template name="includeCSS"/>
                <xsl:call-template name="cssHook"/>
            </head>
            <body class="simple" id="TOP">
                <xsl:call-template name="bodyHook"/>
                <xsl:call-template name="bodyJavascriptHook"/>
                <div class="stdheader">
                    <xsl:call-template name="stdheader">
                        <xsl:with-param name="title">
                            <xsl:apply-templates select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </div>
                <xsl:call-template name="corpusBody"/>
                <xsl:call-template name="stdfooter"/>
                <xsl:call-template name="bodyEndHook"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template name="corpusBody">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="publicationStmt/availability/p[starts-with(., ' Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten')]">
        <p>
            Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
            davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
            www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
            Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
            Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
            allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen).
        </p>
    </xsl:template>
        
        <doc xmlns="http://www.oxygenxml.com/ns/doc/xsl">
                <desc>[common] create external link. from commons-linking.xsl
                        <param name="ptr">ptr</param>
                        <param name="dest">dest</param>
                        <param name="class">class</param>
                </desc>
        </doc>
        <xsl:template name="makeExternalLink">
                <xsl:param name="ptr" as="xs:boolean"  select="false()"/>
                <xsl:param name="dest"/>
                <xsl:param name="title"/>
                <xsl:param name="class">link_<xsl:value-of select="local-name(.)"/>                        
                </xsl:param>
                <xsl:element name="{$linkElement}" namespace="{$linkElementNamespace}">
                        <xsl:if test="(self::tei:ptr or self::tei:ref) and @xml:id">
                                <xsl:attribute name="id" select="@xml:id"/>
                        </xsl:if>
                        <xsl:if test="$title">
                                <xsl:attribute name="title" select="$title"/>
                        </xsl:if>
                        <xsl:call-template name="makeRendition">
                                <xsl:with-param name="default" select="$class"/>
                        </xsl:call-template>
                        <xsl:if test="@type and not($outputTarget='epub3' or $outputTarget='html5')">
                                <xsl:attribute name="type">
                                        <xsl:value-of select="@type"/>
                                </xsl:attribute>
                        </xsl:if>
                        <xsl:attribute name="{$linkAttribute}" namespace="{$linkAttributeNamespace}">
                                <xsl:variable name="destWithFrag">
                                        <xsl:value-of select="$dest"/>
                                        <xsl:if test="contains(@from,'id (')">
                                                <xsl:text>#</xsl:text>
                                                <xsl:value-of select="substring(@from,5,string-length(normalize-space(@from))-1)"/>
                                        </xsl:if>
                                </xsl:variable>                                
                                <xsl:sequence select="a:resolve-uri($destWithFrag, $linkURLPattern)"/>
                        </xsl:attribute>
                        <xsl:choose>
                                <xsl:when test="@n">
                                        <xsl:attribute name="title"  namespace="{$linkAttributeNamespace}">
                                                <xsl:value-of select="@n"/>
                                        </xsl:attribute>
                                </xsl:when>
                        </xsl:choose>
                        <xsl:call-template name="xrefHook"/>
                        <xsl:choose>
                                <xsl:when test="$dest=''">??</xsl:when>
                                <xsl:when test="$ptr">
                                        <xsl:element name="{$urlMarkup}" namespace="{$linkElementNamespace}">
                                                <xsl:choose>
                                                        <xsl:when test="starts-with($dest,'mailto:')">
                                                                <xsl:value-of select="substring-after($dest,'mailto:')"/>
                                                        </xsl:when>
                                                        <xsl:when test="starts-with($dest,'file:')">
                                                                <xsl:value-of select="substring-after($dest,'file:')"/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                                <xsl:value-of select="$dest"/>
                                                        </xsl:otherwise>
                                                </xsl:choose>
                                        </xsl:element>
                                </xsl:when>
                                <xsl:otherwise>
                                        <xsl:apply-templates/>
                                </xsl:otherwise>
                        </xsl:choose>
                </xsl:element>
        </xsl:template>
        

   
   <xsl:template match="g">
           <xsl:variable name="def" select="document(@ref)"/>
           <span>
                   <xsl:call-template name="microdata"/>
                   <xsl:call-template name="makeRendition">
                           <xsl:with-param name="default">g</xsl:with-param>
                   </xsl:call-template>
                   <xsl:if test="$def/desc">
                           <xsl:attribute name="title" select="$def/desc"/>
                   </xsl:if>
                   <xsl:if test="$def/(name|charName)">
                           <xsl:attribute name="data-name" select="$def/(name|charName)"/>
                   </xsl:if>
                   <xsl:choose>
                           <xsl:when test="text() and string-length(text()) > 0">
                                   <xsl:value-of select="text()"/>
                           </xsl:when>
                           <xsl:when test="$def/mapping[string-length(.) > 0]">
                                   <xsl:value-of select="$def/mapping[string-length(.) > 0][1]"/>
                           </xsl:when>
                           <xsl:otherwise>
                                   <xsl:text>⯑</xsl:text>
                                   <xsl:comment>could not expand character <xsl:value-of select="@ref"/></xsl:comment>
                           </xsl:otherwise>
                   </xsl:choose>
           </span>
   </xsl:template>
        
   <xsl:template match="handShift">
           <xsl:variable name="handNote" select="document(@new)"/>
           <span class="handShift" data-new="{@new}" title="✍ {string($handNote)}">✍</span>
   </xsl:template>

   <!-- Faust GE stuff: TODO move to external stylesheet -->
        
   <xsl:template name="startHook">                      
           <xsl:apply-templates select="ge:document"/>           
   </xsl:template>   
     
   <xsl:template match="ge:document">         
           <xsl:apply-templates/>
   </xsl:template>
        
   <xsl:template match="ge:surface">
           <xsl:call-template name="makeBlock">
                   <xsl:with-param name="element">div</xsl:with-param>
                   <xsl:with-param name="style">surface</xsl:with-param>
           </xsl:call-template>
   </xsl:template>
        
   <xsl:template match="ge:line">
           <xsl:call-template name="makeBlock">
                   <xsl:with-param name="element">div</xsl:with-param>
                   <xsl:with-param name="style">line ge-line</xsl:with-param>
           </xsl:call-template>
   </xsl:template>
        
   <xsl:template match="f:vspace">
           <br class="vspace" data-quantity="{@quantity}" data-unit="{@unit}" style="line-height: {@quantity * 100}%"/>
   </xsl:template>
        
   <xsl:template match="f:st">
           <xsl:call-template name="makeInline">
                   <xsl:with-param name="style">strikethrough</xsl:with-param>
           </xsl:call-template>
   </xsl:template>
           
   <xsl:template match="f:*">
           <xsl:call-template name="makeInline">
                   <xsl:with-param name="style" select="concat('faust_', local-name(.))"/>
           </xsl:call-template>
   </xsl:template>

        <xsl:template match="ge:*">
                <xsl:call-template name="makeInline">
                        <xsl:with-param name="style" select="concat('ge_', local-name(.))"/>
                </xsl:call-template>
        </xsl:template>
        
        
        <!-- taken from html.xsl, added the option to override the element via parameter -->
        <xsl:template name="makeBlock">
                <xsl:param name="style"/>
                <xsl:param name="element" select="if (tei:isInline(.)) then 'span' else 'div'"/>
                <xsl:element name="{$element}">
                        <xsl:call-template name="microdata"/>
                        <xsl:call-template name="makeRendition">
                                <xsl:with-param name="default" select="$style"/>
                        </xsl:call-template>
                        <xsl:if test="@xml:id">
                                <xsl:attribute name="id">
                                        <xsl:value-of select="@xml:id"/>
                                </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="@hand">
                                <span class="handShift" data-new="{@hand}" title="✍ {string(document(@hand))}">✍</span>
                        </xsl:if>                        
                        <xsl:apply-templates/>                        
                </xsl:element>
        </xsl:template>


        <!-- Enzyclopaediae -->
        <xsl:template name="cssHook">
                <!-- Firefox doesn't support display: run-in :-( -->
                <style type="text/css">                        
.runIn    { font: inherit; font-weight: bold; display: inline; }
.runIn:before { display: block; margin-top: 1.5ex; content: "" }
.runIn +p { display: inline; }
                </style>
        </xsl:template>
        
  </xsl:stylesheet>
