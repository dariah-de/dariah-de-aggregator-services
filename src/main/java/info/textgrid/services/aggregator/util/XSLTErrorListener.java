package info.textgrid.services.aggregator.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;

/**
 * The XSLT error listener used by the TEI transformations. Will log everything
 * to the given logger. Fatal errors will be thrown.
 * 
 * @author tv
 * 
 */
public class XSLTErrorListener implements ErrorListener {
	private final Logger logger;

	public XSLTErrorListener(final Logger logger) {
		this.logger = logger;
	}

	@Override
	public void warning(final TransformerException exception)
			throws TransformerException {
		logger.log(Level.INFO,
				"XSLT warning: " + exception.getMessageAndLocation(), exception);
	}

	@Override
	public void fatalError(final TransformerException exception)
			throws TransformerException {
		logger.log(Level.WARNING,
				"Fatal XSLT error: " + exception.getLocationAsString(),
				exception);
		throw exception;
	}

	@Override
	public void error(final TransformerException exception)
			throws TransformerException {
		logger.log(Level.WARNING,
				"XSLT error: " + exception.getMessageAndLocation(), exception);
	}
}