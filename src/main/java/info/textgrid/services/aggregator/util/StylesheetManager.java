package info.textgrid.services.aggregator.util;

import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.services.aggregator.ITextGridRep;
import info.textgrid.services.aggregator.ITextGridRep.TGOSupplier;
import info.textgrid.services.aggregator.html.TGUriResolver;

import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.text.MessageFormat;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XsltCompiler;
import net.sf.saxon.s9api.XsltExecutable;

import com.google.common.base.Optional;
import com.google.common.base.Throwables;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheStats;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.google.common.cache.Weigher;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

/**
 * Manages and caches XSLT stylesheets across service calls, also maintains
 * processor policies etc. All stylesheets invoked throughout the service should
 * be invoked through the manager's
 * {@link #getStylesheet(URI, Optional, boolean, boolean)} method.
 */
public class StylesheetManager {
	private static final Logger logger = Logger
			.getLogger(StylesheetManager.class.getCanonicalName());
	public final Processor xsltProcessor;
	private Cache<URI, XsltExecutable> stylesheets;
	private ITextGridRep repository;
	private ServletContext servlet;
	private Set<URI> importantStylesheets = Sets.newHashSet();

	/**
	 * Initializes a stylesheet manager.
	 *
	 * @param servlet
	 *            The servlet context, used for retrieving internal stylesheets.
	 * @param repository
	 *            The TextGridRep instance, used for retrieving textgrid
	 *            stylesheets.
	 */
	public StylesheetManager(final ServletContext servlet,
			final ITextGridRep repository) {
		this.servlet = servlet;
		this.repository = repository;

		xsltProcessor = new Processor(false);
		xsltProcessor.getUnderlyingConfiguration().setURIResolver(
				new TGUriResolver(repository));
		// xsltProcessor.getUnderlyingConfiguration().setAllowExternalFunctions(
		// false); // we run external stylesheets
		xsltProcessor.getUnderlyingConfiguration().setCompileWithTracing(true);
		stylesheets = CacheBuilder.newBuilder().recordStats()
				.maximumWeight(200).weigher(new Weigher<URI, XsltExecutable>() {
					@Override
					public int weigh(final URI key, final XsltExecutable value) {
						return importantStylesheets.contains(key) ? 1 : 10;
					}
				}).removalListener(new RemovalListener<URI, XsltExecutable>() {
					@Override
					public void onRemoval(
							final RemovalNotification<URI, XsltExecutable> notification) {
						logger.info(MessageFormat
								.format("Removed stylesheet {0} from cache, reason: {1}",
										notification.getKey(),
										notification.getCause()));
						logger.info(stylesheets.stats().toString());
					}
				}).build(new CacheLoader<URI, XsltExecutable>() {

					@Override
					public XsltExecutable load(final URI url) throws Exception {
						final XsltCompiler compiler = xsltProcessor
								.newXsltCompiler();
						try {
							final XsltExecutable executable = compiler
									.compile(new StreamSource(url.toString()));
							logger.log(Level.INFO,
									"Successfully loaded stylesheet {0}", url);
							return executable;
						} catch (final Exception e) {
							logger.log(Level.SEVERE, MessageFormat.format(
									"Failed to load stylesheet {0}", url), e);
							throw e;
						}
					}
				});

	}

	/**
	 * Returns an appropriate stylesheet for the given URI.
	 *
	 * Basically, we try the following options in order:
	 * <ol>
	 * <li>The stylesheet is cached -> return the cached version.
	 * <li>The stylesheet is public or external -> load & cache it.
	 * <li>The stylesheet is non-public TextGrid internal -> load & do not cache
	 * it.
	 * </ol>
	 *
	 * @param uri
	 *            the URI of the stylesheet to load
	 * @param sid
	 *            the session ID to use, if present
	 * @param forceLoad
	 *            do not use a cached version even if present.
	 * @param frequentlyUsed
	 *            if <code>true</code>, this stylesheet will be less likely
	 *            evicted.
	 * @throws IOException
	 *             if an error occurs reading the stylesheet.
	 * @throws SaxonApiException
	 *             if saxon fails to compile the stylesheet.
	 */
	public XsltExecutable getStylesheet(final URI uri,
			final Optional<String> sid, final boolean forceLoad,
			final boolean frequentlyUsed) throws SaxonApiException, IOException {

		if (frequentlyUsed)
			importantStylesheets.add(uri);

		try {
			if (forceLoad)
				stylesheets.invalidate(uri);

			return stylesheets.get(uri, new StylesheetLoader(uri, sid));
		} catch (final ExecutionException e) {
			final Throwable cause = e.getCause();
			if (cause instanceof PrivateResourceException)
				return ((PrivateResourceException) cause).getXsltExecutable();
			else {
				Throwables.propagateIfPossible(cause, SaxonApiException.class,
						IOException.class);
				return null; // will never be reached
			}

		}

	}

	private final class StylesheetLoader implements Callable<XsltExecutable> {
		private final URI uri;
		private final Optional<String> sid;

		private StylesheetLoader(final URI uri, final Optional<String> sid) {
			this.uri = uri;
			this.sid = sid;
		}

		@Override
		public XsltExecutable call() throws Exception {
			final XsltCompiler compiler = xsltProcessor.newXsltCompiler();
			final XsltExecutable executable;
			if (uri.getScheme() == null || uri.getScheme() == "file") {
				// (1) Internal
				final URL resource = resolveInternalPath(uri.getPath());
				executable = compiler.compile(new StreamSource(resource
						.openStream(), resource.toExternalForm()));
				logger.log(Level.INFO, "Cached internal stylesheet {0}",
						resource);
				return executable;
			} else if (TGUriResolver.isResolveable(uri)) {

				// (3/4) it's a TextGrid object, load it from TG-crud.
				final TGOSupplier<InputStream> xsltSupplier = repository.read(
						uri, sid.orNull());
				executable = compiler.compile(new StreamSource(xsltSupplier
						.getInput(), uri.toString()));

				if (isPublic(xsltSupplier.getMetadata())) {
					// (3) it's public -> we can cache it.
					logger.log(Level.INFO, "Cached public stylesheet {0}", uri);
					return executable;
				} else {
					logger.log(Level.INFO, "Loaded private stylesheet {0}", uri);
					throw new PrivateResourceException(executable);
				}
			} else {
				URLConnection connection = uri.toURL().openConnection();
				if (connection instanceof HttpURLConnection) {
					HttpURLConnection httpConnection = (HttpURLConnection) connection;
					httpConnection.setInstanceFollowRedirects(true);
					httpConnection.setRequestProperty("Accept", "text/xsl, text/xslt, application/xslt+xml, */*;q=0.8");
				}
				// (2) it's non-TextGrid -- load & cache it.
				InputStream inputStream = connection.getInputStream();
				executable = compiler.compile(new StreamSource(inputStream));
				logger.log(Level.INFO, "Cached external stylesheet {0}", uri);
				return executable;
			}
		}
	}

	private static class PrivateResourceException extends Exception {

		private static final long serialVersionUID = -3506322226718139009L;
		private XsltExecutable xsltExecutable;

		public XsltExecutable getXsltExecutable() {
			return xsltExecutable;
		}

		public PrivateResourceException(final XsltExecutable xsltExecutable) {
			super("The source object is private and will not be cached.");
			this.xsltExecutable = xsltExecutable;
		}
	}


	public URL resolveInternalPath(final String path)
			throws MalformedURLException {
		URL stylesheet;
		if (servlet == null) {
			logger.info("No servlet context, trying fallback property");
			final String dir = System.getProperty("webapp.directory");
			if (dir == null)
				throw new IllegalStateException(
						"Could not find stylesheet: Neither ServletContext nor fallback property webapp.directory have been defined.");
			stylesheet = new URL("file://" + dir + path);
		} else {
			stylesheet = servlet.getResource(path);
		}
		logger.fine("Resolved internal stylesheet: "
				+ stylesheet.toExternalForm());
		return stylesheet;
	}

	private static boolean isPublic(final ObjectType metadata) {
		try {
			return metadata.getGeneric().getGenerated().getAvailability()
					.contains("public");
		} catch (final NullPointerException e) {
			return false;
		}
	}

	@Override
	protected void finalize() throws Throwable {
		if (stylesheets != null)
			logger.log(Level.INFO,
					"Shutting down stylesheet manager. Stats: {0}",
					stylesheets.stats());
		super.finalize();
	}

	public CacheStats stats() {
		return stylesheets.stats();
	}
	
	public Set<URI> knownStylesheets() {
		return ImmutableSet.copyOf(stylesheets.asMap().keySet());
	}

}