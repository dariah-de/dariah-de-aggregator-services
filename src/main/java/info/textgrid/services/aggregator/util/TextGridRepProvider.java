package info.textgrid.services.aggregator.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.xml.stream.XMLStreamException;
import javax.xml.ws.Holder;

import org.codehaus.jettison.json.JSONException;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;

import info.textgrid.middleware.confclient.ConfservClient;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.middleware.tgsearch.client.SearchClient;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;
import info.textgrid.services.aggregator.ITextGridRep;

public class TextGridRepProvider implements ITextGridRep {

	private HashMap<String, String> config;
	private ConfservClient confservClient;
	private TGCrudService crud;
	private SearchClient publicSearchClient;
	private final String CONF_ENDPOINT;
	private Map<String, String> configOverrides;
	private Properties localConfig;
	private static final String DEFAULT_CONF_ENDPOINT = "https://www.textgridlab.org/1.0/confserv";
	private static final Logger logger = Logger
			.getLogger(TextGridRepProvider.class.getCanonicalName());

	public static final String PROPERTY_PREFIX = "aggregator.rep.";
	private static final String PROPERTY_CRUDENDPOINT_OVERRIDE = "aggregator.tgcrud.links";
	private static final File[] PROPERTY_FILES = {
			new File("/etc/textgrid/aggregator/aggregator.properties"),
			new File(System.getProperty("user.home"), ".aggregator.properties") };

	@Override
	public String getCONF_ENDPOINT() {
		return CONF_ENDPOINT;
	}

	public TextGridRepProvider(final String endpoint) {
		this.CONF_ENDPOINT = endpoint;
		logger.info("Initialized a TextGridRepProvider with the endpoint "
				+ endpoint);
	}

	private static ITextGridRep instance;

	/**
	 * {@inheritDoc}
	 *
	 * This implementation will first check whether there is an
	 * {@linkplain #getConfigOverrides() override configuration} for the given
	 * key.
	 */
	@Override
	public String getConfValue(final String key) throws WebApplicationException {
		if (getConfigOverrides().containsKey(key))
			return getConfigOverrides().get(key);
		else
			return getConfig().get(key);
	}

	/**
	 * {@inheritDoc}
	 *
	 * This implementation will return the configuration from the confserv,
	 * <em>ignoring</em> overrides from local configuration. Clients are
	 * advised to call {@link #getConfValue(String)} instead of retrieving
	 * the whole configuration.
	 *
	 * @see #getConfigOverrides()
	 */
	@Override
	public Map<String, String> getConfig() {
		if (config == null) {
			try {
				confservClient = new ConfservClient(CONF_ENDPOINT);
				config = confservClient.getAll();
				logger.info("Received config from " + CONF_ENDPOINT);
				if (logger.isLoggable(Level.FINE)) {
					logger.fine(Joiner.on("\n").withKeyValueSeparator("=")
							.join(config));
				}
			} catch (final IOException e) {
				throw new WebApplicationException(e);
			} catch (final JSONException e) {
				throw new WebApplicationException(e);
			} catch (final XMLStreamException e) {
				throw new WebApplicationException(e);
			}
		}
		return config;
	}

	/**
	 * Returns a map of configuration overrides.
	 *
	 * You can provide overrides for confserv keys by prefixing the key with
	 * {@value #PROPERTY_PREFIX} and providing it in one of the configuration
	 * files ({@value #PROPERTY_FILES}) or as a system property.
	 *
	 * @see #getConfValue(String)
	 */
	public Map<String, String> getConfigOverrides() {
		if (configOverrides == null) {
			final Properties properties = getLocalConfig();

			configOverrides = Maps.newHashMap();

			for (final String key : properties.stringPropertyNames()) {
				if (key.startsWith(PROPERTY_PREFIX)) {
					configOverrides.put(
							key.substring(PROPERTY_PREFIX.length()),
							properties.getProperty(key));
				}
			}
		}
		return configOverrides;
	}

	/**
	 * Returns the effective local properties, e.g.,from /etc/textgrid/aggregator/aggregator.properties.
	 */
	public Properties getLocalConfig() {
		if (localConfig == null) {
			Properties properties = System.getProperties();
			for (final File file : PROPERTY_FILES) {
				if (file.exists()) {
					properties = new java.util.Properties(properties);
					try {
						properties.load(new FileInputStream(file));
					} catch (final IOException e) {
						logger.log(
								Level.WARNING,
								MessageFormat
								.format("Failed to load configuration file {0} although it exists: {1}",
										file, e.getMessage()), e);
					}
				}
			}
			this.localConfig = properties;
		}
		return localConfig;
	}

	@Override
	public TGCrudService getCRUDService() {
		if (crud == null) {
			try {

              logger.info("Confserv TG-crud entry: " + getConfValue(ConfservClientConstants.TG_CRUD));

              crud = TGCrudClientUtilities.getTgcrud(
						getConfValue(ConfservClientConstants.TG_CRUD), true);
				
				logger.info("TG-crud version: " + crud.getVersion());
				
			} catch (final MalformedURLException e) {
				throw new IllegalArgumentException(e);
			}
		}
		return crud;
	}

	@Override
	public InputStream getContent(final URI uri, final String sid)
			throws ObjectNotFoundFault, MetadataParseFault, IoFault,
			ProtocolNotImplementedFault, AuthFault, IOException {
		final TGCrudService crudService = getCRUDService();
		final Holder<MetadataContainerType> mdHolder = new Holder<MetadataContainerType>();
		final Holder<DataHandler> dHolder = new Holder<DataHandler>();
		crudService.read(sid, null, uri.toString(), mdHolder, dHolder);
		return dHolder.value.getInputStream();
	}

	@Override
	public TGOSupplier<InputStream> read(final URI uri, final String sid)
			throws WebApplicationException {
		final TGCrudService crudService = getCRUDService();
		final Holder<MetadataContainerType> mdHolder = new Holder<MetadataContainerType>();
		final Holder<DataHandler> dHolder = new Holder<DataHandler>();
		try {
			crudService.read(sid, null, uri.toString(), mdHolder, dHolder);

			return new TGOSupplier<InputStream>() {

				@Override
				public InputStream getInput() throws IOException {
					return dHolder.value.getInputStream();
				}

				@Override
				public ObjectType getMetadata() {
					return mdHolder.value.getObject();
				}
			};
		} catch (final ObjectNotFoundFault e) {
			throw new WebApplicationException(e, Status.NOT_FOUND);
		} catch (final MetadataParseFault e) {
			throw new WebApplicationException(e);
		} catch (final IoFault e) {
			throw new WebApplicationException(e);
		} catch (final ProtocolNotImplementedFault e) {
			throw new WebApplicationException(e, Status.BAD_REQUEST);
		} catch (final AuthFault e) {
			throw new WebApplicationException(e, Status.FORBIDDEN);
		}
	}

	@Override
	public InputStream getContent(final URI uri) throws ObjectNotFoundFault,
			MetadataParseFault, IoFault, ProtocolNotImplementedFault,
			AuthFault, IOException {
		return getContent(uri, null);
	}

	@Override
	public SearchClient getPublicSearchClient() {
		if (publicSearchClient == null) {
			final String endpoint = getConfValue(ConfservClientConstants.TG_SEARCH_PUBLIC);
			publicSearchClient = new SearchClient(endpoint);
			logger.info("Instantiated public search client at: " + endpoint);
		}
		return publicSearchClient;
	}

	/**
	 * Creates a search client for the non-public repository, readily
	 * initialized with the given session id. Note that this must be cached by
	 * the client.
	 *
	 * @param sid
	 * @return
	 */
	@Override
	public SearchClient createPrivateSearchClient(final String sid) {
		final String confValue = getConfValue(ConfservClientConstants.TG_SEARCH);
		final SearchClient endpoint = new SearchClient(confValue);
		endpoint.setSid(sid);
		logger.info("Created and configured a private search client at: "
				+ endpoint);
		return endpoint;
	}

	public static ITextGridRep getInstance() {
		logger.warning("Someone wants the default instance -- I hope we're in a test.");
		if (instance == null)
			instance = new TextGridRepProvider(DEFAULT_CONF_ENDPOINT);
		return instance;
	}

	@Override
	public String getCRUDRestEndpoint() {
		final String localOverride = getLocalConfig().getProperty(PROPERTY_CRUDENDPOINT_OVERRIDE);
		if (localOverride != null)
			return localOverride;

		final String soapEndpoint = getConfValue(ConfservClientConstants.TG_CRUD);
		return soapEndpoint.substring(0, soapEndpoint.lastIndexOf('/') + 1)
				.concat("rest");
	}

}
