package info.textgrid.services.aggregator;

import info.textgrid.services.aggregator.util.StylesheetManager;
import info.textgrid.services.aggregator.util.TextGridRepProvider;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.StreamingOutput;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.io.CharStreams;
import com.google.common.io.Resources;

@Path("/version")
public class Version {

	private final ITextGridRep repository;
	private String version;
	private Properties gitInfo;
	private final StylesheetManager stylesheetManager;
	private HttpHeaders headers;

	public Version(final ITextGridRep repository, final StylesheetManager stylesheetManager) {
		this.repository = repository;
		this.stylesheetManager = stylesheetManager;
	}

	private Properties loadGitInfo() {
		if (gitInfo == null) {
			gitInfo = new Properties();
			final InputStream inputStream = getClass().getResourceAsStream(
					"/git.properties");
			if (inputStream != null) {
				try {
					gitInfo.load(inputStream);
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return gitInfo;
	}

	private String getVersion() {
		if (version == null) {
			try {
				version = Resources.asCharSource(Resources.getResource("/version.txt"), Charsets.UTF_8).read();
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return version;
	}

	@GET
	// @Path("/")
	@Produces("text/html")
	public StreamingOutput get() {
		return new StreamingOutput() {

			@Override
			public void write(final OutputStream outputStream)
					throws IOException, WebApplicationException {
				final PrintStream out = new PrintStream(outputStream, true);
				out.println("<!DOCTYPE html><html><head><meta charset='utf-8'/><title>Aggregator Version Report</title></head><body>");
				out.printf("<h1>TextGrid Aggregator</h1>\n");
				final Properties info = loadGitInfo();
				out.printf(
						"<p style='border: 1px solid #ccf; padding: 6px;'>Version: <em>%s</em> (<strong>%s</strong>) from branch <em>%s</em> as of <em>%s</em>, built on <em>%s</em>.</p>",
						getVersion(),
						info.getProperty("git.commit.id.describe", "?"),
						info.getProperty("git.branch", "?"),
						info.getProperty("git.commit.time", "?"),
						info.getProperty("git.build.time", "?"));

				out.printf(
						"<p>TextGridRep Configuration Endpoint: <a href='%1$s'>%1$s</a></p>\n\n",
						repository.getCONF_ENDPOINT());
				out.print("<h3>Repository Configuration:</h3>\n<table><tr><td>");
				out.print(Joiner.on("</td></tr>\n<tr><td>")
						.withKeyValueSeparator("</td><td>")
						.join(repository.getConfig()));
				out.println("</td></tr></table>");

				out.printf("<h3>Configuration Overrides:</h3>\n"+
						"<p>Specify a configuration key prefixed with <code>%s</code> in the configuration file to override a confserv value.</p>\n" +
						"<table>\n<tr><td>", TextGridRepProvider.PROPERTY_PREFIX);
				out.print(Joiner.on("</td></tr>\n<tr><td>")
						.withKeyValueSeparator("</td><td>")
						.join(((TextGridRepProvider) repository).getConfigOverrides()));
				out.println("</td></tr></table>");

				out.printf("<p>Using TG-crud version: <strong>%s</strong>\n",
						repository.getCRUDService().getVersion());

				final URL xslVersionURL = stylesheetManager.resolveInternalPath("/tei-stylesheets/VERSION");
				final InputStream xslVersionStream = xslVersionURL.openStream();
				final String xslVersion = CharStreams.toString(new InputStreamReader(xslVersionStream));
				out.printf("<p>Using TEI stylesheets version: <strong>%s</strong>\n", xslVersion);

				if (headers != null) {
					out.println("<h2>HTTP Headers</h2>\n<table class='headers'>");
					for (final Entry<String, List<String>> entry : headers.getRequestHeaders().entrySet()) {
						out.printf("<tr><td class='header'>%s</td>", entry.getKey());
						if (entry.getValue().size() == 1)
							out.printf("<td class='headerValue'>%s</td></tr>\n", entry.getValue().get(0));
						else
							out.printf("<td class='headerValue'><ul>\n<li>%s</li>\n</ul></td></tr>\n",
									Joiner.on("</li>\n<li>").join(entry.getValue()));
					}
					out.println("</table>");
				}


				out.printf("<h2>StylesheetManager statistics</h2>\n");
				out.printf("<p>%s</p>\n", stylesheetManager.stats().toString());
				out.printf("<p>The following stylesheets are currently cached: </p><ol>\n");
				for (final URI uri : stylesheetManager.knownStylesheets())
					out.printf("<li>%s</li>", uri);

				out.println("</ol></body></html>");
				out.close();
				outputStream.close();
			}
		};
	}

	public Version headers(HttpHeaders headers) {
		this.headers = headers;
		return this;
	}

}
