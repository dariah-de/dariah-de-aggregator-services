package info.textgrid.services.aggregator;

import info.textgrid.services.aggregator.util.StylesheetManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriBuilder;
import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XsltTransformer;

import com.google.common.base.Optional;

public class Help implements StreamingOutput {

	private final StylesheetManager stylesheetManager;
	private String endpointUrl;

	public Help(final StylesheetManager stylesheetManager, String endpointUrl) {
		this.stylesheetManager = stylesheetManager;
		this.endpointUrl = endpointUrl;
	}

	@Override
	public void write(final OutputStream output) throws IOException,
			WebApplicationException {
		try {
			final XsltTransformer transformer = stylesheetManager
					.getStylesheet(
							URI.create("/stylesheets/wadl_documentation-2009-02.xsl"),
							Optional.<String> absent(), false, false).load();
			final Serializer serializer = stylesheetManager.xsltProcessor
					.newSerializer(output);
			transformer.setDestination(serializer);
			System.out.println("wadlurl: " + getWadlURL());
			transformer.setSource(new StreamSource(getWadlURL().openStream(),
					getWadlURL().toString()));
			transformer.setParameter(new QName("apptitle"), new XdmAtomicValue(
					"TextGrid Aggregator Service"));
			transformer.transform();
		} catch (final SaxonApiException e) {
			throw new WebApplicationException(e);
		} catch (final FileNotFoundException e) {
			throw new WebApplicationException(GenericExceptionMapper
					.toResponse(
							Status.NOT_FOUND,
							"There still was a not found error: "
									+ e.getMessage(),
							"endpointUrl: " + endpointUrl)
					.build());
		}

	}

	public URL getWadlURL() {
		return getWadlURL(endpointUrl);
	}

	private URL getWadlURL(String string) {
		final URI resolved = UriBuilder.fromUri(string).replaceQuery("_wadl")
				.build();
		try {
			final URL url = resolved.toURL();
			final URLConnection connection = url.openConnection();
			connection.connect();
			if (((HttpURLConnection) connection).getResponseCode() == 200)
				return url;
			else
				return null;
		} catch (final MalformedURLException e) {
			throw new WebApplicationException(e);
		} catch (final IOException e) {
			throw new WebApplicationException(e);
		}
	}
}