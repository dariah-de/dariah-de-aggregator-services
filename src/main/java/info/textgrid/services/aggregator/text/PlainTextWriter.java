package info.textgrid.services.aggregator.text;

import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.*;
import info.textgrid.services.aggregator.ITextGridRep;
import info.textgrid.services.aggregator.html.TGUriResolver;
import info.textgrid.services.aggregator.teicorpus.CorpusBasedExporter;
import info.textgrid.services.aggregator.util.StylesheetManager;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XsltTransformer;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

public class PlainTextWriter extends CorpusBasedExporter {
    private static final URI TO_TEXT_XSL = java.net.URI.create("/tei-stylesheets/txt/tei-to-text.xsl");

    private final StylesheetManager stylesheetManager;

    public PlainTextWriter(ITextGridRep repository, StylesheetManager stylesheetManager, String uriList, Request request) {
        super(repository, request, uriList);
        this.stylesheetManager = stylesheetManager;
        setMediaType(MediaType.TEXT_PLAIN + "; charset=UTF-8");
        setDisposition(Disposition.INLINE);
        setFileExtension("txt");
    }

    @Override
    public void write(OutputStream output) throws IOException, WebApplicationException {
        try {
            final XsltTransformer transformer = stylesheetManager.getStylesheet(TO_TEXT_XSL, getSid(), false, true).load();
            if (getSid().isPresent())
                transformer.setURIResolver(new TGUriResolver(repository, getSid()));
            transformer.setSource(loadSource(false));
            final Serializer serializer = stylesheetManager.xsltProcessor.newSerializer(output);
            transformer.setDestination(serializer);
            transformer.transform();
        } catch (SaxonApiException | IoFault | MetadataParseFault | ProtocolNotImplementedFault e) {
            throw new WebApplicationException(e);
        } catch (ObjectNotFoundFault objectNotFoundFault) {
            throw new WebApplicationException(objectNotFoundFault, 404);
        } catch (AuthFault authFault) {
            throw new WebApplicationException(authFault, 403);
        }
    }
}
