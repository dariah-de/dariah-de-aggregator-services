package info.textgrid.services.aggregator.teicorpus;

import info.textgrid.namespaces.metadata.agent._2010.AgentType;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.services.aggregator.ITextGridRep;
import info.textgrid.services.aggregator.tree.AggregationTreeWalker;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.stream.EventFilter;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.sax.SAXResult;
import javax.xml.ws.Holder;

import org.springframework.util.xml.StaxUtils;
import com.google.common.base.Joiner;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

public class TEICorpusSerializer extends AggregationTreeWalker implements
		StreamingOutput {

	private static final String TEI_CORPUS = "teiCorpus";
	private static final String TEI_NS = "http://www.tei-c.org/ns/1.0";
	private final Logger logger = Logger.getLogger("info.textgrid.services.aggregator.teicorpus.serializer");


	private XMLOutputFactory outputFactory;
	private XMLEventFactory eventFactory;
	private XMLEventWriter writer;
	private final Iterable<ObjectType> rootObjects;
	private XMLInputFactory inputFactory;
	private final Set<String> seen = Sets.newHashSet();
	private static final EventFilter NO_DOCUMENT_NODE = new EventFilter() {

		@Override
		public boolean accept(final XMLEvent event) {
			return !(event.isStartDocument() || event.isEndDocument());
		}
	};
	private TEIHeaderStack stack;
	private boolean handled;
	private boolean flat;
	private String title = "TextGridRep Basket";
	private int level = 0;
	private static String toString(final ObjectType object) {
		return Joiner.on(" / ").join(object.getGeneric().getProvided().getTitle()) + " (" + object.getGeneric().getGenerated().getTextgridUri().getValue() + ", " +
			object.getGeneric().getProvided().getFormat() + ")";
	}
	public TEICorpusSerializer(final ITextGridRep repository, final Iterable<ObjectType> rootObjects)
			throws ObjectNotFoundFault, MetadataParseFault, IoFault, AuthFault {
		this.rootObjects = rootObjects;
		setRepository(repository);
		logger.info("Starting TEIcorpus serialization");
	}

	public TEICorpusSerializer(final ITextGridRep repository, final ObjectType rootObject, final boolean flat, final String sid) throws ObjectNotFoundFault, MetadataParseFault, IoFault, AuthFault {
		this(repository, ImmutableList.of(rootObject) , flat, sid);
	}

	public TEICorpusSerializer(final ITextGridRep repository, final Iterable<ObjectType> rootObjects, final boolean flat, final String sid)
			throws ObjectNotFoundFault, MetadataParseFault, IoFault, AuthFault {
		this(repository, rootObjects);
		this.flat = flat;
		this.setSid(sid);
	}

	@Override
	public void write(final OutputStream output) throws IOException, WebApplicationException {
		logger.info(MessageFormat.format("writing TEIcorpus for {0}, sid: {1}", this, getSid() != null && !getSid().isEmpty()));
		outputFactory = XMLOutputFactory.newInstance();
		eventFactory = XMLEventFactory.newInstance();
		outputFactory.setProperty(XMLOutputFactory.IS_REPAIRING_NAMESPACES, true);
		inputFactory = XMLInputFactory.newInstance();
		try {
			writer = outputFactory.createXMLEventWriter(output);
			writer.setDefaultNamespace(TEI_NS);
			writer.add(eventFactory.createStartDocument());
			
			Iterator<ObjectType> iterator = rootObjects.iterator();
			boolean hasMultipleRoots = false;
			if (iterator.hasNext()) {
				iterator.next();
				hasMultipleRoots = iterator.hasNext();
			}

			if (hasMultipleRoots) {
				writer.add(eventFactory.createStartElement("", TEI_NS, TEI_CORPUS));
				writeContainerHeader(getTitle());
				level++;
			}

			for (final ObjectType rootObject : rootObjects)
				walk(rootObject, false);

			if (hasMultipleRoots) {
				writer.add(eventFactory.createEndElement("", TEI_NS, TEI_CORPUS));
				level--;
			}

			writer.close();
		} catch (final XMLStreamException e) {
			logger.log(Level.SEVERE, "XML Error during serialization", e);
			throw new WebApplicationException(e);
		}
	}

	@Override
	protected boolean walk(final ObjectType object, final boolean again) {
		if (seen.contains(object.getGeneric().getGenerated().getTextgridUri().getValue()))
			writeSkip(object);

		handled = super.walk(object, again);

		if (!handled)
			if (object.getGeneric().getProvided().getFormat()
						.equals("text/xml"))
			walkXML(object);
		else
			writeSkip(object);
		return true;
	}

	@Override
	protected void walkAggregation(final ObjectType object, final boolean again) {
		logger.fine("[Processing aggregation " + toString(object));
		level++;
		if (stack == null)
				try {
					stack = new TEIHeaderStack(getRepository(), object, getSearchClient(isPublic(object)));
				} catch (final Exception e) {
					throw Throwables.propagate(e);
				}
		else
			stack.push(object);

		if (!flat || level <= 1) {
		try {
			writer.add(eventFactory.createStartElement("", TEI_NS, TEI_CORPUS));
			writer.add(eventFactory.createAttribute("corresp", object
					.getGeneric().getGenerated().getTextgridUri().getValue()));
		} catch (final XMLStreamException e) {
			throw new IllegalStateException(e);
		}

		// writeHeader(object);
		XMLEventFilter xmlEventFilter = new XMLEventFilter(writer) {

			@Override
			public void add(final XMLEvent event) throws XMLStreamException {
				if (!(event.isStartDocument() || event.isEndDocument()))
					super.add(event);
			}

		};
		stack.writeHeader(new SAXResult(StaxUtils.createContentHandler(xmlEventFilter)));
		}

		super.walkAggregation(object, again);

		if (!flat || level <= 1)
			try {
				writer.add(eventFactory.createEndElement("", TEI_NS, TEI_CORPUS));
			} catch (final XMLStreamException e) {
				throw new IllegalStateException(e);
			}

		level--;
		logger.fine("Finished aggregation " + toString(object) + "]");
	}

	@SuppressWarnings("unused")
	private void writeHeader(final ObjectType object) throws XMLStreamException {
		writer.add(eventFactory.createStartElement("",  TEI_NS, "teiHeader"));
		writer.add(eventFactory.createComment("This is a (temporary) header generated from aggregation metadata."));
		writer.add(eventFactory.createStartElement("",  TEI_NS, "fileDesc"));
		writer.add(eventFactory.createStartElement("",  TEI_NS, "titleStmt"));
		for (final String title : object.getGeneric().getProvided().getTitle())
			writeSimpleTEIElement("title", title);
		if (object.getEdition() != null)
			for (final AgentType agent : object.getEdition().getAgent()) {
				writer.add(eventFactory.createStartElement("",  TEI_NS, "respStmt"));
				writeSimpleTEIElement("resp", agent.getRole().toString());
				writer.add(eventFactory.createStartElement("",  TEI_NS, "author"));
				if (agent.getId() != null)
					writer.add(eventFactory.createAttribute("key", agent.getId()));
				writer.add(eventFactory.createCharacters(agent.getValue()));
				writer.add(eventFactory.createEndElement("",  TEI_NS, "author"));
				writer.add(eventFactory.createEndElement("",  TEI_NS, "respStmt"));
			}
		writer.add(eventFactory.createEndElement("",  TEI_NS, "titleStmt"));
		writer.add(eventFactory.createStartElement("",  TEI_NS, "publicationStmt"));
		writer.add(eventFactory.createStartElement("",  TEI_NS, "idno"));
		writer.add(eventFactory.createAttribute("type", "textgrid"));
		writer.add(eventFactory.createCharacters(object.getGeneric().getGenerated().getTextgridUri().getValue()));
		writer.add(eventFactory.createEndElement("",  TEI_NS, "idno"));
		writer.add(eventFactory.createEndElement("",  TEI_NS, "publicationStmt"));
		writer.add(eventFactory.createEndElement("",  TEI_NS, "fileDesc"));
		writer.add(eventFactory.createEndElement("",  TEI_NS, "teiHeader"));
	}

	private void writeContainerHeader(final String title) throws XMLStreamException {
		writer.add(eventFactory.createStartElement("",  TEI_NS, "teiHeader"));
		writer.add(eventFactory.createStartElement("",  TEI_NS, "fileDesc"));
		writer.add(eventFactory.createStartElement("",  TEI_NS, "titleStmt"));
		writeSimpleTEIElement("title", title);
		writer.add(eventFactory.createEndElement("",  TEI_NS, "titleStmt"));
		writer.add(eventFactory.createStartElement("",  TEI_NS, "publicationStmt"));
		writeSimpleTEIElement("p", "Exported from the TextGrid repository");
		writer.add(eventFactory.createEndElement("",  TEI_NS, "publicationStmt"));
		writer.add(eventFactory.createEndElement("",  TEI_NS, "fileDesc"));
		writer.add(eventFactory.createEndElement("",  TEI_NS, "teiHeader"));
	}

	private void writeSimpleTEIElement(final String element, final String content) throws XMLStreamException {
		writer.add(eventFactory.createStartElement("",  TEI_NS, element));
		writer.add(eventFactory.createCharacters(content));
		writer.add(eventFactory.createEndElement("",  TEI_NS, element));
	}

	protected void walkXML(final ObjectType object) {
		logger.fine("Processing XML " + toString(object));
		final Holder<MetadataContainerType> mdHolder = new Holder<MetadataContainerType>();
		final Holder<DataHandler> dhHolder = new Holder<DataHandler>();
		try {
			getRepository().getCRUDService().read(
					getSid(),
					"",
					object.getGeneric().getGenerated().getTextgridUri()
							.getValue(), mdHolder,
					dhHolder);
			final XMLEventReader reader = inputFactory.createFilteredReader(
					inputFactory.createXMLEventReader(dhHolder.value.getInputStream()),
					NO_DOCUMENT_NODE);

			// skip to document node:
			while (reader.hasNext() && !reader.peek().isStartElement())
				reader.next();

			final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(null, true);
			rewriter.setMergeLinkAdjuster(new ConfigurableXMLRewriter.DefaultMergeLinkAdjuster(URI.create(object.getGeneric().getGenerated().getTextgridUri().getValue())));
			rewriter.configure(URI.create("internal:tei#tei"));
			rewriter.rewrite(reader, writer);
		} catch (final ObjectNotFoundFault e) {
			throw new WebApplicationException(e, Response.Status.NOT_FOUND);
		} catch (final MetadataParseFault e) {
			throw new WebApplicationException(e);
		} catch (final IoFault e) {
			throw new WebApplicationException(e);
		} catch (final ProtocolNotImplementedFault e) {
			throw new WebApplicationException(e);
		} catch (final AuthFault e) {
			throw new WebApplicationException(e, Response.Status.FORBIDDEN);
		} catch (final XMLStreamException e) {
			throw new WebApplicationException(e);
		} catch (final IOException e) {
			throw new WebApplicationException(e, Response.Status.SERVICE_UNAVAILABLE);
		}
	}

	/**
	 * Writes a comment for objects that are skipped.
	 */
	private void writeSkip(final ObjectType object) {
		logger.fine("Skipped " + toString(object));
		try {
			writer.add(eventFactory.createComment(MessageFormat.format(
					"Skipped {0}.", toString(object))));
		} catch (final XMLStreamException e) {
			throw new IllegalStateException(e);
		}
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(final String title) {
		this.title = title;
	}

}
