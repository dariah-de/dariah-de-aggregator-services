package info.textgrid.services.aggregator.teicorpus;

import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.services.aggregator.AbstractExporter;
import info.textgrid.services.aggregator.ITextGridRep;

import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;

public class TEICorpusExporter extends AbstractExporter {

	private boolean flat;

	public TEICorpusExporter(final ITextGridRep repository, final Request request,
			final String uriList) {
		super(repository, request, uriList);
		setFileExtension("xml");
		setMediaType("application/tei+xml");
	}

	@Override
	public void write(final OutputStream output) throws IOException,
			WebApplicationException {
		TEICorpusSerializer serializer;
		try {
			serializer = new TEICorpusSerializer(repository, getRootObjects(), isFlat(), getSid().orNull());
			serializer.setTitle(getTitle());
			serializer.write(output);
		} catch (final ObjectNotFoundFault e) {
			throw new WebApplicationException(e, Status.NOT_FOUND);
		} catch (final MetadataParseFault e) {
			throw new WebApplicationException(e, Status.BAD_REQUEST);
		} catch (final IoFault e) {
			throw new WebApplicationException(e);
		} catch (final AuthFault e) {
			throw new WebApplicationException(e, Status.FORBIDDEN);
		}
	}

	public boolean isFlat() {
		return flat;
	}

	public void setFlat(final boolean flat) {
		this.flat = flat;
	}

}
