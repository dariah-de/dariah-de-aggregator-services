package info.textgrid.services.aggregator.teicorpus;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

public abstract class XMLEventFilter implements XMLEventWriter {

	protected final XMLEventWriter writer;

	public XMLEventFilter(final XMLEventWriter writer) {
		this.writer = writer;
	}

	@Override
	public void flush() throws XMLStreamException {
		writer.flush();
	}

	@Override
	public void close() throws XMLStreamException {
		writer.close();
	}

	@Override
	public void add(final XMLEvent event) throws XMLStreamException {
		writer.add(event);
	}

	@Override
	public void add(final XMLEventReader reader) throws XMLStreamException {
		while (reader.hasNext())
			add(reader.nextEvent());
	}

	@Override
	public String getPrefix(final String uri) throws XMLStreamException {
		return writer.getPrefix(uri);
	}

	@Override
	public void setPrefix(final String prefix, final String uri) throws XMLStreamException {
		writer.setPrefix(prefix, uri);
	}

	@Override
	public void setDefaultNamespace(final String uri) throws XMLStreamException {
		writer.setDefaultNamespace(uri);
	}

	@Override
	public void setNamespaceContext(final NamespaceContext context)
			throws XMLStreamException {
		writer.setNamespaceContext(context);
	}

	@Override
	public NamespaceContext getNamespaceContext() {
		return writer.getNamespaceContext();
	}

}
