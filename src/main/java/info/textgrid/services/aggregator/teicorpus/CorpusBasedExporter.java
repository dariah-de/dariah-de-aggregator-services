package info.textgrid.services.aggregator.teicorpus;

import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.services.aggregator.AbstractExporter;
import info.textgrid.services.aggregator.GenericExceptionMapper;
import info.textgrid.services.aggregator.ITextGridRep;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import com.google.common.io.ByteStreams;
import com.google.common.io.FileBackedOutputStream;
import com.google.common.io.Files;
import com.google.common.io.InputSupplier;

public abstract class CorpusBasedExporter extends AbstractExporter {

	private static final Logger logger = Logger.getLogger(CorpusBasedExporter.class.getCanonicalName());
	protected boolean readStylesheetPI = false;
	private InputSupplier<? extends InputStream> sourceBuffer = null;
	private IBufferFactory bufferFactory = new FBBAOSBufferFactory();
	private boolean flatCorpus = false;

	public CorpusBasedExporter(final ITextGridRep repository,
			final Request request, final String uriList) {
		super(repository, request, uriList);
	}

	public static interface IBufferFactory {
		public OutputStream getBufferSink();
		public InputSupplier<? extends InputStream> getBufferContents();
	}

	public static final class FBBAOSBufferFactory implements IBufferFactory {
		private FileBackedOutputStream sink;

		@Override
		public OutputStream getBufferSink() {
			this.sink = new FileBackedOutputStream(1024 * 1024, true);
			return sink;
		}

		@Override
		public InputSupplier<InputStream> getBufferContents() {
			return sink.getSupplier();
		}
	}

	public static final class FileBufferFactory implements IBufferFactory {
		private final File file;
		private FileOutputStream sink;

		public FileBufferFactory(final File file) throws FileNotFoundException {
			this.file = file;
			this.sink = new FileOutputStream(file);
		}

		@Override
		public InputSupplier<? extends InputStream> getBufferContents() {
			return Files.newInputStreamSupplier(file);
		}

		@Override
		public OutputStream getBufferSink() {
			return sink;
		}
	}

	/**
	 * Loads a source for the given document.
	 *
	 * <p>
	 * If the input is a basket or aggregation, it will be read and converted to
	 * TEIcorpus document. If it is an XML document, it will be returned as-is.
	 * </p>
	 *
	 * @param bufferRequired
	 *            if <code>true</code>, the document will be cached locally and
	 *            subsequent calls to this method will return a source created
	 *            from the local cache. Use this if you intend to read the
	 *            document multiple times.
	 *
	 *            Currently, documents that are not XML documents are buffered
	 *            regardless of this parameter.
	 */
	protected Source loadSource(final boolean bufferRequired)
			throws ObjectNotFoundFault, MetadataParseFault, IoFault,
			ProtocolNotImplementedFault, AuthFault, IOException {

		if (sourceBuffer != null)
			return new StreamSource(sourceBuffer.getInput());

		ObjectType metadata;
		if (sourceType != SourceType.BASKET) {
			metadata = getContentSimple().getMetadata();
			final String format = metadata.getGeneric().getProvided()
					.getFormat();
			if (format.contains("aggregation")) {
				sourceType = SourceType.AGGREGATION;
			} else if (format.matches("^(text|application)/.*xml.*$")) {
				sourceType = SourceType.XML;
			} else {
				final String errorMsg = MessageFormat
						.format("This exporter can only convert aggregations or XML documents, however, the document {0} you referred to has the MIME type {1}.",
								this, format);
				throw new WebApplicationException(
						GenericExceptionMapper.toResponse(
								Status.UNSUPPORTED_MEDIA_TYPE, errorMsg, "").build());
			}
		}

		StreamSource source;
		if (sourceType == SourceType.AGGREGATION
				|| sourceType == SourceType.BASKET) {
			final TEICorpusSerializer corpusSerializer =
					new TEICorpusSerializer(repository, getRootObjects(), isFlatCorpus(), getSid().orNull());
			final OutputStream corpusBuffer = getBufferFactory().getBufferSink();
			corpusSerializer.write(corpusBuffer);
			corpusBuffer.close();
			sourceBuffer = getBufferFactory().getBufferContents();
			source = new StreamSource(sourceBuffer.getInput());
		} else if (sourceType == SourceType.XML && bufferRequired) {
			final OutputStream xmlBuffer = getBufferFactory().getBufferSink();
			ByteStreams.copy(getContentSimple(), xmlBuffer);
			sourceBuffer = getBufferFactory().getBufferContents();
			source = new StreamSource(sourceBuffer.getInput());
		} else {
			source = new StreamSource(getContentSimple().getInput(),
					rootURIs.toString());
		}
		logger.log(Level.INFO, MessageFormat.format(
				"Fetched source for {0} after {1}", this, stopwatch.toString()));

		return source;
	}

	private IBufferFactory getBufferFactory() {
		return bufferFactory;
	}

	protected void setBufferFactory(final IBufferFactory bufferFactory) {
		this.bufferFactory = bufferFactory;
	}

	public boolean isFlatCorpus() {
		return flatCorpus;
	}

	public void setFlatCorpus(final boolean flatCorpus) {
		this.flatCorpus = flatCorpus;
	}

}
