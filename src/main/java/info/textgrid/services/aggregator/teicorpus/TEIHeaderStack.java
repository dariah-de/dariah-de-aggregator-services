package info.textgrid.services.aggregator.teicorpus;

import info.textgrid.middleware.tgsearch.client.SearchClient;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.TextgridUri;
import info.textgrid.namespaces.metadata.core._2010.GenericType;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.core._2010.ProvidedType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgsearch.EntryType;
import info.textgrid.namespaces.middleware.tgsearch.PathGroupType;
import info.textgrid.namespaces.middleware.tgsearch.PathResponse;
import info.textgrid.namespaces.middleware.tgsearch.PathType;
import info.textgrid.services.aggregator.ITextGridRep;

import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;

import org.apache.cxf.common.jaxb.JAXBContextCache;
import org.apache.cxf.common.jaxb.JAXBContextCache.CachedContextAndSchemas;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.google.common.collect.Lists;
import com.ibm.icu.text.MessageFormat;

/**
 * Generates headers for every possible TEIcorpus level.
 *
 * Clients initially create a new TEIHeaderStack for the root instance. This
 * class will then prepare a path request and initialize the stack. Clients can
 * afterwards push() new objects onto the stack while they descend into the hierarchy,
 * generate a header for the current stack, and pop() objects afterwards.
 *
 */
public class TEIHeaderStack {

	private final List<ObjectType> parents = Lists.newLinkedList();
	private Templates stylesheet;
	private static final Logger logger = Logger.getLogger(TEIHeaderStack.class.getCanonicalName());


	public TEIHeaderStack(final ITextGridRep repository, final ObjectType object, final SearchClient searchClient) throws ObjectNotFoundFault,
	MetadataParseFault, IoFault {
		final String uri = object.getGeneric().getGenerated().getTextgridUri()
				.getValue();
		parents.add(object);
		final PathResponse pathResponse = searchClient.getPath(uri);
		for (final PathGroupType pathGroup : pathResponse.getPathGroup()) {
			for (final PathType path : pathGroup.getPath()) {
				for (final EntryType entry : path.getEntry())
					if (!entry.getTextgridUri().equals(uri)) {
						try {
							final MetadataContainerType container = repository.getCRUDService().readMetadata(searchClient.getSid(), null, entry.getTextgridUri());
							parents.add(0, container.getObject());
						} catch (final AuthFault e) {
							logger.log(Level.WARNING, MessageFormat.format("Could not access ancestor {0} while building header for {1}: {2}. Skipping.", entry.getTextgridUri(), uri, e.getFaultInfo().getCause()));
						}
					}
			}
		}
	}

	public TEIHeaderStack(final String title) {
		final ObjectType object = new ObjectType();
		final GenericType generic = new GenericType();
		final ProvidedType provided = new ProvidedType();
		final GeneratedType generated = new GeneratedType();
		final TextgridUri textgridUri = new GeneratedType.TextgridUri();
		textgridUri.setValue("textgrid:pseudo-object");

		generated.setTextgridUri(textgridUri);

		provided.setFormat("application/tg.pseudo-object");
		provided.getTitle().add(title);
		provided.setNotes("Generated using TextGridRep's basket");

		generic.setProvided(provided);
		generic.setGenerated(generated);
		object.setGeneric(generic);

		parents.add(object);
	}

	public void push(final ObjectType object) {
		parents.add(0, object);
	}

	public ObjectType pop() {
		return parents.remove(0);
	}

	public Document generateCommonDOM() {
		final DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builder;
		try {
			builder = domFactory.newDocumentBuilder();
			final Document document = builder.newDocument();
			final Element rootElement = document.createElement("root");
			document.appendChild(rootElement);

			JAXBContext context = null;
			try {
				final CachedContextAndSchemas cachedContextAndSchemas = JAXBContextCache
						.getCachedContextAndSchemas(ObjectType.class);
				if (cachedContextAndSchemas != null) {
					context = cachedContextAndSchemas.getContext();
				}
			} catch (final JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (context == null) {
				context = JAXBContext.newInstance(ObjectType.class);
			}
			final Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			for (final ObjectType object : parents) {
				marshaller.marshal(object, rootElement);
			}
			return document;
		} catch (final ParserConfigurationException e) {
			throw new IllegalStateException(e);
		} catch (final JAXBException e) {
			throw new IllegalStateException(e);
		}
	}

	protected Templates getStylesheet()
			throws TransformerConfigurationException {
		if (stylesheet == null) {
			final InputStream xslStream = getClass().getClassLoader()
					.getResourceAsStream("md2tei.xsl");
			final StreamSource xslSource = new StreamSource(xslStream);

			final TransformerFactory factory = TransformerFactory.newInstance();
			stylesheet = factory.newTemplates(xslSource);
		}
		return stylesheet;
	}

	public void writeHeader(final Result result) {
		DOMSource domSource;
		try {
			domSource = new DOMSource(generateCommonDOM());
			final Transformer transformer = getStylesheet().newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
					"yes");
			transformer.transform(domSource, result);
		} catch (final TransformerException e) {
			throw new IllegalStateException(e);
		}
	}

}
