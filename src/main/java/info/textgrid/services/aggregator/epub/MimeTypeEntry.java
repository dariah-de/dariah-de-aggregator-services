package info.textgrid.services.aggregator.epub;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * This is a MIME type entry as defined in the Open Container Format
 * specification.
 * 
 * This must be the first entry in the zip file. Use
 * {@link #writeTo(ZipOutputStream)} to write it to your {@link ZipOutputStream}
 * and the class will care for the rest:
 * 
 * <h3>Example Usage</h3>
 * 
 * <pre>
 * ZipOutputStream zout = new ZipOutputStream(outputStream);
 * new MimeTypeEntry(&quot;application/epub+zip&quot;).writeTo(zout);
 * // add further entries to the ZIP stream
 * zout.close();
 * </pre>
 * 
 * 
 */
public class MimeTypeEntry extends ZipEntry {

	public final String mimetype;
	private byte[] bytes;

	/**
	 * Creates and initializes an entry for the MIME type.
	 */
	public MimeTypeEntry(final String mimetype) {
		super("mimetype");
		this.mimetype = mimetype.trim();
		try {
			bytes = mimetype.getBytes("UTF-8");
			final CRC32 crc = new CRC32();
			crc.update(bytes);
			setMethod(STORED);
			setCrc(crc.getValue());
			setSize(bytes.length);
		} catch (final UnsupportedEncodingException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Writes this entry to the given ZipOutputStream.
	 * 
	 * @param zout
	 *            The target stream
	 * @throws IOException
	 *             when writing goes wrong.
	 */
	public void writeTo(final ZipOutputStream zout) throws IOException {
		zout.putNextEntry(this);
		zout.write(bytes);
		zout.closeEntry();
	}

}
