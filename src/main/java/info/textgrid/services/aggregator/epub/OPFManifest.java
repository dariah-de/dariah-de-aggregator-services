package info.textgrid.services.aggregator.epub;

import info.textgrid._import.ImportObject;
import info.textgrid._import.RewriteMethod;
import info.textgrid.utils.linkrewriter.ImportMapping;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.Maps;

public class OPFManifest {
	public static final String OPF_NS = "http://www.idpf.org/2007/opf";
	public final ImmutableMap<URI, String> externalItems;
	public static final ImmutableMap<String, String> EXTENSION = ImmutableMap
			.of("image/jpeg", ".jpg", "image/png", ".png", "text/html", ".html");
	private ImportMapping importMapping;

	public OPFManifest(final File opfFile) throws SAXException, IOException,
	URISyntaxException {
		final DocumentBuilderFactory factory = DocumentBuilderFactory
				.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			final Document document = builder.parse(opfFile);
			final NodeList itemList = document.getElementsByTagNameNS(OPF_NS,
					"item");

			final Map<URI, String> map = Maps.newLinkedHashMap();
			for (int i = 0; i < itemList.getLength(); i++) {
				final Element item = (Element) itemList.item(i);
				final URI uri = new URI(item.getAttribute("href"));
				if (uri.isAbsolute() && !map.containsKey(uri))
					map.put(uri, item.getAttribute("media-type"));
			}
			externalItems = ImmutableSortedMap.copyOf(map);
		} catch (final ParserConfigurationException e) {
			throw new IllegalStateException(e);
		}

	}

	public ImportMapping getImportMapping() {
		if (importMapping == null) {
			importMapping = new ImportMapping();
			for (final Entry<URI, String> entry : externalItems.entrySet()) {
				final String fileName = entry.getKey().getSchemeSpecificPart()
						.concat(EXTENSION.get(entry.getValue()));
				final ImportObject io = new ImportObject();
				io.setTextgridUri(entry.getKey().toString());
				io.setLocalData(fileName);
				if (entry.getValue().equals("application/xhtml+xml")) {
					io.setRewriteMethod(RewriteMethod.XML);
					io.setRewriteConfig("internal:html#html");
				}
				importMapping.add(io);
			}
		};
		return importMapping;
	}

	public String getFileName(final String uri) {
		return getImportMapping().getImportObjectForTextGridURI(uri).getLocalData();
	}

}
