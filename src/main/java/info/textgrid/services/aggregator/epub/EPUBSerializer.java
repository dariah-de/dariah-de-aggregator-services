package info.textgrid.services.aggregator.epub;

import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.services.aggregator.ITextGridRep;
import info.textgrid.services.aggregator.teicorpus.CorpusBasedExporter;
import info.textgrid.services.aggregator.util.StylesheetManager;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.Source;

import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XsltTransformer;

import org.apache.cxf.helpers.FileUtils;
import org.xml.sax.SAXException;

import com.google.common.base.Optional;
import com.google.common.io.ByteStreams;
import com.google.common.io.Files;

public class EPUBSerializer extends CorpusBasedExporter implements
		StreamingOutput {

	private static final URI EPUB_XSL = URI
			.create("/stylesheets/epub.xsl");
	private static final URI GROUP_XSL = URI.create("/stylesheets/corpus2groups.xsl");
	private static final URI EPUB_AGG_XSL = URI.create("/stylesheets/epub-agg.xsl");
	private static final Logger logger = Logger.getLogger(EPUBSerializer.class
			.getCanonicalName());

	private final Optional<String> sid;
	private StylesheetManager stylesheetManager;
	private File workingDir;
	private Optional<URI> providedStylesheet = Optional.absent();
	private File corpus;

	public EPUBSerializer(final ITextGridRep repository,
			final StylesheetManager stylesheetManager, final String uriList,
			final Optional<String> sid, final Request request) {
		super(repository, request, uriList);
		this.stylesheetManager = stylesheetManager;
		this.sid = sid;
		setMediaType("application/epub+zip");
		setFileExtension("epub");
		setFlatCorpus(true);
		logger.info(MessageFormat.format("Started EPUB serialization for {0} after {1}",
				uriList, stopwatch));
	}

	public void setStylesheet(final URI stylesheet) {
		this.providedStylesheet = Optional.fromNullable(stylesheet);
	}

	@Override
	public ResponseBuilder createResponse() throws MetadataParseFault,
			ObjectNotFoundFault, IoFault, AuthFault,
			ProtocolNotImplementedFault, IOException, SaxonApiException {

		super.createResponse();
		if (isNotModified())
			return getResponseBuilder();

		workingDir = Files.createTempDir();
		logger.fine(MessageFormat.format("Using {0} to build the E-Book", workingDir));

		corpus = new File(workingDir, "corpus.xml");
		setBufferFactory(new FileBufferFactory(corpus));
		final Source source = loadSource(true);

		final XsltTransformer transformer = configureTransformer(source);
		transformer.transform();
		logger.fine(MessageFormat.format("Finished EPUB transformation for {0} after {1}",
				this, stopwatch));
		return getResponseBuilder();
	}

	private XsltTransformer configureTransformer(final Source source)
			throws SaxonApiException, IOException {
		final XsltTransformer transformer;
		if (providedStylesheet.isPresent())
			transformer = getStylesheetManager().getStylesheet(
					providedStylesheet.get(), sid, false, false).load();
		else {
			final Serializer rawEpubSerializer = getStylesheetManager().xsltProcessor.newSerializer(new File(workingDir, "output.xml"));
			if (getSourceType() == SourceType.XML) {
				transformer = getStylesheetManager().getStylesheet(EPUB_XSL, sid, false, true).load();
				transformer.setDestination(rawEpubSerializer);
			} else {
				transformer = getStylesheetManager().getStylesheet(GROUP_XSL, getSid(), false, true).load();
				final XsltTransformer transformer2 = getStylesheetManager().getStylesheet(EPUB_AGG_XSL, sid, false, true).load();
				transformer.setDestination(transformer2);
				transformer2.setDestination(rawEpubSerializer);
			}
		}

		transformer.setSource(source);
		return transformer;
	}

	private StylesheetManager getStylesheetManager() {
		return stylesheetManager;
	}

	@Override
	public void write(final OutputStream output) throws IOException,
			WebApplicationException {
		logger.info("Starting EPUB zipping for " + this + " after " + stopwatch);
		final ZipOutputStream zip = new ZipOutputStream(output);
		try {

			// first entry is the uncompressed mimetype marker
			new MimeTypeEntry("application/epub+zip").writeTo(zip);

			final File mimeTypeFile = new File(workingDir, "mimetype");
			final URI base = workingDir.toURI();
			final File opsDir = new File(workingDir, "OPS");
			final OPFManifest manifest = new OPFManifest(new File(opsDir,
					"content.opf"));
			final ConfigurableXMLRewriter xhtmlRewriter = new ConfigurableXMLRewriter(
					manifest.getImportMapping(), true);
			xhtmlRewriter.configure(URI.create("internal:html#html"));
			final ConfigurableXMLRewriter opfRewriter = new ConfigurableXMLRewriter(
					manifest.getImportMapping(), true);
			opfRewriter.configure(URI.create("internal:epub#opf"));

			final Deque<File> queue = new LinkedList<File>();
			queue.push(workingDir);
			while (!queue.isEmpty()) {
				final File directory = queue.pop();

				for (final File child : directory.listFiles()) {
					// filter stuff we don't want:
					if (child.equals(mimeTypeFile) || child.equals(corpus) || child.equals(new File(workingDir, "copy.xml"))) {
						continue;
					}

					String name = base.relativize(child.toURI()).getPath();
					if (child.isDirectory()) {
						queue.push(child);
						if (!name.endsWith("/")) {
							name = name.concat("/");
						}
						zip.putNextEntry(new ZipEntry(name));
					} else {
						zip.putNextEntry(new ZipEntry(name));

						if (Files.getFileExtension(name).equals("html")) {
							xhtmlRewriter.rewrite(new FileInputStream(child),
									zip);
						} else if (Files.getFileExtension(name).equals("opf")) {
							opfRewriter
									.rewrite(new FileInputStream(child), zip);
						} else {
							Files.copy(child, zip);
						}
						zip.closeEntry();
					}
				}
			}

			logger.info(MessageFormat.format(
					"Adding {0} external items to EPUB for {1} after {2}",
					manifest.externalItems.size(), this, stopwatch));

			// now we need to add those files that are referenced by
			// absolute URI in the manifest
			for (final Entry<URI, String> externalItem : manifest.externalItems
					.entrySet()) {
				final String pseudoFileName = base.relativize(
						new File(opsDir, manifest.getFileName(externalItem
								.getKey().toString())).toURI()).getPath();
				zip.putNextEntry(new ZipEntry(pseudoFileName));
				ByteStreams.copy(
						repository.getContent(externalItem.getKey(), sid.orNull()), zip);
				zip.closeEntry();
			}
			logger.info(MessageFormat.format(
					"Successfully exported EPUB for {0} after {1}",
					this, stopwatch));
		} catch (final SAXException e) {
			throw new WebApplicationException(e);
		} catch (final URISyntaxException e) {
			throw new WebApplicationException(e);
		} catch (final XMLStreamException e) {
			throw new WebApplicationException(e);
		} catch (final ObjectNotFoundFault e) {
			throw new WebApplicationException(e);
		} catch (final MetadataParseFault e) {
			throw new WebApplicationException(e);
		} catch (final IoFault e) {
			throw new WebApplicationException(e);
		} catch (final ProtocolNotImplementedFault e) {
			throw new WebApplicationException(e);
		} catch (final AuthFault e) {
			throw new WebApplicationException(e);
		} finally {
			zip.close();
			FileUtils.removeDir(workingDir);
			stopwatch.stop();
		}

	}

}
