package info.textgrid.services.aggregator.html;

import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.services.aggregator.AbstractExporter;
import info.textgrid.services.aggregator.ITextGridRep;
import info.textgrid.services.aggregator.teicorpus.CorpusBasedExporter;
import info.textgrid.services.aggregator.util.StylesheetManager;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;

import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XsltExecutable;
import net.sf.saxon.s9api.XsltTransformer;

import com.google.common.base.Optional;
import com.google.common.base.Throwables;

/**
 * The essential steps:
 *
 * <ol>
 * <li>Construct the transformation source.
 * <p>
 * Depends on different factors: If it's an aggregation, we need to recursively
 * construct a teiCorpus first. If we need to parse the
 * {@code <?xsl-stylesheet?>} processing instruction, we need to at least
 * partially parse the document twice, so we download it to a temporary buffer.
 * Otherwise, the source could be the result stream of the call to TG-crud.
 * </p>
 * <p>
 * Includes some, potentially longish, requests to TextGridRep or other sources
 * that may cause exceptions.
 * </p>
 * </li>
 * <li>Load and configure the stylesheet.
 * <p>
 * Typically, the order is argument-specified > associated (via pi) > default
 * stylesheet, with the associated stylesheet only checked on request.
 * </p>
 * </li>
 * <li>Perform the transformation and output the result.
 * <p>
 * This step essentially requires the output stream to be present.
 * </p>
 * </li>
 * </ol>
 *
 * @author Thorsten Vitt <thorsten.vitt@uni-wuerzburg.de>
 *
 */
public class HTMLWriter extends CorpusBasedExporter implements StreamingOutput {
	private static final URI TO_HTML_XSL = URI.create("/stylesheets/tohtml.xsl");
	private static final URI EXTRACT_BODY_XSL = URI.create("/stylesheets/extractbody.xsl");
	private static final URI SIMULATE_XSL = URI.create("/stylesheets/frame.xsl");

	private Optional<URI> explicitStylesheetURI = Optional.absent();
	private boolean refreshStylesheet = false;
	private boolean embedded = false;
	private boolean simulate = false;

	private Optional<URI> css = Optional.absent();
	ObjectType metadata;

	private Optional<URI> associatedStylesheetURI = Optional.absent();

	private XsltTransformer transformer;

	private String actualStylesheetLabel;

	private Optional<String> requestedMediaType;

	private Source source;
	private final StylesheetManager stylesheetManager;
	private Optional<String> id = Optional.absent();
	private Optional<String> linkUrlPattern = Optional.absent();
	private Optional<String> toc = Optional.absent();
	private static final Logger logger = Logger.getLogger(HTMLWriter.class.getCanonicalName());



	// Constructor and configuration

	public HTMLWriter(final ITextGridRep repository, final StylesheetManager stylesheetManager, final String rootURIs, final Request request) {
		super(repository, request, rootURIs);
		logger.log(Level.INFO, "Starting HTML export for {0}", rootURIs);
		this.stylesheetManager = stylesheetManager;
		setDisposition(Disposition.INLINE);
		setFileExtension("html");
		setMediaType(MediaType.TEXT_HTML);
		logger.info(MessageFormat.format("Started HTML serialization for {0} after {1}",
				this, stopwatch));
	}

	public HTMLWriter(final ITextGridRep repository, final StylesheetManager stylesheetManager, final String rootURIs,
			final URI stylesheetURI, final boolean refreshStylesheet,
			final boolean readStylesheetPI, final boolean embedded,
			final URI css, final String sid, final String mediaType,
			final String id, final Request request) {

		this(repository, stylesheetManager, rootURIs, request);

		this.explicitStylesheetURI = Optional.fromNullable(stylesheetURI);
		this.refreshStylesheet = refreshStylesheet;
		this.readStylesheetPI = readStylesheetPI;
		this.embedded = embedded;

		this.css = Optional.fromNullable(css);
		this.sid(sid);
		this.id = Optional.fromNullable(id);
		this.requestedMediaType = Optional.fromNullable(mediaType);
	}

	public AbstractExporter stylesheet(final URI uri) {
		this.explicitStylesheetURI = Optional.fromNullable(uri);
		return this;
	}

	public HTMLWriter simulate(final boolean simulate) {
		this.simulate = simulate;
		return this;
	}

	public AbstractExporter refresh(final boolean refresh) {
		this.refreshStylesheet = refresh;
		return this;
	}

	public AbstractExporter embedded(final boolean embedded) {
		this.readStylesheetPI = embedded;
		if (embedded) {
			this.sourceType = SourceType.XML;
		}
		return this;
	}
	
	public HTMLWriter linkUrlPattern(final String linkUrlPattern) {
		this.linkUrlPattern = Optional.fromNullable(linkUrlPattern);
		return this;
	}

	public AbstractExporter css(final URI css) {
		this.css = Optional.fromNullable(css);
		return this;
	}

	void detectEmbeddedStylesheet(final Source source) {
		try {
			final Source associatedStylesheet =
					TransformerFactory.newInstance()
					.getAssociatedStylesheet(source, null, null, null);
			this.associatedStylesheetURI = Optional.of(new URI(
					associatedStylesheet.getSystemId()));
			logger.log(Level.INFO, "Detected associated stylesheet {0}, URI {1}", new Object[] { associatedStylesheet, associatedStylesheetURI.get()});

		} catch (final TransformerConfigurationException e) {
			logger.log(Level.WARNING, "Failed to load stylesheet from <?xsl-stylesheet?> declaration", e);
		} catch (final TransformerFactoryConfigurationError e) {
			logger.log(Level.WARNING, "Failed to load stylesheet from <?xsl-stylesheet?> declaration", e);
		} catch (final URISyntaxException e) {
			logger.log(Level.WARNING, "Failed to load stylesheet from <?xsl-stylesheet?> declaration", e);
		}
	}

	private XsltExecutable getStylesheet() throws SaxonApiException,
			IOException {
		try {
            if (explicitStylesheetURI.isPresent()) {
                actualStylesheetLabel = explicitStylesheetURI.get().toString() + " (explicit)";
                return stylesheetManager.getStylesheet(explicitStylesheetURI.get(), getSid(),
                        refreshStylesheet, false);
            } else if (associatedStylesheetURI.isPresent()) {
                actualStylesheetLabel = associatedStylesheetURI.get().toString() + " (associated)";
                return stylesheetManager.getStylesheet(associatedStylesheetURI.get(), getSid(),
                        refreshStylesheet, false);
            };
		} catch (final SaxonApiException e) {
			logger.log(Level.WARNING, MessageFormat.format("Failed to load stylesheet {0}, falling back to internal",
					actualStylesheetLabel), e);
		}
        actualStylesheetLabel = "(internal)";
        return stylesheetManager.getStylesheet(TO_HTML_XSL, getSid(), false, true);
	}

	private Source getSource() throws ObjectNotFoundFault, MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, IOException {
		if (this.source == null) {
			this.source = loadSource(readStylesheetPI);
			if (readStylesheetPI && sourceType == SourceType.XML) {
				detectEmbeddedStylesheet(source);
				source = loadSource(readStylesheetPI);
			}
		}
		return source;
	}

	public XsltTransformer getTransformer() throws SaxonApiException, IOException {
		if (transformer != null)
			return transformer;

		final XsltTransformer transformer = getStylesheet().load();
		if (getSid().isPresent()) {
			transformer.setURIResolver(new TGUriResolver(repository, getSid()));
		}
		final String graphicsURLPattern = repository.getCRUDRestEndpoint()
				+ "/@URI@/data"
				+ (getSid().isPresent()? "?sessionId=" + getSid().get() : "");
		logger.info("Configured graphics URL pattern = " + graphicsURLPattern);
		transformer.setParameter(new QName("graphicsURLPattern"),
				new XdmAtomicValue(graphicsURLPattern));
		if (linkUrlPattern.isPresent()) {
			logger.info("Configured link URL pattern = " + linkUrlPattern.get());
			transformer.setParameter(new QName("linkURLPattern"),
					new XdmAtomicValue(linkUrlPattern.get()));
		}
		if (css.isPresent()) {
			transformer.setParameter(new QName("cssFile"), new XdmAtomicValue(
					css.get()));
		}
		transformer.setParameter(new QName("embedded"), new XdmAtomicValue(
				embedded || simulate));

		if (id.isPresent())
			transformer.setParameter(new QName("ID"), new XdmAtomicValue(id.get()));
		
		if (toc.isPresent())
			transformer.setParameter(new QName("onlyTOC"), new XdmAtomicValue(toc.get()));

		transformer.setSource(source);

		if (requestedMediaType.isPresent()) {
			transformer.getUnderlyingController().setOutputProperty(
					OutputKeys.MEDIA_TYPE, requestedMediaType.get());
		}

		this.transformer = transformer;
		logger.log(Level.INFO, MessageFormat.format("Prepared XSLT stylesheet {1} for {0} after {2}", this, actualStylesheetLabel, stopwatch.toString()));
		return transformer;
	}

	@Override
	public void write(final OutputStream out) throws IOException,
			WebApplicationException {
		try {
			logger.log(Level.INFO, MessageFormat.format("Ready for transformation of {0} after {1}", this, stopwatch.toString()));
			final Serializer serializer = stylesheetManager.xsltProcessor.newSerializer(out);
			if (simulate) {
				final XsltTransformer frame = stylesheetManager.getStylesheet(SIMULATE_XSL, getSid(), false, true).load();
				frame.setDestination(serializer);
				frame.setParameter(new QName("extraclass"), new XdmAtomicValue(getRootObjects().iterator().next().getGeneric().getGenerated().getProject().getId()));
				getTransformer().setDestination(frame);
			} else if (embedded) {
				final XsltTransformer extractBody = stylesheetManager.getStylesheet(EXTRACT_BODY_XSL, getSid(), false, true).load();
				extractBody.setDestination(serializer);
				extractBody.setParameter(new QName("extraclass"), new XdmAtomicValue(getRootObjects().iterator().next().getGeneric().getGenerated().getProject().getId()));
				getTransformer().setDestination(extractBody);
			} else {
				getTransformer().setDestination(serializer);
			}
			getTransformer().transform();

		} catch (final Exception e) {
			logger.log(Level.SEVERE, MessageFormat.format("Transformation of {0} failed ({2}) after {1}", this, stopwatch.toString(), e.getMessage()));
			Throwables.propagateIfPossible(e, IOException.class,
					WebApplicationException.class);
			Throwables.propagate(e);
		}
		logger.log(Level.INFO, MessageFormat.format("Finished and delivered transformation of {0} after {1}", this, stopwatch.toString()));
		stopwatch.stop();
	}



	@Override
	protected ResponseBuilder doEvaluatePreconditions()
			throws MetadataParseFault, ObjectNotFoundFault, IoFault, AuthFault {
		if (refreshStylesheet)
			return null;
		else
			return super.doEvaluatePreconditions();
	}

	@Override
	public ResponseBuilder createResponse() throws ObjectNotFoundFault, MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, IOException, SaxonApiException {
		if (isNotModified())
			return getResponseBuilder();

		final Source source = getSource();
		getTransformer().setSource(source);

		if (requestedMediaType.isPresent()) {
			setMediaType(requestedMediaType.get());
		} else {
			final Properties outputProperties = getTransformer()
					.getUnderlyingController().getOutputProperties();
			setMediaType(outputProperties.getProperty(OutputKeys.MEDIA_TYPE,
					"text/html"));
		}
		return super.createResponse();
	}

	public HTMLWriter toc(final String toc) {
		this.toc = Optional.fromNullable(toc);
		return this;
	}
}