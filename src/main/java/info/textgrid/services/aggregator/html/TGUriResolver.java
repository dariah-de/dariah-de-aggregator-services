package info.textgrid.services.aggregator.html;

import info.textgrid.services.aggregator.ITextGridRep;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;

import com.google.common.base.Optional;
import com.ibm.icu.text.MessageFormat;

public class TGUriResolver implements URIResolver {

	private final Optional<String> sid;
	private final String crudRestEndpoint;
	final static Logger logger = Logger
			.getLogger("info.textgrid.services.aggregator.html.URIResolver");

	public TGUriResolver(final ITextGridRep repository,
			final Optional<String> sid) {
		super();
		crudRestEndpoint = repository.getCRUDRestEndpoint();
		this.sid = sid;
	}

	public TGUriResolver(final ITextGridRep repository) {
		this(repository, Optional.<String> absent());
	}

	public TGUriResolver(final ITextGridRep repository, final String sid) {
		this(repository, sid == null || "".equals(sid) ? Optional
				.<String> absent() : Optional.of(sid));
	}

	public static boolean isResolveable(final URI uri) {
		final String scheme = uri.getScheme();
		return "textgrid".equals(scheme) || "hdl".equals(scheme);
	}

	@Override
	public Source resolve(final String href, final String base) throws TransformerException {
		logger.finer(MessageFormat.format(
				"Trying to resolve href={0}, base={1}", href, base));
		try {
			final URI uri = new URI(href).resolve(base);
			if (isResolveable(uri)) {
				final StringBuilder resolved = new StringBuilder(
						crudRestEndpoint);
				resolved.append('/').append(uri.getScheme()).append(':')
						.append(uri.getSchemeSpecificPart()).append("/data");
				if (sid.isPresent()) {
					resolved.append("?sessionId=").append(sid.get());
				}
				if (uri.getFragment() != null) {
					resolved.append('#').append(uri.getFragment());
				}
				final URL url = new URL(resolved.toString());
				logger.log(Level.FINER,
						MessageFormat.format("Resolved {0} to {1}", uri, url));
				return new StreamSource(url.openStream(), href);
			} else {
				logger.log(Level.FINER, "Did not resolve {0}", uri);
				return null;
			}
		} catch (final URISyntaxException e) {
			throw new TransformerException(e);
		} catch (final MalformedURLException e) {
			throw new TransformerException(e);
		} catch (final IOException e) {
			throw new TransformerException(e);
		}
	}

}
