package info.textgrid.services.aggregator;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Map;

import javax.ws.rs.WebApplicationException;

import com.google.common.io.InputSupplier;

import info.textgrid.middleware.tgsearch.client.SearchClient;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;

public interface ITextGridRep {

	public SearchClient getPublicSearchClient();

	public TGCrudService getCRUDService();

	public String getConfValue(final String key) throws WebApplicationException;

	public abstract InputStream getContent(final URI uri, String sid)
			throws ObjectNotFoundFault, MetadataParseFault, IoFault,
			ProtocolNotImplementedFault, AuthFault, IOException;

	public InputStream getContent(final URI uri) throws ObjectNotFoundFault, MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, IOException;

	public abstract TGOSupplier<InputStream> read(final URI uri, final String sid)
			throws WebApplicationException;

	public abstract SearchClient createPrivateSearchClient(final String sid);

	public interface TGOSupplier<T> extends InputSupplier<T> {
		public ObjectType getMetadata();
	}

	public String getCRUDRestEndpoint();

	String getCONF_ENDPOINT();

	Map<String, String> getConfig();

}