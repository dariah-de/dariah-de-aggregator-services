package info.textgrid.services.aggregator;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletContext;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import info.textgrid.services.aggregator.text.PlainTextWriter;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.Descriptions;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;

import com.google.common.base.Optional;

import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.services.aggregator.epub.EPUBSerializer;
import info.textgrid.services.aggregator.html.HTMLWriter;
import info.textgrid.services.aggregator.pdf.PDF;
import info.textgrid.services.aggregator.teicorpus.TEICorpusExporter;
import info.textgrid.services.aggregator.util.StylesheetManager;
import info.textgrid.services.aggregator.zip.ZipResult;
import net.sf.saxon.s9api.SaxonApiException;

@Description(title = "TextGrid Aggregator Service",
	value = "The Aggregator is a service to export and convert TextGrid documents. It is able to recursively process collections, editions, and other TextGrid aggregations.")
@CrossOriginResourceSharing(allowAllOrigins=true)
public class REST {

	private final ITextGridRep repository;
	private String endpointUrl;

	private StylesheetManager stylesheetManager;

	private StylesheetManager getStylesheetManager() {
		if (stylesheetManager == null)
			stylesheetManager = new StylesheetManager(servlet, repository);
		return stylesheetManager;
	}

	@Context
	private ServletContext servlet;

	public REST(final ITextGridRep repository, String endpointUrl) {
		this.repository = repository;
		this.endpointUrl = endpointUrl;
	}

	@GET
	@Path("/teicorpus/{uris}")
	@Produces("application/tei+xml")
	@Descriptions({
			@Description(target = DocTarget.METHOD, value = "Creates a TEI corpus of all the TEI documents (recursively) aggregated by the given aggregation"),
			@Description(target = DocTarget.RETURN, value = "TEI corpus document") })
	public Response getCorpus(
			@Description("TextGrid URIs of the root objects, separated by commas") @PathParam("uris") final String uriList,
			@Description("Whether to generate a Content-Disposition: attachment header") @QueryParam("attach") @DefaultValue("true") final boolean attach,
			@Description("If true, no intermediate TEI corpus documents will be generated for intermediate aggregations, hierarchical structure will be lost") @QueryParam("flat") @DefaultValue("false") final boolean flat,
			@Description("Title for the container if multiple root objects are given") @QueryParam("title") final String titleArgument,
			@Description("Session id for accessing restricted resources") @QueryParam("sid") final String sid,
			@Description("Also access sandboxed data") @QueryParam("sandbox") final boolean sandbox,
			@Context final Request request) throws URISyntaxException,
			ObjectNotFoundFault, MetadataParseFault, IoFault, AuthFault,
			ProtocolNotImplementedFault, IOException, SaxonApiException {

		final TEICorpusExporter exporter = new TEICorpusExporter(repository,
				request, uriList);
		exporter.setFlat(flat);
		exporter.setTitle(titleArgument);
		exporter.sid(sid);
		exporter.sandbox(sandbox);

		return exporter.createResponse().build();
	}

	@GET
	@Path(value = "/epub/{object}")
	@Produces(value = "application/epub+zip")
	@Description("Converts the given TEI object or the aggregation of TEI objects to an E-Book in EPUB format")
	public Response getEPUB(
			@Description("The TextGrid URI(s) of the object(s) to convert, separated by commas. Should be either TEI objects or aggregations of TEI (and maybe other) objects") @PathParam("object") final String uriList,
			@Description("URL of an alternative stylesheet to use. Must be compatible.") @QueryParam("stylesheet") final URI xsluri,
			@Description("Title if multiple root objects given") @QueryParam("title") final String titleParam,
			@Description("Session ID for accessing protected objects") @QueryParam("sid") final String sid,
			@Description("Also access sandboxed data") @QueryParam("sandbox") final boolean sandbox,
			@Context final Request request) throws ObjectNotFoundFault,
			MetadataParseFault, IoFault, AuthFault,
			ProtocolNotImplementedFault, IOException, SaxonApiException {

		final EPUBSerializer serializer = new EPUBSerializer(repository,
				getStylesheetManager(), uriList, Optional.fromNullable(sid),
				request);
		serializer.setStylesheet(xsluri);
		serializer.setTitle(titleParam);
		serializer.sandbox(sandbox);

		return serializer.createResponse().build();
	}

	@GET
	@Path(value = "/html/{object}")
	@Produces(value = "text/html")
	@Descriptions({
		@Description(target = DocTarget.METHOD, value = "Generates HTML output. This is typically fast, and it is also used at textgridrep.de.", title = "HTML generator"),
		@Description(target = DocTarget.RETURN, value = "Either an XHTML document (expect HTML5 elements), or a XHTML fragment containing only the body, if the embedded parameter is true.")
	})
	public Response getHTML(
			@Description("The TextGrid URIs of the TEI document(s) or aggregation(s) to transform, separated by commas") @PathParam("object") final String uriList,
			@Description("If given, an alternative XSLT stylesheet to use. Must be a textgrid URI.") @QueryParam("stylesheet") final URI xsluri,
			@Description("If true, check for an <?xsl-stylesheet?> processing instruction in the document to render. Only textgrid: URIs will be resolved.") @QueryParam("pi") @DefaultValue("true") final boolean pi,
			@Description("If true and a stylesheet has been given, force reloading the stylesheet, do not cache") @QueryParam("refreshStylesheet") final boolean refreshStylesheet,
			@Description("Session ID to access protected resources") @QueryParam("sid") final String sid,
			@Description("If true, an HTML fragment consisting of a <div class='body'> element containing the contents of the HTML <body> will be returned, ready to be embedded in an existing HTML page") @QueryParam("embedded") final boolean embedded,
			@Description("URL of the CSS that should be referenced in the HTML that is created") @QueryParam("css") final URI css,
			@Description("URL pattern for links. @URI@ will be replaced with the textgrid: URI.") @QueryParam("linkPattern") final String linkURLPattern,
			@Description("The requested content type. E.g., text/html or text/xml") @QueryParam("mediatype") final String mediaType,
			@Description("An XML ID. If given, only this element will be transformed.") @QueryParam("id") final String id,
			@Description("If true, a full webpage that looks similar to textgridrep.de's browse view will be returned") @QueryParam("simulate") @DefaultValue("false") final boolean simulate,
			@Description("If given, generate a table of contents instead of a full HTML document") @QueryParam("toc") final String toc,
			@Description("Also access sandboxed data") @QueryParam("sandbox") final boolean sandbox,
			@Context final Request request) throws ObjectNotFoundFault,
			MetadataParseFault, IoFault, AuthFault,
			ProtocolNotImplementedFault, WebApplicationException, IOException,
			SaxonApiException, ExecutionException {

		final HTMLWriter writer = new HTMLWriter(repository,
				getStylesheetManager(), uriList, xsluri, refreshStylesheet, pi,
				embedded, css, sid, mediaType, id, request);
		writer.sandbox(sandbox);
		writer.simulate(simulate);
		writer.linkUrlPattern(linkURLPattern);
		writer.toc(toc);
		return writer.createResponse().build();
	}

	@GET
	@Path(value = "/text/{object}")
	@Produces(value = "text/plain")
	@Descriptions({
			@Description(target = DocTarget.METHOD, value="Generates plain text output."),
			@Description(target = DocTarget.RETURN, value = "A plain text document.")
	})
	public Response getText(
			@Description("The TextGrid URIs of the TEI document(s) or aggregation(s) to transform, separated by commas") @PathParam("object") final String uriList,
			@Description("Session ID to access protected resources") @QueryParam("sid") final String sid,
			@Description("Also access sandboxed data") @QueryParam("sandbox") final boolean sandbox,
            @Context final Request request) throws IoFault, AuthFault, IOException, SaxonApiException, ObjectNotFoundFault, MetadataParseFault, ProtocolNotImplementedFault {
		PlainTextWriter writer = new PlainTextWriter(repository, getStylesheetManager(), uriList, request);
		writer.sid(sid);
		writer.sandbox(sandbox);
		return writer.createResponse().build();
	}

	@GET
	@Path(value = "/zip/{objects}")
	@Produces("application/zip")
	@Descriptions({
		@Description(target=DocTarget.METHOD, value="Creates a ZIP containing the specified objects and everything that has been aggregated by them, "
				+ "optionally transformed and filtered. Links within supported XML documents will be rewritten to relative URLs if the target document "
				+ "is also packed into this ZIP. This method may take quite a while depending on the number of objects, and it will not start "
				+ "returning something until the metadata for everything that will be exported has been collected, so increase your timeouts ..."),
		@Description(target=DocTarget.RETURN, value="The ZIP file returned will usually contain a content document plus a sidecar .meta file for each "
				+ "exported object. The .meta file contains the raw metadata according to the TextGrid metadata schema. For aggregations (editions, "
				+ "collections), we will typically create both a directory and an ORE content file containing the list of items. "
				+ "Additionally, a file called /.INDEX.imex will be included in the archive. This file contains the list of exported objects "
				+ "together with the local filename, the name of the local .meta file, the link rewriting method used, and the original TextGrid URI. "
				+ "You can use this file to re-import the whole set of files using the TextGridLab. See the Link Rewriter Library's documentation "
				+ "for an XML schema and description of the format. \n\nThe ZIP file may contain ZIP comments refering to warning or informational "
				+ "messages during export.")
	})
	public Response getZIP(
			@Description("The TextGridURIs of the TEI documents or aggregations to zip, separated by commas (,)") @PathParam("objects") final String uriList,
			@Description("Session ID to access protected resources") @QueryParam("sid") final String sid,
			@Description("(optional) title for the exported data, currently only used for generating the filename. If none is given, the first title of the first object will be used.") @QueryParam("title") final String title,
			@QueryParam("filenames") @DefaultValue("{parent|/}{author}-{title}*.{ext}") @Description("Pattern for the generated filenames in the ZIP files.") final String filenames,
			@QueryParam("metanames") @DefaultValue("{filename}.meta") @Description("Pattern for the filenames for the metadata files in the ZIP files.") final String metanames,
			@QueryParam("dirnames") @DefaultValue("{parent|/}{title}*") @Description("Pattern for the directory names generated for aggregations etc. This pattern applied to the parent aggregation is available as {parent} in filenames and metanames.") final String dirnames,

			@QueryParam("only") @Description("If at least one only parameter is given, restrict export to objects with the given MIME types") final List<String> only,

			@QueryParam("meta") @Description("Include metadata and aggregation files in the ZIP file.") @DefaultValue("true") final boolean meta,

			@QueryParam("transform") @Description("(EXPERIMENTAL) Transform each XML document before zipping. Values currently available are text, html, or the textgrid: URI of an XSLT stylesheet.") final String transform,
			@QueryParam("query") @Description("(EXPERIMENTAL) perform the given TGsearch query and use its result as root objects instead of the objects.") final String query,
			@QueryParam("filter") @Description("for query: additional filters") final List<String> filter,
			@QueryParam("target") @Description("if query is used, the query target (metadata, fulltext or both)") @DefaultValue("both") final String target,
			@QueryParam("start") @Description("for query: start at result no.") @DefaultValue("0") int start,
			@QueryParam("stop") @Description("for query: max. number of results") @DefaultValue("65535") int stop,
			@QueryParam("stream") @Description("if true, favor fast results over ideal rewriting") @DefaultValue("true") boolean stream,
			@Description("Also access sandboxed data") @QueryParam("sandbox") final boolean sandbox,
			@Context final Request request) throws MetadataParseFault,
			ObjectNotFoundFault, IoFault, AuthFault,
			ProtocolNotImplementedFault, IOException, SaxonApiException {

		final ZipResult zipResult = new ZipResult(repository,
				getStylesheetManager(), request, uriList, filenames, metanames,
				dirnames, only, meta, transform);
		zipResult.sandbox(sandbox);
		if (title != null)
			zipResult.setTitle(title);
		if (sid != null)
			zipResult.sid(sid);
		if (query != null)
			zipResult.initFromSearch(query, target, start, stop, filter);
		zipResult.streaming(stream);
		return zipResult.createResponse().build();
	}

	@GET
	@Path(value = "/pdf/{object}")
	@Produces("application/pdf")
	@Description("(BROKEN) the PDF export will currently not work on any installed instance of the service.")
	public Response getPDF(@PathParam("object") final URI uri,
			@QueryParam("sid") final String sid, @Context final Request request)
			throws MetadataParseFault, ObjectNotFoundFault, IoFault, AuthFault,
			ProtocolNotImplementedFault, IOException, SaxonApiException {
		final PDF pdf = new PDF(repository, getStylesheetManager(), request,
				uri);
		pdf.sid(sid);
		return pdf.createResponse().build();

	}

	@GET
	@Path(value = "/version")
	@Produces("text/html")
	@Description("Produces an HTML page containing version and configuration information for the service instance.")
	public StreamingOutput getVersion(@Context HttpHeaders headers) {
		final Version version = new Version(repository, getStylesheetManager()).headers(headers);

		return version.get();
	}

	@GET
	@Path(value = "/help")
	@Produces("text/html")
	@Description("Returns an auto-generated help page summarizing all available arguments.")
	public StreamingOutput getHelp() throws SaxonApiException, IOException {
		return new Help(getStylesheetManager(), endpointUrl);
	}


}
