package info.textgrid.services.aggregator.zip;

import info.textgrid._import.ImportObject;
import info.textgrid._import.RewriteMethod;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.services.aggregator.AbstractExporter;
import info.textgrid.services.aggregator.ITextGridRep;
import info.textgrid.services.aggregator.tree.AggregationTreeFactory;
import info.textgrid.services.aggregator.util.StylesheetManager;
import info.textgrid.utils.export.aggregations.AggregationEntry;
import info.textgrid.utils.export.aggregations.IAggregation;
import info.textgrid.utils.export.aggregations.IAggregationEntry;
import info.textgrid.utils.export.filenames.ConfigurableFilenamePolicy;
import info.textgrid.utils.export.filenames.IFilenamePolicy;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter;
import info.textgrid.utils.linkrewriter.ImportMapping;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.text.MessageFormat;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXB;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XsltTransformer;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.ByteStreams;
import com.google.common.io.FileBackedOutputStream;
import com.google.common.net.MediaType;

public class ZipResult extends AbstractExporter implements StreamingOutput {

	
	protected class ExportingTreeFactory extends AggregationTreeFactory {
		
		public class WrappedIOException extends RuntimeException {
			private static final long serialVersionUID = -6846249739134937929L;

			public WrappedIOException(IOException cause) {
				super(cause);
			}
		}

		private final ZipOutputStream zip;	

		protected ExportingTreeFactory(ObjectType root,
				ITextGridRep repository, String sid, ZipOutputStream zip) {
			super(root, repository, sid, false);
			this.zip = zip;
			walk(root, false);
		}

		@Override
		protected boolean walk(ObjectType object, boolean again) {
			boolean result = super.walk(object, again);
			writeCurrentItem();
			return result;
		}

		private void writeCurrentItem() {
			AggregationEntry entry = getLastEntry();
			ImportEntry importEntry = addSingleItemToMapping(mapping, entry);
			if (importEntry != null)
				try {
					writeFile(zip, importEntry);
				} catch (IOException e) {
					logger.log(Level.SEVERE, "Streaming export failed", e);
					throw new WrappedIOException(e);
				}
		}

		@Override
		protected void walkAggregation(ObjectType aggregation, boolean again) {
			super.walkAggregation(aggregation, again);
			writeCurrentItem();
		}

	}

	private final ConfigurableFilenamePolicy policy;
	private final IFilenamePolicy metaPolicy;
	private ImportMapping mapping;
	private final static Logger logger = Logger.getLogger(ZipResult.class
			.getCanonicalName());

	private final Set<URI> written = Sets.newHashSet();
	private boolean onlySomeFormats;
	private List<MediaType> onlyFormats;
	private boolean includeMeta;
	private StylesheetManager stylesheetManager;
	private Optional<Transformation> transformation = Optional.absent();
	private boolean streamingMode = true;
	
	public ZipResult streaming(final boolean streamingMode) {
		this.streamingMode = streamingMode;
		return this;
	}

	private static final Function<ObjectType, String> GetURI = new Function<ObjectType, String>() {

		@Override
		public String apply(final ObjectType input) {
			return input.getGeneric().getGenerated().getTextgridUri()
					.getValue();
		}

	};

	public ZipResult(final ITextGridRep repository,
			StylesheetManager stylesheetManager, final Request request,
			final String uriList, final String filenames,
			final String metanames, final String dirnames,
			final List<String> only, final boolean includeMeta, String transform) {
		super(repository, request, uriList);
		final ConfigurableFilenamePolicy parentPolicy = ConfigurableFilenamePolicy
				.builder(dirnames).isParent().build();
		this.policy = ConfigurableFilenamePolicy.builder(filenames)
				.subPolicy("parent", parentPolicy).build();
		this.metaPolicy = ConfigurableFilenamePolicy.builder(metanames)
				.subPolicy("parent", parentPolicy)
				.subPolicy("filename", policy).build();

		if (only != null && only.size() > 0)
			this.onlySomeFormats = true;
		this.onlyFormats = Lists.transform(only,
				new Function<String, MediaType>() {

					@Override
					public MediaType apply(final String input) {
						return MediaType.parse(input);
					}

				});

		setFileExtension("zip");
		setMediaType("application/zip");
		this.includeMeta = includeMeta;

		this.stylesheetManager = stylesheetManager;
		setupTransform(transform);
	}

	private static class Transformation {
		public final String targetFormat;
		public final URI stylesheet;

		public Transformation(String targetFormat, String stylesheet) {
			this.targetFormat = targetFormat;
			this.stylesheet = URI.create(stylesheet);
		}
	}

	private static ImmutableMap<String, Transformation> BUILTIN_TRANSFORMATIONS = ImmutableMap
			.<String, Transformation> builder()
			.put("text",
					new Transformation("text/plain",
							"/tei-stylesheets/txt/tei-to-text.xsl"))
			.put("html",
					new Transformation("text/html",
							"/stylesheets/tohtml.xsl")).build();

	private void setupTransform(String transform) {
		if (transform == null || transform.isEmpty())
			return;

		if (BUILTIN_TRANSFORMATIONS.containsKey(transform))
			this.transformation = Optional.of(BUILTIN_TRANSFORMATIONS
					.get(transform));
		else if (transform.startsWith("textgrid:")
				|| transform.startsWith("hdl:"))
			this.transformation = Optional.of(new Transformation("text/xml",
					transform));
	}

	@Override
	public void write(final OutputStream output) throws IOException,
			WebApplicationException {
		final ZipOutputStream zip = new ZipOutputStream(output);
		try {
			final Iterable<ObjectType> rootObjects = getRootObjects();
			logger.log(Level.INFO, MessageFormat.format(
					"Starting {1} ZIP export after {0}", stopwatch, streamingMode? "streaming" : "fully link rewriting"));
			zip.setComment("# Exported from TextGrid -- www.textgrid.de");

			mapping = new ImportMapping();

			if (streamingMode)
				exportStreaming(zip);
			else
				exportFullRewriting(zip);

			// now serializing the mapping. Should be moved to the end since
			// writing may change the rewrite config.
			zip.putNextEntry(new ZipEntry(".INDEX.imex"));
			JAXB.marshal(mapping.toImportSpec(), zip);
			zip.closeEntry();


		} catch (final MetadataParseFault e) {
			throw new WebApplicationException(e);
		} catch (final ObjectNotFoundFault e) {
			throw new WebApplicationException(e, Status.NOT_FOUND);
		} catch (final IoFault e) {
			throw new WebApplicationException(e);
		} catch (final AuthFault e) {
			throw new WebApplicationException(e, Status.FORBIDDEN);
		} finally {
			zip.close();
			stopwatch.stop();
		}
		logger.log(Level.INFO,
				MessageFormat.format("Finished exporting after {0}", stopwatch));
	}

	private void exportFullRewriting(final ZipOutputStream zip) throws IOException, MetadataParseFault, ObjectNotFoundFault, IoFault, AuthFault {
		final Iterable<ObjectType> rootObjects = getRootObjects();
				
		final List<IAggregationEntry> roots = Lists.newArrayList();
//				.newArrayListWithCapacity(rootObjects.length);
		for (final ObjectType rootMetadata : rootObjects) {
			final IAggregationEntry entry;
			if (rootMetadata.getGeneric().getProvided().getFormat()
					.contains("aggregation")) {
				entry = AggregationTreeFactory.create(rootMetadata,
						repository, getSid().orNull());
			} else {
				entry = new AggregationEntry(rootMetadata, null);
			}
			logger.log(Level.INFO, MessageFormat.format(
					"  Built aggregation tree for {1} after {0}",
					stopwatch, GetURI.apply(rootMetadata)));
			roots.add(entry);
			addToMapping(mapping, (AggregationEntry) entry);
		}

		for (final ImportObject o : mapping) {
			final ImportEntry importEntry = (ImportEntry) o;
			writeFile(zip, importEntry);
		}
	}
	
	private void exportStreaming(final ZipOutputStream zip) throws MetadataParseFault, ObjectNotFoundFault, IoFault, AuthFault {
		for (ObjectType root : getRootObjects()) {
			new ExportingTreeFactory(root, repository, getSid().or(""), zip);			
		}
	}
	

	// FIXME refactor -> Rewrite library
	private static ImmutableMap<String, String> REWRITE_CONFIGS = null;

	private static Optional<String> getRewriteConfig(final String contentType) {
		if (REWRITE_CONFIGS == null) {
			REWRITE_CONFIGS = ImmutableMap
					.<String, String> builder()
					.put("text/tg.aggregation+xml",
							"internal:textgrid#aggregation")
					.put("text/tg.edition+tg.aggregation+xml",
							"internal:textgrid#aggregation")
					.put("text/tg.collection+tg.aggregation+xml",
							"internal:textgrid#aggregation")
					.put("text/xsd+xml", "internal:schema#xsd")
					.put("text/linkeditorlinkedfile", "internal:tei#tei")
					.put("text/xml", "internal:tei#tei")
					.put("application/xhtml+xml", "internal:html#html").build();
		}
		return Optional.fromNullable(REWRITE_CONFIGS.get(contentType));
	}

	private static class ImportEntry extends ImportObject {
		private final AggregationEntry treeEntry;

		public AggregationEntry getTreeEntry() {
			return treeEntry;
		}

		public long getLastModified() {
			return treeEntry.getMetadata().getGeneric().getGenerated()
					.getLastModified().toGregorianCalendar().getTimeInMillis();
		}

		public ImportEntry(AggregationEntry treeEntry) {
			super();
			this.treeEntry = treeEntry;
			setTextgridUri(treeEntry.getTextGridURI().toString());
			final Optional<String> rewriteConfig = ZipResult
					.getRewriteConfig(treeEntry.getFormat());
			if (rewriteConfig.isPresent()) {
				setRewriteMethod(RewriteMethod.XML);
				setRewriteConfig(rewriteConfig.get());
			} else {
				setRewriteMethod(RewriteMethod.NONE);
			}
		}

	}

	/**
	 * Recursively adds the entry to the mapping.
	 * 
	 * @param mapping
	 * @param entry
	 * @return the ImportEntry that has been created for this.
	 */
	private ImportEntry addToMapping(final ImportMapping mapping,
			final AggregationEntry entry) {
		ImportEntry importObject = addSingleItemToMapping(mapping, entry);

		if (entry instanceof IAggregation) {
			final IAggregation aggregation = (IAggregation) entry;
			for (final IAggregationEntry child : aggregation.getChildren()) {
				addToMapping(mapping, (AggregationEntry) child);
			}
		}
		return importObject;
	}

	private ImportEntry addSingleItemToMapping(final ImportMapping mapping,
			final AggregationEntry entry) {
		ImportEntry importObject = null;
		final String format = entry.getFormat();
		if (!onlySomeFormats || isAllowedFormat(format)) {
			if (transformation.isPresent() && "text/xml".equals(entry.getFormat()))
				entry.setTransformedFormat(transformation.get().targetFormat);
			importObject = new ImportEntry(entry);
			importObject.setLocalData(policy.getFilename(entry).toString());
			importObject.setLocalMetadata(metaPolicy.getFilename(entry)
					.toString());
			mapping.add(importObject);
		}
		return importObject;
	}

	private boolean isAllowedFormat(final String format) {
		if (!onlySomeFormats)
			return true;
		final MediaType thisMediaType = MediaType.parse(format);
		for (final MediaType allowed : onlyFormats) {
			if (thisMediaType.is(allowed))
				return true;
		}
		return false;
	}

	private void writeFile(final ZipOutputStream zip,
			final ImportEntry importEntry) throws IOException {
		
		URI uri = URI.create(importEntry.getTextgridUri());
		if (written.contains(uri))
			return;
		else
			written.add(uri);
		

		writeMetadata(zip, importEntry);
		writeMissingDirectories(zip, importEntry.getLocalData(), importEntry.getLastModified());

		final ZipEntry zipEntry = new ZipEntry(importEntry.getLocalData());
		zipEntry.setTime(importEntry.getTreeEntry().getMetadata().getGeneric()
				.getGenerated().getLastModified().toGregorianCalendar()
				.getTimeInMillis());

		try {
			final InputStream content = repository.getContent(importEntry
					.getTreeEntry().getTextGridURI(), getSid().orNull());

			// Content options:
			// (1) Transformation, e.g. to text. Only for xml and if configured.
			if (transformation.isPresent()
					&& ("text/xml".equals(importEntry.getTreeEntry()
							.getFormat()))) {
				zipEntry.setComment("Transformed from text/xml");
				zip.putNextEntry(zipEntry);
				XsltTransformer transformer = stylesheetManager
						.getStylesheet(transformation.get().stylesheet,
								getSid(), false, false).load();
				StreamSource source = new StreamSource(content, importEntry
						.getTreeEntry().getTextGridURI().toString());
				Serializer serializer = stylesheetManager.xsltProcessor
						.newSerializer(zip);
				transformer.setSource(source);
				transformer.setDestination(serializer);
				transformer.transform();

				// (2) Link rewriting, using XML. Only if rewrite method
				// configured.
			} else if (importEntry.getRewriteMethod().equals(RewriteMethod.XML)) {
				final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(
						mapping, true);
				rewriter.configure(URI.create(importEntry.getRewriteConfig()));
				final Optional<URI> base = policy.getBase(importEntry
						.getTreeEntry());
				if (base.isPresent()) {
					rewriter.setBase(base.get());
				}
				final FileBackedOutputStream buffer = new FileBackedOutputStream(
						1024 * 1024);
				try {
					rewriter.rewrite(content, buffer);
					zip.putNextEntry(zipEntry);
					ByteStreams.copy(buffer.getSupplier(), zip);
				} catch (final XMLStreamException e) {
					// (2a) fallback if rewriting failed
					final String errorMsg = MessageFormat
							.format("Failed to rewrite {0} (error: {1}). Exported with verbatim links instead.",
									importEntry, e.getMessage());
					logger.log(Level.WARNING, errorMsg, e);
					zipEntry.setComment(errorMsg);
					importEntry.setRewriteMethod(RewriteMethod.NONE);
					zip.putNextEntry(zipEntry);
					ByteStreams.copy(buffer.getSupplier(), zip);
				}
			} else {
				// (3) plain copy. For all other cases.
				zip.putNextEntry(zipEntry);
				ByteStreams.copy(content, zip);
			}
			zip.closeEntry();
		} catch (final Exception e) {
			Throwables.propagateIfPossible(e, IOException.class);
			throw new WebApplicationException(e);
		}
	}

	private void writeMetadata(final ZipOutputStream zip,
			final ImportEntry importEntry) throws IOException {
		if (!includeMeta)
			return;
		
		writeMissingDirectories(zip, importEntry.getLocalMetadata(), importEntry.getLastModified());
		
		final ZipEntry zipEntry = new ZipEntry(importEntry.getLocalMetadata());
		zipEntry.setTime(importEntry.getLastModified());
		zip.putNextEntry(zipEntry);

		final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(
				mapping, true);
		rewriter.configure(URI.create("internal:textgrid#metadata"));
		final Optional<URI> base = metaPolicy.getBase(importEntry
				.getTreeEntry());
		if (base.isPresent()) {
			rewriter.setBase(base.get());
		}

		final FileBackedOutputStream buffer = new FileBackedOutputStream(
				1024 * 1024);
		JAXB.marshal(importEntry.getTreeEntry().getMetadata(), buffer);
		try {
			rewriter.rewrite(buffer.getSupplier().getInput(), zip);
		} catch (final XMLStreamException e) {
			logger.log(Level.SEVERE, MessageFormat.format(
					"Error rewriting the metadata of {0}. Should not happen.",
					importEntry), e);
		}
		zip.closeEntry();
	}

	private final Set<String> writtenDirectoryNames = Sets.newHashSet();

	/**
	 * This writes 'parent directory' entries to the zip stream for all
	 * directory names required for the given filename.
	 * 
	 * @param zip
	 *            The zip stream.
	 * @param filename
	 *            The filename for which to create directory entries
	 * @param lastModified
	 *            The modification date to use for the directory entries created
	 *            in this step.
	 */
	private void writeMissingDirectories(final ZipOutputStream zip,
			final String filename, final long lastModified) {
		String[] components = filename.split("/");
		StringBuilder dir = new StringBuilder();
		for (int i = 0; i < components.length - 1; i++) {
			dir.append(components[i]).append('/');
			String path = dir.toString();
			if (!writtenDirectoryNames.contains(path)) {
				ZipEntry zipEntry = new ZipEntry(path);
				zipEntry.setTime(lastModified);
				writtenDirectoryNames.add(path);
				try {
					zip.putNextEntry(zipEntry);
					zip.closeEntry();
				} catch (IOException e) {
					logger.log(
							Level.WARNING,
							MessageFormat
									.format("Failed to write parent directory entry {0} for {1}. Skipping.",
											path, filename), e);
				}
			}
		}
	}
}
