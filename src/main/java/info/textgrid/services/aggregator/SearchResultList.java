package info.textgrid.services.aggregator;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

import info.textgrid.middleware.tgsearch.client.SearchClient;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

import com.google.common.base.Joiner;
import com.google.common.collect.ForwardingCollection;
import com.google.common.collect.Lists;
import com.google.common.collect.UnmodifiableIterator;

/**
 * List that represents all results of a search and fetches new results on
 * demand.
 * 
 */
public class SearchResultList extends ForwardingCollection<ObjectType> {

	private int chunkSize = 1000;

	public class SearchResultIterator extends UnmodifiableIterator<ObjectType> {
		
		int cursor = 0;

		@Override
		public ObjectType next() {
			if (!hasNext())
				throw new NoSuchElementException();
			if (cursor >= nextStart)
				fetchNextChunk();
			ObjectType result = availableResults.get(cursor);
			cursor++;
			return result;
		}


		@Override
		public boolean hasNext() {
			if (!started)
				fetchNextChunk();
			return cursor < hits;
		}

	}

	private List<ObjectType> availableResults = Lists.newArrayList();

	private boolean started = false; // have we submitted the first query yet?
	private boolean finished = false; // have we already submitted the last
										// query?
	private int hits = -1; // # of hits, -1 if we don't know yet
	private int nextStart = 0; //
	private int stop = Integer.MAX_VALUE;
	private final SearchClient searchClient;
	
	protected SearchResultList(final SearchClient client, final String query, final String target, List<String> filter, boolean sandbox) {
		this.searchClient = client;
		this.query = query == null || "".equals(query)? "*" : query;
		this.filter = filter;
		client.setTarget(target);
		client.setSandbox(sandbox);
	}
	
	public static SearchResultList create(final SearchClient client, final String query, final String target, List<String> filter, boolean sandbox) {
		return new SearchResultList(client, query, target, filter, sandbox);
	}
	
	private void ensureNotStarted() {
		if (started)
			throw new IllegalStateException("Cannot modify properties after starting to fetch results.");
	}
	
	public SearchResultList start(int start) {
		ensureNotStarted();
		this.nextStart = start;
		return this;
	}
	
	public SearchResultList stop(int stop) {
		ensureNotStarted();
		this.stop = stop;
		return this;
	}
	
	public SearchResultList chunkSize(int chunkSize) {
		ensureNotStarted();
		this.chunkSize = chunkSize;
		return this;
	}
	
	@Override
	public int size() {
		if (!started)
			fetchNextChunk();
		return hits;
	}

	private Logger logger = Logger.getLogger(getClass().getCanonicalName());

	private final String query;

	private final List<String> filter;


	@Override
	public Iterator<ObjectType> iterator() {
		if (finished)
			return availableResults.iterator();
		else
			return new SearchResultIterator();
	}

	@Override
	protected Collection<ObjectType> delegate() {
		return availableResults;
	}

	/**
	 * Fetches the next chunk of results.
	 * 
	 * The first call of this method will set the 'started' state, thus making
	 * the start, stop etc. properties unmodifyable. It will also allocate
	 * memory to hold all results.
	 * 
	 * Until fetching results is finished, every call to this method will try to
	 * fetch at most {@link #CHUNK_SIZE} query results. The results will then be 
	 * added to the internal list of available elements, and the nextStart and hits
	 * properties will be updated accordingly (according to the actually returned
	 * results!). When the number of totally retrieved elements is >= the number of
	 * the last call's hits value, fetching is considered finished and subsequent
	 * calls to this method will do nothing.
	 */
	protected void fetchNextChunk() {
		if (finished)
			return;
		

		Response response = searchClient.query(query, null, "", nextStart,
				Math.min(stop - nextStart, chunkSize), filter);
		hits = Integer.parseInt(response.getHits());
		logger.info(MessageFormat.format("Next query for {0}, filters {3}, sandbox {1}, {2} hits", query, searchClient.getSandbox(), hits,
				filter == null? "null" : Joiner.on("; ").join(filter)));
		if (!started) {
			availableResults = Lists.newArrayListWithExpectedSize(hits);
			started = true;
		}
		stop = Math.min(stop, hits);
		for (ResultType result : response.getResult()) {
			nextStart += 1;
			availableResults.add(result.getObject());
		}
		logger.info(MessageFormat.format("Added {0} of {1} results", nextStart,
				hits));
	}

	@Override
	public ObjectType[] toArray() {
		return standardToArray(new ObjectType[0]);
	}

	@Override
	public <T> T[] toArray(T[] array) {
		return super.toArray(array);
	}
	
	

}
