package info.textgrid.services.aggregator;

import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;

import java.text.MessageFormat;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import org.springframework.web.util.HtmlUtils;

import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;

public class GenericExceptionMapper implements ExceptionMapper<Exception> {
	Logger logger = Logger.getLogger("info.textgrid.services.aggregator");

	public static final Map<Class<? extends Exception>, Response.Status> STATUS_MAP = ImmutableMap
			.<Class<? extends Exception>, Response.Status> builder()
			.put(MetadataParseFault.class, Status.BAD_REQUEST)
			.put(ObjectNotFoundFault.class, Status.NOT_FOUND)
			.put(AuthFault.class, Status.UNAUTHORIZED)
			.put(IoFault.class, Status.INTERNAL_SERVER_ERROR)
			.put(ProtocolNotImplementedFault.class, Status.BAD_REQUEST)
			.put(IllegalArgumentException.class, Status.BAD_REQUEST)
			.build();

	private static final String HTML_TEMPLATE =
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
					"<!DOCTYPE html>\n"
					+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
					+ "    <head>\n"
					+ "        <title>{0} {1}</title>\n"
					+ "        <style type=\"text/css\">'\n"
					+ "            h1 { border-bottom: 1px solid red; }\n"
					+ "            .details { color: gray; }\n"
					+ "        '</style>\n"
					+ "    </head>\n"
					+ "    <body>\n"
					+ "        <h1>{1}</h1>\n"
					+ "        <p class=\"message\">{2}</p>\n"
					+ "        <div class=\"details\">\n"
					+ "            <pre>\n"
					+ "{3}\n"
					+ "            </pre>\n"
					+ "        </div>\n"
					+ "    </body>\n" + "</html>";

	@Override
	public Response toResponse(final Exception exception) {
		Status status;
		String message;
		if (exception instanceof WebApplicationException && ((WebApplicationException) exception).getResponse() != null /* && ((WebApplicationException) exception).getResponse().getStatus() != 500 */) {
			final WebApplicationException wae = (WebApplicationException) exception;
			status = Status.fromStatusCode(wae.getResponse().getStatus());
			final Object entity = wae.getResponse().getEntity();
			if (entity != null)
				return wae.getResponse(); // message = entity.toString();
			else
				message = status.toString();
		} else if (exception instanceof WebApplicationException
				&& exception.getCause() != null) {
			status = STATUS_MAP.get(exception.getCause().getClass());
			if (status == null) {
				status = Status
				.fromStatusCode(((WebApplicationException) exception)
						.getResponse().getStatus());
			}
			message = exception.getCause().getLocalizedMessage();
		} else {
			status = STATUS_MAP.get(exception.getClass());
			message = exception.getLocalizedMessage();
		}
		if (status == null) {
			status = Status.INTERNAL_SERVER_ERROR;
		}

		logger.log(Level.WARNING, MessageFormat.format(
				"{2} {3} returned: caught {0}: {1}", exception.getClass()
				.getSimpleName(), exception.getMessage(), status
						.getStatusCode(), status.toString()), exception);

		final Response response = toResponse(status, message, Throwables.getStackTraceAsString(exception)).build();
		return response;
	}

	/**
	 * Returns a fancy HTML-formatted response for the given parameters.
	 * @param status the status for which the response is to be generated.
	 * @param message an additional informative message. Will be HTML-escaped and displayed below the status msg
	 * @param detail a detail message, e.g., a stack trace. Will be HTML-escaped and displayed in a preformatted way below the status msg.
	 * @return the generated response.
	 */
	public static ResponseBuilder toResponse(final Status status,
			final String message, final String detail) {
		final ResponseBuilder builder = Response.status(status);
		builder.type(MediaType.APPLICATION_XHTML_XML_TYPE);
		builder.entity(prepareXHTMLMessage(status, message, detail));
		return builder;
	}

	public static String prepareXHTMLMessage(final Status status,
			final String message, final String detail) {
		return MessageFormat.format(HTML_TEMPLATE, status.getStatusCode(),
				HtmlUtils.htmlEscapeHex(status.toString()),
				HtmlUtils.htmlEscapeHex(message),
				HtmlUtils.htmlEscapeHex(detail));
	}


}
