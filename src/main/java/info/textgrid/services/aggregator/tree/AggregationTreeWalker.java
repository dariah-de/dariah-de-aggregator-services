package info.textgrid.services.aggregator.tree;

import info.textgrid.middleware.tgsearch.client.SearchClient;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import info.textgrid.services.aggregator.ITextGridRep;

import java.util.List;
import java.util.Set;

import javax.ws.rs.InternalServerErrorException;

import com.google.common.collect.Sets;

public abstract class AggregationTreeWalker {
	private ITextGridRep repository;

	protected final Set<String> seen = Sets.newHashSet();

	private String sid;

	private SearchClient privateSearchClient;

	/**
	 * Process the given object.
	 * <p>
	 * The object is first added to the {@linkplain #seen list of seen objects},
	 * then a specialized walk method is called depending on the object's
	 * metadata. The specialized walk method's <var>again</var> parameter is
	 * passed the information whether this object has been already seen before.
	 * </p>
	 *
	 * <p>
	 * The {@link AggregationTreeWalker} implementation calls
	 * {@link #walkAggregation(ObjectType, boolean)} if the object's format
	 * contains "aggregation", otherwise, the object is only added to the
	 * registry. Clients should extend or override.
	 * </p>
	 *
	 * @param object
	 *            the metadata of the object to process.
	 * @param again
	 *            whether the object has already been seen
	 * @return <code>true</code> if this method has handled the object in some
	 *         way (ATW: true iff it is an aggregation), <code>false</code> otherwise.
	 */
	protected boolean walk(final ObjectType object, final boolean again) {
		boolean secondTime = again;
		if (!again) {
			secondTime = !seen.add(object.getGeneric().getGenerated()
					.getTextgridUri().getValue());
		}

		try {
		if (object.getGeneric().getProvided().getFormat()
				.contains("aggregation")) {
				walkAggregation(object, secondTime);
			return true;
		} else
			return false;
		} catch (final NullPointerException e) {
			throw new IllegalStateException(
					"We encountered an object without required metadata: "
							+ object.getGeneric().getGenerated()
									.getTextgridUri().getValue(), e);
		}
	}

	/**
	 * Iterates through the given <var>aggregation</var>'s contents unless it is
	 * visited <var>again</var>.
	 *
	 * <p>
	 * This method will issue a
	 * {@linkplain SearchClient#listAggregation(String) TG-search request to
	 * list the given aggregation's content}. It will iterate through the
	 * results and call {@link #walk(ObjectType, boolean)} for every aggregate
	 * that is returned. If the result contains results for which the TG-search
	 * request does not return object metadata,
	 * {@link #walkInaccessibleURI(String)} will be called instead.
	 * </p>
	 *
	 * <p>
	 * Clients may extend or override if they need special treatment.
	 * </p>
	 *
	 * @param aggregation
	 *            The metadata of the aggregation to list.
	 * @param again
	 *            Whether this aggregation has been visited again, e.g., in a
	 *            different branch of the aggregation graph.
	 */
	protected void walkAggregation(final ObjectType aggregation,
			final boolean again) {
		if (again)
			return;

		// FIXME walkAggregation is also called for works, but it shouldn't.
		// This is a workaround, we actually shoud factor out the work handling
		// in the subclasses ...
		if (!aggregation.getGeneric().getProvided().getFormat().contains("aggregation"))
			return;

		if (getRepository() == null)
			throw new IllegalStateException(
					"AggregationTreeWalkers must be initialized with a repository before use.");

		final SearchClient searchClient = getSearchClient(isPublic(aggregation));
		final String id = aggregation.getGeneric().getGenerated()
				.getTextgridUri().getValue();
		try {
			final Response response = searchClient.listAggregation(id);
			final List<ResultType> result = response.getResult();
			for (final ResultType resultType : result) {
				final ObjectType aggregate = resultType.getObject();
				if (aggregate != null) {
					walk(aggregate, false);
				} else {
					walkInaccessibleURI(resultType.getTextgridUri());
				}
			}
		} catch (final InternalServerErrorException e) {
			throw new IllegalArgumentException(
					"Traversing the aggregation tree TG-search failed to list the contents of aggregation "
							+ describe(aggregation), e);
		}

	}

	/** for logging / debugging purposes */
	private static String describe(final ObjectType object) {
		final StringBuilder builder = new StringBuilder();
		try {
			builder.append(object.getGeneric().getGenerated().getTextgridUri()
					.getValue());
			builder.append(": ").append(
					object.getGeneric().getProvided().getTitle());
			builder.append(" (")
					.append(object.getGeneric().getProvided().getFormat())
					.append(")");
		} catch (final NullPointerException e) {
			builder.append("[Failed to retrieve some MD]");
		}
		return builder.toString();
	}

	/**
	 * Returns whether the object identified by the argument is public.
	 */
	protected static boolean isPublic(final ObjectType object) {
		final String availability = object.getGeneric().getGenerated()
				.getAvailability();
		return (availability != null && availability.contains("public"));
	}

	protected SearchClient getSearchClient(final boolean isPublic) {
		if (isPublic)
			return getRepository().getPublicSearchClient();
		else {
			if (privateSearchClient == null) {
				privateSearchClient = getRepository()
						.createPrivateSearchClient(getSid());
			}
			return privateSearchClient;
		}
	}

	/**
	 * Called by {@link #walkAggregation(ObjectType, boolean)} when the
	 * aggregation contains entries for which there is no metadata included.
	 *
	 * The default implementation does nothing, clients may override.
	 *
	 * @param textgridUri
	 *            the URI listed in the aggregation.
	 */
	protected void walkInaccessibleURI(final String textgridUri) {
	}

	public ITextGridRep getRepository() {
		return repository;
	}

	public void setRepository(final ITextGridRep repository) {
		this.repository = repository;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(final String sid) {
		this.sid = sid;
		this.privateSearchClient = null;
	}
}
