package info.textgrid.services.aggregator.tree;

import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Pid;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.TextgridUri;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.services.aggregator.ITextGridRep;
import info.textgrid.utils.export.aggregations.Aggregation;
import info.textgrid.utils.export.aggregations.AggregationEntry;

import java.util.Deque;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.ibm.icu.text.MessageFormat;

public class AggregationTreeFactory extends AggregationTreeWalker {

	private final Deque<Aggregation> stack = Lists.newLinkedList();
	private Aggregation root;
	private static Logger logger = Logger.getLogger("info.textgrid.utils.export.aggregations");

	protected AggregationTreeFactory(final ObjectType root, final ITextGridRep repository, final String sid) {
		this(root, repository, sid, true);
	}


	protected AggregationTreeFactory(final ObjectType root, final ITextGridRep repository, final String sid, boolean start) {
		super();
		this.setRepository(repository);
		this.setSid(sid);
//		this.root = new Aggregation(root);
//		stack.push(this.root);
		if (start)
			walkAggregation(root, false);
	}
	
	private static final Pattern INTERNAL_LINK = Pattern.compile("^(textgrid|hdl):([^.]*)(\\.(\\d+))?$");
	private AggregationEntry lastEntry;
	
	/**
	 * Returns the {@link AggregationEntry} that has been created last.
	 */
	protected AggregationEntry getLastEntry() {
		return lastEntry;
	}
	
	
	/**
	 * Returns <code>true</code> iff link is a link that probably refers to target.
	 */
	static boolean refersTo(final String link, final ObjectType target) {
		final Matcher matcher = INTERNAL_LINK.matcher(link);
		if (!matcher.matches()) {
			logger.log(Level.WARNING, "Internal link {0} doesn't match the pattern for internal links", link);
			return false;
		}
		final String scheme = matcher.group(1);
		if ("textgrid".equals(scheme)) {
			final String targetURI = target.getGeneric().getGenerated().getTextgridUri().getValue();
			if (link.equals(targetURI))
				return true;
			if (matcher.group(3) == null) { // generic URI
				final Matcher targetMatcher = INTERNAL_LINK.matcher(targetURI);
				return targetMatcher.matches() && targetMatcher.group(2).equals(matcher.group(2));
			}
		} else if ("hdl".equals(scheme)) {
			List<Pid> pids = target.getGeneric().getGenerated().getPid();
			return Iterables.any(pids, new Predicate<Pid>() {

				@Override
				public boolean apply(Pid input) {
					return link.equals(input.getValue());
				}
			});
		}
		return false;
	}

	@Override
	protected boolean walk(final ObjectType object, final boolean again) {
		if (super.walk(object, again))
			return true; // Aggregation already handled via #walkAggregation
		else {
			Aggregation parent = stack.peek();
			lastEntry = new AggregationEntry(object, parent);
			if (parent != null && parent.getMetadata().getEdition() != null && 
					refersTo(parent.getMetadata().getEdition().getIsEditionOf(), object))
				parent.setWork(lastEntry);
			return true;
		}
	}

	@Override
	protected void walkAggregation(final ObjectType aggregation,
			final boolean again) {
		stack.push(new Aggregation(aggregation, stack.peek()));
		super.walkAggregation(aggregation, again);

		if (aggregation.getEdition() != null
				&& aggregation.getEdition().getIsEditionOf() != null) {
			final String workURI = aggregation.getEdition().getIsEditionOf();
			if (!seen.contains(workURI)) {
				try {
					final ObjectType workObject = getRepository()
							.getCRUDService()
							.readMetadata(getSid(), "", workURI).getObject();
					walk(workObject, again);
				} catch (final MetadataParseFault e) {
					logger.log(
							Level.WARNING,
							MessageFormat
									.format("Failed to retrieve work metadata for URI {0}, edition {1}",
											workURI, stack.peek()), e);
					walkInaccessibleURI(workURI);
				} catch (final ObjectNotFoundFault e) {
					logger.log(
							Level.WARNING,
							MessageFormat
									.format("Failed to retrieve work metadata for URI {0}, edition {1}",
											workURI, stack.peek()), e);
					walkInaccessibleURI(workURI);
				} catch (final IoFault e) {
					logger.log(
							Level.WARNING,
							MessageFormat
									.format("Failed to retrieve work metadata for URI {0}, edition {1}",
											workURI, stack.peek()), e);
					walkInaccessibleURI(workURI);
				} catch (final AuthFault e) {
					logger.log(
							Level.WARNING,
							MessageFormat
									.format("Failed to retrieve work metadata for URI {0}, edition {1}",
											workURI, stack.peek()), e);
					walkInaccessibleURI(workURI);
				}
			}
		}

		this.root = stack.pop();
		this.lastEntry = this.root;
	}

	protected Aggregation getRoot() {
		return root;
	}
	
	public static Aggregation create(final ObjectType root, final ITextGridRep repository, final String sid) {
		return new AggregationTreeFactory(root, repository, sid, true).getRoot();
	}

}
