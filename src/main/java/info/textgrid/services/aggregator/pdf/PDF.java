package info.textgrid.services.aggregator.pdf;

import info.textgrid._import.RewriteMethod;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.services.aggregator.AbstractExporter;
import info.textgrid.services.aggregator.GenericExceptionMapper;
import info.textgrid.services.aggregator.ITextGridRep;
import info.textgrid.services.aggregator.ITextGridRep.TGOSupplier;
import info.textgrid.services.aggregator.util.StylesheetManager;
import info.textgrid.services.aggregator.util.XSLTErrorListener;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter;
import info.textgrid.utils.linkrewriter.ImportMapping;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XsltTransformer;

import org.apache.commons.io.FileUtils;
import org.apache.cxf.jaxrs.model.wadl.Description;

import com.google.common.io.FileBackedOutputStream;
import com.google.common.io.Files;

@Path("/pdf")
@Description("Generates a (default) PDF representation of a TEI document. " +
		"This is currently highly experimental, and it requires LaTeX and " +
		"the TEI stylesheets to be installed on the machine onto which " +
		"this service is deployed.")
public class PDF extends AbstractExporter {

	private static final URI TO_PDF_XSL = URI.create("/tei-stylesheets/latex/latex.xsl");

	final Logger logger = Logger
			.getLogger("info.textgrid.services.aggregator.pdf");

	@Context
	private ServletContext servlet;

	private StylesheetManager stylesheetManager;

	private File workingDir;

	public PDF(final ITextGridRep repository, final StylesheetManager manager, final Request request, final URI uri) {
		super(repository, request, uri.toString());
		this.stylesheetManager = manager;

		this.setFileExtension("pdf");
		this.setMediaType("application/pdf");
	}

	@Override
	public ResponseBuilder createResponse() throws MetadataParseFault, ObjectNotFoundFault, IoFault, AuthFault, ProtocolNotImplementedFault, IOException, SaxonApiException {
		super.createResponse();
		if (isNotModified())
			return getResponseBuilder();

		workingDir = Files.createTempDir();
		try {

			// First step: Rewrite with a non-initalized mapping to a temporary
			// buffer
			// to collect the undefined references
			final ImportMapping mapping = new ImportMapping();
			final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(
					mapping, true);
			rewriter.configure(URI.create("internal:tei#tei"));
			rewriter.recordMissingReferences();
			final TGOSupplier<InputStream> textSupplier = getContent();
			final String rootObjectFormat = textSupplier.getMetadata().getGeneric().getProvided().getFormat();
			if (!rootObjectFormat.startsWith("text/xml"))
				return GenericExceptionMapper.toResponse(Status.UNSUPPORTED_MEDIA_TYPE, "PDF generation currently supports only text/xml documents. The requested documen has an invalid document type, however:", rootObjectFormat);

			final FileBackedOutputStream buffer = new FileBackedOutputStream(
					1024 * 1024);
			rewriter.rewrite(textSupplier.getInput(), buffer);


			// now download the missing references if applicable, add
			// them to the mapping
			final Set<String> linkedURIs = rewriter.getMissingReferences();
			for (final String missingReference : linkedURIs) {
				final URI missingURI = URI.create(missingReference);
				if (missingURI.isAbsolute()
						&& (missingURI.getScheme().equals("textgrid") || missingURI
								.getScheme().equals("hdl"))
								&& mapping
								.getImportObjectForTextGridURI(missingReference) == null) {
					final TGOSupplier<InputStream> supplier = repository.read(
							missingURI, getSid().orNull());
					final String format = supplier.getMetadata().getGeneric()
							.getProvided().getFormat();
					String fileName = null;
					if (format.equals("image/jpeg")) {
						fileName = missingURI.getSchemeSpecificPart()
						.replace('.', '-').concat(".jpg");
					} else if (format.equals("image/png")) {
						fileName = missingURI.getSchemeSpecificPart()
						.replace('.', '-').concat(".png");
					} else if (format.equals("image/tiff")) {
						fileName = missingURI.getSchemeSpecificPart()
						.replace('.', '-').concat(".tif");
					} else if (format.equals("application/pdf")) {
						fileName = missingURI.getSchemeSpecificPart()
						.replace('.', '-').concat(".pdf");
					}
					if (fileName != null) {
						mapping.add(missingReference, fileName, null,
								RewriteMethod.NONE);
						Files.copy(supplier, new File(workingDir, fileName));
					}
				}
			}

			// finally, rewrite again, this time using the mapping
			final File teiFile = new File(workingDir, "data.xml");
			final FileOutputStream teiStream = new FileOutputStream(teiFile);
			rewriter.rewrite(buffer.getSupplier().getInput(), teiStream);
			teiStream.close();
			buffer.close();

			// now generate the TeX
			final XsltTransformer transformer = stylesheetManager.getStylesheet(TO_PDF_XSL, getSid(), false, true).load();
			final File tex = new File(workingDir, "data.tex");
			transformer.setDestination(stylesheetManager.xsltProcessor.newSerializer(tex));
			transformer.setSource(new StreamSource(teiFile));
			transformer.setErrorListener(new XSLTErrorListener(logger));
			transformer.transform();

			final File pdfFile = new File(workingDir, "data.pdf");

			final Process latexProcess = Runtime
					.getRuntime()
					.exec("pdflatex -no-shell-escape -interaction batchmode data.tex",
							null, workingDir);
			if (latexProcess.waitFor() != 0 && !pdfFile.exists()) {
				final File logFile = new File(workingDir, "data.log");
				if (logFile.canRead())
					return GenericExceptionMapper.toResponse(Status.INTERNAL_SERVER_ERROR, "The requested document was transformed to a file LaTeX failed to deal with. Below is the LaTeX log file.",
							Files.toString(logFile, Charset.forName("UTF-8")));
				else
					throw new IllegalStateException("LaTeX process failed, no log file found.");
			}
			return getResponseBuilder();

		} catch (final IOException e) {
			throw new WebApplicationException(e);
		} catch (final SaxonApiException e1) {
			throw new WebApplicationException(e1);
		} catch (final InterruptedException e1) {
			throw new WebApplicationException(e1);
		} catch (final XMLStreamException e1) {
			throw new WebApplicationException(e1);
		}
	}


	@Override
	public void write(final OutputStream output) throws IOException {
		try {
			Files.copy(new File(workingDir, "data.pdf"), output);
		} finally {
			FileUtils.deleteDirectory(workingDir);
			workingDir = null;
		}
	}

	@Override
	protected void finalize() throws Throwable {
		if (workingDir != null && workingDir.exists())
			FileUtils.deleteDirectory(workingDir);
		super.finalize();
	}



}
