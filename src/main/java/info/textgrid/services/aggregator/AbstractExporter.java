package info.textgrid.services.aggregator;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.XMLGregorianCalendar;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.services.aggregator.ITextGridRep.TGOSupplier;
import info.textgrid.utils.export.filenames.DefaultFilenamePolicy;
import net.sf.saxon.s9api.SaxonApiException;

/**
 * The abstract base class for all aggregators/exporters. This tries to capture
 * all behavior common to the various format specific exporters, and it tries to
 * provide sensible defaults so code complexity for the implementers is reduced.
 *
 * <p>These are the typical steps:</p>
 * <ol>
 * <li>The exporter is instantiated using one of the constructors and configured
 * for the specific request.</li>
 * <li>The response (except for the content part) is built using
 * {@link #createResponse()}.
 * <p>
 * This step should include the required steps to decide on the response type
 * (success/error …), but it should return as fast as possible since the
 * client/user will not get any feedback before.
 * </p>
 * </li>
 * <li>The actual content should be generated and returned by the
 * {@link #write(java.io.OutputStream)} method, which is called on-demand when
 * the client tries to read the body part of the response.</li>
 * </ol>
 *
 * Implementors will typically call at least the following configuration methods:
 * <ul>
 * <li>{@link #setMediaType(String)} to set the response's content type
 * <li>{@link #setFileExtension(String)} to set the response's suggested filename extension
 * </ul>
 * Implementors will typically implement / extend the following methods:
 * <ul>
 * <li>implement {@link #write(java.io.OutputStream)} to generate the contents
 * <li>extend {@link #createResponse()} to setup the response any further
 * </ul>
 *
 *
 * @author vitt
 */
public abstract class AbstractExporter implements StreamingOutput {

	private static final Logger logger = Logger
			.getLogger(AbstractExporter.class.getCanonicalName());
	private Optional<String> sid = Optional.absent();
	private boolean sandbox = false;

	protected Optional<String> getSid() {
		return sid;
	}
	
	public boolean getSandbox() { 
		return sandbox; 
	}
	
	public void sandbox(boolean sandbox) {
		this.sandbox = sandbox;
	}

	public SourceType getSourceType() {
		return sourceType;
	}

	public URI[] getRootURIs() {
		return rootURIs;
	}

	// detected and extracted
	public enum SourceType {
		UNKNOWN, XML, AGGREGATION, BASKET
	}

	protected enum Disposition {
		INLINE, ATTACH;

		@Override
		public String toString() {
			return super.toString().toLowerCase(Locale.ENGLISH);
		}
	}

	protected SourceType sourceType = SourceType.UNKNOWN;
	protected final ITextGridRep repository;
	protected final Request request;
	protected final Stopwatch stopwatch;
	private TGOSupplier<InputStream> content;
	
	@Deprecated
	protected URI[] rootURIs;
	private Iterable<ObjectType> rootObjects;
	private Date lastModified;
	private Boolean notModified = null;
	private ResponseBuilder responseBuilder;
	private String title;
	private String fileExtension;
	private String mediaType = MediaType.APPLICATION_OCTET_STREAM;

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(final String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public Disposition getDisposition() {
		return disposition;
	}

	public void setDisposition(final Disposition disposition) {
		this.disposition = disposition;
	}

	private Disposition disposition = Disposition.ATTACH;

	public AbstractExporter(final ITextGridRep repository,
			final Request request, final String uriList) {
		Preconditions.checkArgument(repository != null, "non-null repository argument required");

		stopwatch = Stopwatch.createStarted();
		this.repository = repository;
		this.request = request;
		this.rootURIs = extractURIs(uriList);
		if (rootURIs.length > 1)
			this.sourceType = SourceType.BASKET;
	}

	private static URI[] extractURIs(final String uriList) {
		final String[] uriStrings = uriList.split(",");
		Preconditions.checkArgument(uriStrings.length > 0, "No URI found in %s", uriList);
		final URI[] uris = new URI[uriStrings.length];
		for (int i = 0; i < uriStrings.length; i++) {
			uris[i] = URI.create(uriStrings[i]);
		}
		return uris;
	}
	
	protected void initFromSearch(final String query, final String target, int start, int stop, List<String> filter) {
		rootObjects = SearchResultList.create(repository.getPublicSearchClient(), query, target, filter, getSandbox()).start(start).stop(stop);
	}

	/**
	 * Returns an array with the metadata records of all root objects.
	 */
	protected Iterable<ObjectType> getRootObjects() throws MetadataParseFault,
			ObjectNotFoundFault, IoFault, AuthFault {
		if (rootObjects == null) {
			if (sourceType == SourceType.BASKET) {
				final Builder<ObjectType> builder = ImmutableList.builder();
				for (int i = 0; i < rootURIs.length; i++) {
					final MetadataContainerType container = repository.getCRUDService().readMetadata(sid.orNull(), null, rootURIs[i].toString());
					builder.add(container.getObject());
				}
				rootObjects = builder.build();
				logger.log(Level.INFO, MessageFormat.format(
						"Collected root objects for basket {0} after {1}",
						this, stopwatch.toString()));
			} else
				rootObjects = ImmutableList.of(getContentSimple().getMetadata());
		}
		return rootObjects;
	}

	/**
	 * Returns a single, possibly virtual object representing the exporter's
	 * root input. {@link AbstractExporter}'s implementation delegates to
	 * {@link #getContentBasket()} or {@link #getContentSimple()}, depending on
	 * the request type.
	 *
	 * @throws IllegalStateException
	 *             if not supported.
	 */
	protected TGOSupplier<InputStream> getContent() {
		if (sourceType == SourceType.BASKET) {
			return getContentBasket();
		} else
			return getContentSimple();
	}

	/**
	 * Constructs a single virtual object representing the root objects. Clients
	 * who need this functionality must override.
	 *
	 * @throws IllegalStateException
	 *             if not supported.
	 */
	protected TGOSupplier<InputStream> getContentBasket() {
		throw new IllegalStateException(
				"This exporter does not support retrieving the basket content as single object.");
	}

	/**
	 * Returns a {@link TGOSupplier} with content and metadata in case only one
	 * object has been requested.
	 *
	 * This method may cause a TG-crud READ request that is left open after the
	 * metadata part until content data is requested from the supplier's input
	 * stream.
	 *
	 * @throws IllegalStateException
	 *             when more than one root object is requested
	 */
	protected TGOSupplier<InputStream> getContentSimple() {
		Preconditions
				.checkState(
						rootURIs.length == 1,
						"This method is not available for basket requests (requested %s)",
						this);
		if (content == null) {
			content = repository.read(rootURIs[0], sid.orNull());
			logger.info(MessageFormat.format(
					"Fetched source for {0} up to metadata after {1}",
					this, stopwatch));
		}
		return content;
	}

	/**
	 * Sets the session Id
	 */
	public AbstractExporter sid(final String sid) {
		if (sid == null || sid.isEmpty()) {
			this.sid = Optional.absent();
		} else {
			this.sid = Optional.of(sid);
		}
		return this;
	}

	/**
	 * Returns a last modified date suitable for use in HTTP requests.
	 *
	 * Implementers may override or extend.
	 */
	protected Date getLastModified() throws MetadataParseFault,
			ObjectNotFoundFault, IoFault, AuthFault {
		if (lastModified == null) {
			if (getRootObjects() instanceof SearchResultList)
				return new Date();
			XMLGregorianCalendar youngest = null;
			for (final ObjectType object : getRootObjects()) {
				final XMLGregorianCalendar current = object.getGeneric().getGenerated().getLastModified();
				if (youngest == null || current.compare(youngest) == DatatypeConstants.GREATER)
					youngest = current;
			}
			youngest.setMillisecond(0);
			this.lastModified = youngest.toGregorianCalendar().getTime();
		}
		return lastModified;
	}

	/**
	 * Checks whether we can stop exporting and return 304 Not Modified instead.
	 *
	 * The {@link AbstractExporter} implementation simply checks the results of
	 * {@link #getLastModified()}.
	 *
	 * @return a configured {@link ResponseBuilder} when the content is not
	 *         modified, <code>null</code> when the request should be fulfilled
	 *         normally.
	 */
	protected final ResponseBuilder evaluatePreconditions()
			throws MetadataParseFault, ObjectNotFoundFault, IoFault, AuthFault {

		final ResponseBuilder builder = request == null ? null
				: doEvaluatePreconditions();
		if (builder == null) {
			this.responseBuilder = Response.ok();
			this.notModified = false;
		} else {
			this.responseBuilder = builder;
			this.notModified = true;
		}
		return builder;
	}

	/**
	 * The {@link AbstractExporter} implementation simply checks the results of
	 * {@link #getLastModified()}.
	 */
	protected ResponseBuilder doEvaluatePreconditions()
			throws MetadataParseFault, ObjectNotFoundFault, IoFault, AuthFault {
		return request.evaluatePreconditions(getLastModified());
	}

	/**
	 * Returns <code>true</code> when we know from the request that we can skip
	 * generating a real response and can just return
	 * <code>304 Not Modified</code> instead, allowing the client to serve the
	 * request from its local cache.
	 *
	 * <p>
	 * This runs {@link #evaluatePreconditions()}, if necessary. Clients who
	 * wish to influence this behaviour should override
	 * {@link #doEvaluatePreconditions()}.
	 * </p>
	 *
	 * @see #evaluatePreconditions()
	 * @see {@link #doEvaluatePreconditions()}
	 */
	protected final boolean isNotModified() throws MetadataParseFault,
			ObjectNotFoundFault, IoFault, AuthFault {
		if (notModified == null)
			evaluatePreconditions();
		return notModified;
	}

	/**
	 * Returns the current state of the exporter's internal
	 * {@link ResponseBuilder}. If none exists yet,
	 * {@link #evaluatePreconditions()} is called and a minimal response builder
	 * is constructed.
	 */
	protected ResponseBuilder getResponseBuilder() throws MetadataParseFault,
			ObjectNotFoundFault, IoFault, AuthFault {
		if (responseBuilder == null)
			evaluatePreconditions();
		return responseBuilder;
	}

	/**
	 * Configures caching parameters into the current response builder.
	 *
	 * @see #getResponseBuilder()
	 * @see #createResponse()
	 */
	protected void configureCache() throws MetadataParseFault,
			ObjectNotFoundFault, IoFault, AuthFault {
		final ResponseBuilder builder = getResponseBuilder();
				builder.lastModified(getLastModified());
				final CacheControl cacheControl = new CacheControl();
				cacheControl.setPrivate(sid.isPresent());
				cacheControl.setMustRevalidate(sid.isPresent());
				cacheControl.setMaxAge(86400);	// one day
				builder.cacheControl(cacheControl);
	}

	/**
	 * Configures the content-disposition header, which contains the
	 * content-disposition and a suggested filename.
	 *
	 * @see #setDisposition(Disposition)
	 * @see #setTitle(String)
	 * @see #setFileExtension(String)
	 */
	protected void configureFilename() throws MetadataParseFault,
			ObjectNotFoundFault, IoFault, AuthFault {

		getResponseBuilder().header(
				"Content-Disposition",
				getDisposition()
						+ ";filename=\""
						+ calculateFilename() + "\"");
	}


	private String calculateFilename() throws MetadataParseFault, ObjectNotFoundFault, IoFault, AuthFault {
		final String title = calculateTitle();
		final StringBuilder result = new StringBuilder(title.length() + 30);
		result.append(DefaultFilenamePolicy.INSTANCE.translate(title));
		Iterator<ObjectType> roots = getRootObjects().iterator();
		if (roots.hasNext()) {
			ObjectType first = roots.next();
			if (!roots.hasNext())
				result.append('.').append(URI.create(first.getGeneric().getGenerated().getTextgridUri().getValue()).getSchemeSpecificPart());
		}
		if (fileExtension != null && !fileExtension.isEmpty())
			result.append('.').append(fileExtension);
		return result.toString();
	}

	private String calculateTitle() throws MetadataParseFault, ObjectNotFoundFault, IoFault, AuthFault {
		final Iterator<ObjectType> rootObjects = getRootObjects().iterator();
		if (title != null && !title.isEmpty())
			return title;
		else if (!rootObjects.hasNext())
			return "(nothing)";
		else {
			title = rootObjects.next().getGeneric().getProvided().getTitle().get(0);
			if (rootObjects.hasNext())
				title = title + " etc";
			return title;
		}
	}

	/**
	 * Creates a response builder that is fully configured and ready to build a
	 * response and return it.
	 */
	public ResponseBuilder createResponse() throws MetadataParseFault,
			ObjectNotFoundFault, IoFault, AuthFault,
			ProtocolNotImplementedFault, IOException, SaxonApiException {
		if (isNotModified()) {
			return getResponseBuilder();
		} else {
			configureFilename();
			configureCache();
			getResponseBuilder().type(getMediaType()).entity(this);
			return getResponseBuilder();
		}
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(final String mediaType) {
		this.mediaType = mediaType;
	}

	@Override
	protected void finalize() throws Throwable {
		if (stopwatch.isRunning())
			stopwatch.stop();
		super.finalize();
	}

	@Override
	public String toString() {
		return MessageFormat.format("{0} ({1} -> {2})", Joiner.on(", ").join(rootURIs), sourceType, fileExtension);
	}



}