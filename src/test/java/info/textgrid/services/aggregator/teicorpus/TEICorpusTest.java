package info.textgrid.services.aggregator.teicorpus;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assume.*;
import static org.junit.Assert.*;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.services.aggregator.ITextGridRep;
import info.textgrid.services.aggregator.util.TextGridRepProvider;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import net.sf.saxon.s9api.SaxonApiException;

import org.apache.cxf.helpers.IOUtils;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

public class TEICorpusTest {

	private ITextGridRep repository;

	@Before
	public void setUp() throws Exception {
		repository = TextGridRepProvider.getInstance();
	}

	@Test(expected = WebApplicationException.class)
	public void testGet404() throws URISyntaxException, ObjectNotFoundFault, MetadataParseFault, IoFault, AuthFault, ProtocolNotImplementedFault, IOException, SaxonApiException {
		final Response response = new TEICorpusExporter(repository, null, "textgrid:doesnotexist").createResponse().build();
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testGet() throws URISyntaxException, WebApplicationException,
	IOException, ObjectNotFoundFault, MetadataParseFault, IoFault, AuthFault, ProtocolNotImplementedFault, SaxonApiException {
		final Response response = new TEICorpusExporter(repository, null, "textgrid:jmzg.0").createResponse().build();
		final Object entity = response.getEntity();
		final ByteArrayOutputStream output = new ByteArrayOutputStream();
		((StreamingOutput) entity).write(output);
		final InputStream inputStream = getClass().getClassLoader()
				.getResourceAsStream("gedichte-anakreons-corpus.xml");
		final String expected = IOUtils.toString(inputStream, "UTF-8");
		Assert.assertEquals(expected, output.toString("UTF-8"));
	}
	
	@Test
	public void testProtected() throws MetadataParseFault, ObjectNotFoundFault, IoFault, AuthFault, ProtocolNotImplementedFault, IOException, SaxonApiException {
		final String sid = System.getProperty("textgrid.sid", "");
		assumeThat("Define the system property textgrid.sid to run the sid based tests.", sid, not(""));
		final TEICorpusExporter exporter = new TEICorpusExporter(repository, null, "textgrid:h4kd.0");
		exporter.sid(sid);
		Response response = exporter.createResponse().build();
		assertThat("SID based corpus export failed", response.getStatus(), is(200));
	}
	

}
