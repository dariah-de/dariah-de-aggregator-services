package info.textgrid.services.aggregator.teicorpus;

import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.services.aggregator.ITextGridRep;
import info.textgrid.services.aggregator.util.TextGridRepProvider;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class TEIHeaderStackTest {

	private final ITextGridRep repository = TextGridRepProvider.getInstance();
	private TEIHeaderStack stack;

	@Test
	public void testTEIHeaderStack() throws ObjectNotFoundFault,
			MetadataParseFault, IoFault, AuthFault {
		Assert.assertNotNull(stack);
	}

	@Before
	public void extracted() throws ObjectNotFoundFault, MetadataParseFault,
			IoFault, AuthFault {
		final ObjectType rootObject = repository.getCRUDService()
				.readMetadata("", "", "textgrid:jfsn.0").getObject();
		stack = new TEIHeaderStack(repository, rootObject, repository.getPublicSearchClient());
	}

	@Test
	public void testGenerateCommonDOM() throws ParserConfigurationException,
			JAXBException {
		final Document document = stack.generateCommonDOM();
		Assert.assertNotNull(document);
		final NodeList childNodes = document.getDocumentElement().getChildNodes();
		Assert.assertTrue("No child nodes :-(", childNodes.getLength() > 0);
	}

	@Test
	public void testLoadStylesheet() throws TransformerConfigurationException {
		final Templates stylesheet = stack.getStylesheet();
		Assert.assertNotNull(stylesheet);
		final Transformer transformer = stylesheet.newTransformer();
		Assert.assertNotNull(transformer);
	}



}
