package info.textgrid.services.aggregator;

import org.apache.commons.io.IOUtils;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.HttpURLConnection;

import static org.junit.Assert.*;
import static org.junit.Assume.*;
import static org.hamcrest.CoreMatchers.*;


public class PlainTextIT extends AbstractIntegrationTest {

    @Test
    public void testText() throws IOException {
        final HttpURLConnection connection = createRequest("/text/textgrid:qdnf.0");
        connection.connect();
        assertThat(connection.getResponseCode(), is(200));
        assertThat(connection.getContentType(), is("text/plain;charset=UTF-8"));
        assertThat(IOUtils.toString(connection.getInputStream()),
                CoreMatchers.<Object>is("Den Pessimisten\n" +
                "Ghasel\n" +
                "Solang uns Liebe lockt mit Lust und Plagen,\n" +
                "Solang Begeistrung wechselt und Verzagen,\n" +
                "Solange wird auf Erden nicht die Zeit,\n" +
                "Die schreckliche, die dichterlose tagen:\n" +
                "Solang in tausend Formen Schönheit blüht,\n" +
                "Schlägt auch ein Herz, zu singen und zu sagen,\n" +
                "Solang das Leid, das ewge, uns umflicht,\n" +
                "Solange werden wirs in Tönen klagen,\n" +
                "Und es erlischt erst dann der letzte Traum,\n" +
                "Wenn er das letzte Herz zu Gott getragen!\n"));
    }
}
