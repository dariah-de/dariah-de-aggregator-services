package info.textgrid.services.aggregator;

import static org.junit.Assert.*;
import info.textgrid.services.aggregator.util.StylesheetManager;
import info.textgrid.services.aggregator.util.TextGridRepProvider;

import java.io.IOException;
import java.net.URI;

import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XsltExecutable;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.base.Optional;

public class StylesheetManagerTest {

	private static URI INTERNAL_XSL = URI.create("/tei-stylesheets/html/html.xsl");
	private StylesheetManager manager;

	@Before
	public void setUp() {
		manager = new StylesheetManager(null, TextGridRepProvider.getInstance());
	}

	@Ignore("Can't really test this now since the war isn't setup properly")
	@Test
	public void testGetInternalStylesheet() throws SaxonApiException, IOException {
		testGetStylesheet(INTERNAL_XSL);
	}

	@Ignore("Need a stable available XSLT")
	@Test
	public void testGetHttpStylesheet() throws SaxonApiException, IOException {
		final URI teiStylesheet = URI.create("http://www.tei-c.org/Vault/P5/2.5.0/xml/tei/stylesheet/html/html.xsl");
		testGetStylesheet(teiStylesheet);
	}

	private void testGetStylesheet(final URI xslURI) throws SaxonApiException, IOException {
		final long requestCount = manager.stats().requestCount();
		final XsltExecutable executable = manager.getStylesheet(xslURI, Optional.<String>absent(), false, true);
		assertNotNull("StylesheetManager failed to return a stylesheet",  executable);
		assertTrue(manager.stats().requestCount() == requestCount+1);
		final long hitCount = manager.stats().hitCount();
		final XsltExecutable executable2 = manager.getStylesheet(xslURI, Optional.<String>absent(), false, true);
		assertTrue(executable == executable2);
		assertTrue(manager.stats().hitCount() == hitCount+1);
		System.out.println(manager.stats());
	}

}
