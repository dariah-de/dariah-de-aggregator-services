package info.textgrid.services.aggregator;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import com.google.common.io.ByteStreams;

public abstract class AbstractIntegrationTest {

	/**
	 * Creates a request in the form of a (not yet connected)
	 * {@link URLConnection}.
	 * 
	 * @param request
	 *            Path and query string (everything after the
	 *            <code>aggregator/</code>
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	protected HttpURLConnection createRequest(final String request)
			throws MalformedURLException,
			IOException {
				final URL url = new URL(System.getProperty("service.url") + request);
				final URLConnection connection = url.openConnection();
				connection.setConnectTimeout(360000);
		return (HttpURLConnection) connection;
			}

	/**
	 * Reads the given connection's content (and throws it away).
	 * 
	 * @param connection
	 *            must be open.
	 * @return number of bytes read
	 * @throws IOException
	 */
	protected long readContents(final URLConnection connection) throws IOException {
		final InputStream content = connection.getInputStream();
		final long realLength = ByteStreams.copy(content,
				ByteStreams.nullOutputStream());
		return realLength;
	}

	protected String getSid() {
		return System.getProperty("textgrid.sid", "");
	}


}