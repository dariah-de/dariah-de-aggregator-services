package info.textgrid.services.aggregator;

import static org.junit.Assert.*;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.middleware.tgsearch.client.SearchClient;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.services.aggregator.util.TextGridRepProvider;

import org.junit.Test;
import org.springframework.stereotype.Service;

@Service
public class TextGridRepTest {

	private final ITextGridRep repository = TextGridRepProvider.getInstance();

	ITextGridRep getTeiCorpus() {
		return repository;
	}

	@Test
	public void testAutoWire() {
		assertNotNull(repository);
	}

	@Test
	public void testGetConfValue() {
		final String confValue = getTeiCorpus().getConfValue(ConfservClientConstants.TG_SEARCH_PUBLIC);
		assertNotNull("Configuration value is null.", confValue);
		// assertEquals("https://textgridlab.org/1.0/tgsearch-public", confValue);
	}

	@Test
	public void testGetCRUDService() {
		final TGCrudService crudService = getTeiCorpus().getCRUDService();
		assertNotNull("TG-crud service is null!?", crudService);
	}

	@Test
	public void testGetSearchClient() {
		final SearchClient searchClient = getTeiCorpus().getPublicSearchClient();
		assertNotNull("TG-search service is null!?", searchClient);
	}

	@Test
	public void testConfigOverride() {
		final String overrideValue = "http://textgrid-esx1.gwdg.de/1.0/tgcrud/TGCrudService";
		System.getProperties().setProperty(TextGridRepProvider.PROPERTY_PREFIX + "tgcrud", overrideValue);
		final TextGridRepProvider repository = new TextGridRepProvider("https://textgridlab.org/1.0/tgcrud/TGCrudService");
		assertEquals(overrideValue, repository.getConfValue("tgcrud"));
		System.getProperties().remove(TextGridRepProvider.PROPERTY_PREFIX + "tgcrud");
	}

}
