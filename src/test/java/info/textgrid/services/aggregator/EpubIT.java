package info.textgrid.services.aggregator;


import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.output.TeeOutputStream;
import org.apache.cxf.transport.http.HTTPException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.adobe.epubcheck.api.EpubCheck;
import com.adobe.epubcheck.util.WriterReportImpl;
import com.google.common.io.ByteStreams;

public class EpubIT extends AbstractIntegrationTest {

	private Path ebookDir;
	private boolean clearEbookDir = false;

	@Before
	public void setupEpubDir() throws IOException {
		final Properties properties = System.getProperties();
		if (properties.containsKey("basedir")) {
			ebookDir = Paths.get(properties.getProperty("basedir"), "target", "E-Books");
			ebookDir.toFile().mkdirs();
		} else {
			ebookDir = Files.createTempDirectory("E-Books");
			clearEbookDir = true;
		}
	}

	@After
	public void clearEpubDir() throws IOException {
		if (clearEbookDir && ebookDir != null)
			FileUtils.deleteDirectory(ebookDir.toFile());
	}


	private File downloadEBook(String requestString, String targetFileName) throws MalformedURLException, IOException,
			FileNotFoundException {
		final HttpURLConnection request = createRequest(requestString);
		final File epub = ebookDir.resolve(targetFileName).toFile();
		final FileOutputStream epubStream = new FileOutputStream(epub);
		ByteStreams.copy(request.getInputStream(), epubStream);
		epubStream.close();
		if (request.getResponseCode() != 200)
			throw new HTTPException(request.getResponseCode(), request.getResponseMessage(), request.getURL());

		System.out.println("Downloaded E-Book to: " + epub);
		System.out.println("[[ATTACHMENT|" + epub.getAbsolutePath() + "]]");
		return epub;
	}

	private void checkEBook(String requestString, String targetFileName) throws MalformedURLException, FileNotFoundException, IOException {
		final File ebook = downloadEBook(requestString, targetFileName);
		final String reportFileName = FilenameUtils.removeExtension(ebook.getPath()).concat(".txt");
		final FileOutputStream reportStream = new FileOutputStream(reportFileName);
		final PrintWriter writer = new PrintWriter(new TeeOutputStream(System.err, reportStream));
		try {
			final WriterReportImpl report = new WriterReportImpl(writer);
			final EpubCheck epubCheck = new EpubCheck(ebook, report);
			epubCheck.validate();
			assertTrue(MessageFormat.format("{0} fatal errors in {1}", report.getFatalErrorCount(), targetFileName), report.getFatalErrorCount() <= 0);
			assertTrue(MessageFormat.format("{0} non-fatal errors in {1}", report.getErrorCount(), targetFileName), report.getErrorCount() <= 0);
		} finally {
			writer.close();
			reportStream.close();
		}
	}

	@Test
	public void testGet() throws MalformedURLException, IOException {
		final URLConnection connection = createRequest("/epub/textgrid:jfst.0");
		connection.connect();
		Assert.assertEquals("application/epub+zip", connection.getContentType());
		final long realLength = readContents(connection);
		Assert.assertTrue("Content empty", realLength > 0);
	}

	@Test
	public void testValidEPUBSingle() throws MalformedURLException, IOException {
		checkEBook("/epub/textgrid:k2kp.0", "Goethes_Briefwechsel_mit_einem_Kinde.k2kp.0.epub");
	}

	@Test // #11497
	@Ignore("#11497 is low priority: the full text is contained, there is just an empty reference.")
	public void testIssue11497() throws MalformedURLException, FileNotFoundException, IOException {
		checkEBook("/epub/textgrid:jkhx.0", "Der_gefesselte_Prometheus.jkhx.0.epub");
	}

	@Test
	public void testValidEPUBCorpus() throws MalformedURLException, FileNotFoundException, IOException {
		checkEBook("/epub/textgrid:n92g.0", "George_Dante_Goettliche_Komoedie.nbcd.0.epub");
	}

	@Test // #29421
	@Ignore("#29421 needs to be fixed, but isn’t release critical")
	public void testValidEPUB29421() throws MalformedURLException, FileNotFoundException, IOException {
		checkEBook("/epub/textgrid:k76f.0", "Die_drei_Erznarren.k76f.0.epub");
	}
}
