package info.textgrid.services.aggregator.tree;

import static org.junit.Assert.*;

import java.io.InputStream;

import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;

import javax.xml.bind.JAXB;

import org.junit.Before;
import org.junit.Test;

public class AggregationTreeFactoryTest {

	private ObjectType object;
	
	@Before
	public void setup() {
		InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream("ksd2.0.meta");
		MetadataContainerType container = JAXB.unmarshal(resourceAsStream, MetadataContainerType.class);
		object = container.getObject();
	}

	@Test
	public void testHdlMatches() {
		assertTrue("hdl:11858/00-1734-0000-0002-4579-D doesn't match", AggregationTreeFactory.refersTo("hdl:11858/00-1734-0000-0002-4579-D", object));
	}
	
	@Test public void testGenericLinkMatches() {
		assertTrue("textgrid:ksd2 doesn't match", AggregationTreeFactory.refersTo("textgrid:ksd2", object));
	}
	
	@Test public void testAbsoluteLinkMatches() {
		assertTrue("textgrid:ksd2.0 doesn't match", AggregationTreeFactory.refersTo("textgrid:ksd2.0", object));
	}

}
