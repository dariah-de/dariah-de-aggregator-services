package info.textgrid.services.aggregator;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.junit.Test;

import com.google.common.io.ByteStreams;

import static org.junit.Assert.*;
import static org.junit.Assume.*;
import static org.hamcrest.CoreMatchers.*;

public class TEICorpusIT extends AbstractIntegrationTest {
	
	@Test
	public void testCorpus() throws IOException {
		final HttpURLConnection connection = createRequest("/teicorpus/textgrid:jfst.0");
		connection.connect();
		assertThat(connection.getResponseCode(), is(200));
		assertThat(connection.getContentType(), is("application/tei+xml"));
		ByteStreams.copy(connection.getInputStream(), ByteStreams.nullOutputStream());
	}
	
	@Test
	public void testCorpusSid() throws IOException {
		assumeThat(getSid(), not(""));

		HttpURLConnection connection = createRequest("/teicorpus/textgrid:h4kd.0?sid=" + getSid());
		connection.connect();
		assertThat(connection.getResponseCode(), is(200));
		assertThat(connection.getContentType(), is("application/tei+xml"));
		ByteStreams.copy(connection.getInputStream(), ByteStreams.nullOutputStream());
	}
	
}
