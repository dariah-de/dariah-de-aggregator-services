package info.textgrid.services.aggregator;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.text.MessageFormat;

import org.junit.Test;

public class PdfIT extends AbstractIntegrationTest {

	@Test
	public void testGet() throws MalformedURLException, IOException {
		final HttpURLConnection connection = createRequest("/pdf/textgrid:jfss.0");
		connection.connect();
		if (connection.getResponseCode() != 200) {
			System.err
					.println(MessageFormat
							.format("PDF test failed: {0} {1}. ignoring due to external dependency",
									connection.getResponseCode(),
									connection.getResponseMessage()));
		}
		// Assert.assertEquals(200, connection.getResponseCode());
		// Assert.assertEquals("application/pdf", connection.getContentType());
		// Assert.assertTrue("Response should be non-null",
		// readContents(connection) > 0);
	}

}
