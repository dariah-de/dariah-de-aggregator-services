package info.textgrid.services.aggregator;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.junit.Test;

public class CacheControlIT extends AbstractIntegrationTest {

	private static SimpleDateFormat HTTP_DATE = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.US);
	
	@Test
	public void testHTMLSingle() throws MalformedURLException, IOException, ParseException {
		final HttpURLConnection connection2 = performTwoRequests("/html/textgrid:mjsx.0");
		assertEquals(304, connection2.getResponseCode());
		
	}

	@Test
	public void testHTMLBasket() throws MalformedURLException, IOException, ParseException {
		final HttpURLConnection connection2 = performTwoRequests("/html/textgrid:mjsx.0,textgrid:mjqh.0");
		assertEquals(304, connection2.getResponseCode());
	}
	
	@Test
	public void testZipBasket() throws MalformedURLException, IOException, ParseException {
		final HttpURLConnection connection2 = performTwoRequests("/zip/textgrid:mjsx.0,textgrid:mjqh.0");
		assertEquals(304, connection2.getResponseCode());
	}
	
	@Test
	public void testEPUBBasket() throws MalformedURLException, IOException, ParseException {
		final HttpURLConnection connection2 = performTwoRequests("/epub/textgrid:mjsx.0,textgrid:mjqh.0");
		assertEquals(304, connection2.getResponseCode());
	}
		
	@Test
	public void testCorpusBasket() throws MalformedURLException, IOException, ParseException {
		final HttpURLConnection connection2 = performTwoRequests("/epub/textgrid:mjsx.0,textgrid:mjqh.0");
		assertEquals(304, connection2.getResponseCode());
	}
		
		
		
	private HttpURLConnection performTwoRequests(String request)
			throws MalformedURLException, IOException, ParseException {
		final HttpURLConnection connection = createRequest(request);
		connection.connect();
		readContents(connection);
		final String lastModified = connection.getHeaderField("Last-Modified");
		final Date lastModifiedDate = HTTP_DATE.parse(lastModified);
		
		final HttpURLConnection connection2 = createRequest(request);
		connection2.setIfModifiedSince(lastModifiedDate.getTime());
		connection2.connect();
		System.out.println(connection2.getResponseCode() + " " + connection2.getResponseMessage());
		return connection2;
	}

}
