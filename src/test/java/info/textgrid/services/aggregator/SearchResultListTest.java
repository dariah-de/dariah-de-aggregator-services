package info.textgrid.services.aggregator;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.services.aggregator.util.TextGridRepProvider;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

public class SearchResultListTest {
	
	ITextGridRep repository = TextGridRepProvider.getInstance();

	@Test
	public void test() {
		SearchResultList results = SearchResultList
			.create(repository.getPublicSearchClient(), "work.genre:\"verse\" AND edition.agent.value:\"anakreon\"", "both", null, false)
			.chunkSize(10);
		int i = 0;
		for (ObjectType object : results) {
			System.out.println(object.getGeneric().getProvided().getTitle());
			i++;
		}
		assertThat(i, is(16));
	}
	
	

}
