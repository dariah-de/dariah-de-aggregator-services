package info.textgrid.services.aggregator.html;

import static org.junit.Assert.*;
import static org.junit.Assume.*;
import static org.hamcrest.CoreMatchers.*;
import info.textgrid.services.aggregator.AbstractIntegrationTest;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import org.junit.Test;

import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;


public class HtmlIT extends AbstractIntegrationTest {

	@Test
	public void testGet() throws IOException {
		final HttpURLConnection connection = createRequest("/html/textgrid:jfst.0");
		connection.connect();
		assertEquals(200, connection.getResponseCode());
		assertEquals("text/html", connection.getContentType());
		assertTrue("Content length > 0", readContents(connection) > 0);
	}
	
	@Test
	public void testBasket() throws IOException {
		HttpURLConnection connection = createRequest("/html/textgrid:mjsx.0,textgrid:mjqh.0");
		connection.connect();
		assertEquals(200, connection.getResponseCode());
		assertEquals("text/html", connection.getContentType());
		String disposition = connection.getHeaderField("Content-Disposition");
		assertEquals("inline;filename=\"Spaetes_Erwachen_etc.html\"", disposition);
		
		InputStreamReader reader = new InputStreamReader(connection.getInputStream(), "UTF-8");
		assertTrue("Input does not contain expected content", CharStreams.toString(reader).contains("Vom Baum ein Zweiglein nur gebrochen,"));
	}

	@Test
	public void testSid() throws IOException {
		assumeThat(getSid(), not(""));
        final HttpURLConnection connection = createRequest("/html/textgrid:h4kd.0?sid=" + getSid());
        connection.connect();
        assertThat(connection.getResponseCode(), is(200));
        assertThat(connection.getContentType(), is("text/html"));
        ByteStreams.copy(connection.getInputStream(), ByteStreams.nullOutputStream());
	}

}
