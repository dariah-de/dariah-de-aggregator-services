package info.textgrid.services.aggregator.html;

import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.services.aggregator.ITextGridRep;
import info.textgrid.services.aggregator.util.StylesheetManager;
import info.textgrid.services.aggregator.util.TextGridRepProvider;

import java.io.*;

import javax.ws.rs.WebApplicationException;

import net.sf.saxon.s9api.SaxonApiException;

import org.apache.commons.io.output.NullOutputStream;
import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLTestCase;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.common.base.Optional;
import com.google.common.io.FileBackedOutputStream;
import com.ibm.icu.text.MessageFormat;

/**
 * The regression test will compare the HTML output created for some given
 * public documents with reference documents saved in src/test/resources. It
 * will use {@link XMLUnit} to compare the generated and the provided documents
 * while {@linkplain XMLUnit#setIgnoreAttributeOrder(boolean) ignoring attribute
 * order}, {@linkplain XMLUnit#setIgnoreComments(boolean) ignoring comments},
 * {@linkplain XMLUnit#setIgnoreDiffBetweenTextAndCDATA(boolean) ignoring the
 * difference between CDATA segments and text}, and
 * {@linkplain XMLUnit#setNormalizeWhitespace(boolean) normalizing whitespace}.
 *
 * If you modified or upgraded the stylesheets and you are sure the differences are ok,
 * you can run {@code mvn -Dregression.outputdir=src/test/resources test} to generate new
 * versions of the reference documents, and check them in.
 *
 * @author Thorsten Vitt <thorsten.vitt@uni-wuerzburg.de>
 */
public class HtmlRegressionTest {

	private static final String OUTPUTDIR_PROPERTY = "regression.outputdir";
	private final ITextGridRep rep = TextGridRepProvider.getInstance();
	private final StylesheetManager stylesheets = new StylesheetManager(null,
			TextGridRepProvider.getInstance());

	protected void writeHtmlTo(final String uris, final OutputStream target, boolean embedded)
			throws WebApplicationException, IOException, ObjectNotFoundFault,
			MetadataParseFault, IoFault, ProtocolNotImplementedFault,
			AuthFault, SaxonApiException {
		final HTMLWriter htmlWriter = new HTMLWriter(rep, stylesheets, uris, null,
				false, true, embedded, null, null, null, null, null);
		htmlWriter.createResponse();
		htmlWriter.write(target);
	}

	public void compareHtml(final String uris, final String compareFilename)
	throws WebApplicationException, IOException, SAXException,
	ObjectNotFoundFault, MetadataParseFault, IoFault,
	ProtocolNotImplementedFault, AuthFault, SaxonApiException {
		compareHtml(uris, compareFilename, false);
	}

	public void compareHtml(final String uris, final String compareFilename, boolean embedded)
			throws WebApplicationException, IOException, SAXException,
			ObjectNotFoundFault, MetadataParseFault, IoFault,
			ProtocolNotImplementedFault, AuthFault, SaxonApiException {
		XMLUnit.setIgnoreAttributeOrder(true);
		XMLUnit.setIgnoreComments(true);
		XMLUnit.setIgnoreDiffBetweenTextAndCDATA(true);
		XMLUnit.setNormalizeWhitespace(true);

		final Optional<String> outputPath = Optional.<String> fromNullable(System
				.getProperty(OUTPUTDIR_PROPERTY));
		if (outputPath.isPresent()) {
			final File targetFile = new File(outputPath.get(), compareFilename);
			final FileOutputStream target = new FileOutputStream(targetFile);
			System.out.println(MessageFormat.format(
					"### Writing the current result for {0} to {1}", uris,
					targetFile));
			writeHtmlTo(uris, target, embedded);
			target.close();
			System.out.println("[[ATTACHMENT|"+targetFile.getAbsolutePath()+"]]");
		} else {
			final FileBackedOutputStream target = new FileBackedOutputStream(
					1048 * 1048);
			System.out
					.println(MessageFormat
							.format("### Comparing HTML output for {0} to reference document {1}",
									uris, compareFilename));

			writeHtmlTo(uris, target, embedded);
			target.close();

			final InputSource control = new InputSource(getClass().getClassLoader()
					.getResourceAsStream(compareFilename));
			final InputSource test = new InputSource(target.getSupplier().getInput());

			final Diff diff = new Diff(control, test);
			final DetailedDiff detailedDiff = new DetailedDiff(diff);
			Assert.assertTrue(MessageFormat.format("HTML Regression for {0} / {1}\n", uris, compareFilename) + detailedDiff.toString(), diff.identical());
		}
	}

	@Test
	public void testPoem() throws WebApplicationException, IOException, SAXException, ObjectNotFoundFault, MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, SaxonApiException {
		compareHtml("textgrid:mr47.0", "mr47.0.html", false);
	}

	@Test
	public void testDrama() throws WebApplicationException, IOException, SAXException, ObjectNotFoundFault, MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, SaxonApiException {
		compareHtml("textgrid:msdd.0", "msdd.0.html", false);
	}

	@Test
	public void testNovel() throws WebApplicationException, IOException, SAXException, ObjectNotFoundFault, MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, SaxonApiException {
		compareHtml("textgrid:mr23.0", "mr23.0.html", false);
	}

	@Test
	public void testPoems() throws WebApplicationException, IOException, SAXException, ObjectNotFoundFault, MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, SaxonApiException {
		compareHtml("textgrid:mjr3.0", "mjr3.0.html", false);
	}
	
	@Test
	public void testEmbedded() throws WebApplicationException, IOException, SAXException, ObjectNotFoundFault, MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, SaxonApiException {
		compareHtml("textgrid:mjr3.0", "mjr3.0-embedded.html", true);
	}

	@Test
	public void testBiography() throws WebApplicationException, IOException, SAXException, ObjectNotFoundFault, MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, SaxonApiException {
		compareHtml("textgrid:nbvb.0", "nbvb.0.html", false);
	}

	@Test
	public void testDescs() throws WebApplicationException, IOException, SAXException, ObjectNotFoundFault, MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, SaxonApiException {
		compareHtml("textgrid:n2kf.0", "n2kf.0.html", false);
	}

	@Test
	public void testLetter() throws WebApplicationException, IOException, SAXException, ObjectNotFoundFault, MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, SaxonApiException {
		compareHtml("textgrid:11sqw.0", "11sqw.0.html", false);
	}

	@Ignore("Comparison issue, TODO check")
	@Test
	public void testEPoetics() throws IOException, IoFault, AuthFault, SaxonApiException, ObjectNotFoundFault, MetadataParseFault, ProtocolNotImplementedFault, SAXException {
	    compareHtml("textgrid:2shhm.0", "2shhm.0.html", false);
	}

}
