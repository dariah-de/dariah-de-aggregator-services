# DARIAH-DE Aggregator Services

The TextGrid Aggregator is the export and conversion tool for data from the TextGrid repository. While TG-crud allows you to get individual objects and their metadata in their original format, this tool allows you to collect a whole bunch of resources in one go (e.g., by walking down aggregation trees, hence the name) and convert them to popular output formats, e.g., HTML.

For the time being the agregator service is only used for TextGrid Lab and Repositiory, it could also be adapted to the use within the DARIAH-DE Repository for aggragating and delivering things such as all objects of one collection from the index pages, and more.
