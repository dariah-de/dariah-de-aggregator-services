HTML export
-----------

Transforms TEI based content to (X)HTML. This is quite fast, it is used for the text representations on <https://textgridrep.de/>.

Synopsis::

/html/{object}?stylesheet&pi&refreshStylesheet&sid&embedded&css&linkPattern&mediatype&id&simulate

resource-wide template parameters


+--------------------------+--------------------------+--------------------------+
| parameter                | value                    | description              |
+==========================+==========================+==========================+
| **object**               | string                   | The TextGrid URIs of the |
|                          |                          | TEI document(s) or       |
|                          |                          | aggregation(s) to        |
|                          |                          | transform, separated by  |
|                          |                          | commas                   |
+--------------------------+--------------------------+--------------------------+


Customizing output to suit your web application
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By default, we'll deliver a standalone XHTML document. However, you'll often want to embed the aggregator's output in your web application. The following options will help you with that:

+--------------------------+--------------------------+--------------------------+
| parameter                | value                    | description              |
+==========================+==========================+==========================+
| **embedded**             | boolean                  | If true, an HTML         |
|                          |                          | fragment consisting of a |
|                          |                          | <div class='body'>       |
|                          |                          | element containing the   |
|                          |                          | contents of the HTML     |
|                          |                          | <body> will be returned, |
|                          |                          | ready to be embedded in  |
|                          |                          | an existing HTML page    |
+--------------------------+--------------------------+--------------------------+
| **css**                  | *` <>`__*                | URL of the CSS that      |
|                          |                          | should be referenced in  |
|                          |                          | the HTML that is created |
+--------------------------+--------------------------+--------------------------+
| **linkPattern**          | string                   | URL pattern for links.   |
|                          |                          | @URI@ will be replaced   |
|                          |                          | with the textgrid: URI.  |
+--------------------------+--------------------------+--------------------------+
| **mediatype**            | string                   | The requested content    |
|                          |                          | type. E.g., text/html or |
|                          |                          | text/xml                 |
+--------------------------+--------------------------+--------------------------+
| **id**                   | string                   | An XML ID. If given,     |
|                          |                          | only this element will   |
|                          |                          | be transformed.          |
+--------------------------+--------------------------+--------------------------+
| **simulate**             | boolean                  | If true, a full webpage  |
|                          |                          | that looks similar to    |
|                          |                          | textgridrep.de's browse  |
|                          |                          | view will be returned    |
|                          | Default: ``false``       |                          |
+--------------------------+--------------------------+--------------------------+

Links and graphics in the transformed text
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

TEI documents in TextGrid will often contain links using TextGrid URIs. As these are not resolved by browsers, the aggregator will try to rewrite links in a way that is useful.

When using the HTML transformation for display embedded in a web application, you'll want to pass in a *linkPattern* query parameter. That is an URL to a page of your web page that will display the HTML view of the document with the given TextGrid URI. E.g., on <https://textgridrep.de/> we use calls like the following:

    https://textgridlab.org/1.0/aggregator/html/textgrid:vqmz.0?embedded=true&mediatype=text/xml&linkPattern=browse.html%3Fid%3D%40URI%40%26fragment%3D%40ID%40

(The decoded value of the linkPattern is ``browse.html?id=@URI@&fragment=@ID@``)

The default stylesheet will use this by rewriting a link target like ``textgrid:4711#someid`` to ``browse.html?id=textgrid:4711&fragment=someid``. Embedded graphics using textgrid: URIs will currently be rewritten to directly refer to a TG-crud URI.


Custom style in TEI files
^^^^^^^^^^^^^^^^^^^^^^^^^

There are some ways to specify how something is rendered. Although all this actually describes how stuff is rendered in the transcribed original, the HTML transformations will try to make use of this for the output. With the default stylesheets, users can expect the following:

* values of TEI ``rend`` attributes will be included in the HTML ``class``
  attribute literally. In the CSS used on textgridrep.de, we have a few rules
  for common stuff like ``italic`` or ``spaced``.

* values of TEI ``style`` attributes are expected to be CSS and are literally
  copied to HTML ``style`` attributes.

* You can use TEI's ``rendition`` mechanism to formally specify custom styling
  for your document. See `Feature Request #2
  <https://gitlab.gwdg.de/dariah-de/dariah-de-aggragator-services/-/issues/2>`_, and especially the files attached
  there, for an example on how this can be used in your TEI document.

This stuff, and also some built-in rules to deal with, e.g., run-in headers,
creates custom ``<style>`` elements in the generated HTML header. As the header
will be stripped in ``embedded`` and ``simulate`` mode, the stylesheets move
these ``<style>`` elements to the body in these cases as scoped CSS
definitions.

Dealing with alternative stylesheets
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By default, we will use the TEI consortium's stylesheets customized with some TextGrid specific rules. However, there are some options to use a different XSLT. However, some of the features below may not be available with alternate XSLTs.

Here are the query parameters for that:

+--------------------------+--------------------------+--------------------------+
| parameter                | value                    | description              |
+==========================+==========================+==========================+
| **stylesheet**           | uri                      | If given, an alternative |
|                          |                          | XSLT stylesheet to use.  |
|                          |                          | Must be a textgrid URI.  |
+--------------------------+--------------------------+--------------------------+
| **pi**                   | boolean                  | If true, check for an    |
|                          |                          | <?xsl-stylesheet?>       |
|                          |                          | processing instruction   |
|                          |                          | in the document to       |
|                          |                          | render. Only textgrid:   |
|                          |                          | URIs will be resolved.   |
+--------------------------+--------------------------+--------------------------+
| **refreshStylesheet**    | boolean                  | If true and a stylesheet |
|                          |                          | has been given, force    |
|                          |                          | reloading the            |
|                          |                          | stylesheet, do not cache |
+--------------------------+--------------------------+--------------------------+


Custom stylesheets will generally be able to resolve ``textgrid:`` URIs using functions like `document()`. They should expect to receive some parameters:

- *linkURLPattern* is a string like, e.g., `https://textgridrep.de/browse.html?id=@URI@&fragment=@ID@`. See previous section for details.

- *graphicsURLPattern* is a string like linkURLPattern but used for URIs to graphics that should be rendered embedded in the HTML. I.e., the expanded pattern will typically land in a ``<img src="..." />`` element.

- *embedded* is a boolean that will be true if the resulting HTML document is intended for embedding, i.e. the HTML header will be stripped.

- *cssFile* might be the URL of a CSS file to link to in the generated HTML.

- *ID* might be an ``xml:id``. If this is passed, your stylesheet should *only* render the XML fragment with the given ID.


Embedded mode — HTML rendering at textgridrep.de
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

textgridrep.de uses the aggregator to render TEI files as HTML. It will activate ``embedded=true``, and it will embed resulting XHTML fragment into its own HTML. If you use your own XSLT, this means, in addition to the notes for custom XSLT above:

* Your stylesheet must return valid XHTML.
* Embedded ``<style>`` elements from the HTML header will be moved to the body and converted to a `scoped style element <https://davidwalsh.name/scoped-css>`_.
* The ``<body>`` element will be converted to a ``<div class="body">`` and returned to be embedded into the container HTML. Everything else your XSLT delivers, especially the HTML ``<head>``, is thrown away. Thus, if you load any JavaScript, make sure to load it from ``<script>`` tags in the body.

You will of course need to pay attention to the usual web development issues, e.g., make sure that any resource you load from within the HTML can be loaded from within pages that are served at <https://textgridrep.de/>.

You can use the simulate mode to partially simulate embedding; it will run the same embed code, but instead of returning just an XHTML fragment it embeds the fragment into a full HTML frame that resembles the TextGridRep.de design. This method can also be used even for unpublished data from within the TextGridLab using the TextGridRep Preview tool that is available from the Marketplace.


Generating a Table of Contents
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You can also only generate a table of contents instead of a full HTML document.

+--------------------------+--------------------------+---------------------------+
| parameter                | value                    | description               |
+==========================+==========================+===========================+
| **toc**                  | string                   | If given, generate a table|
|                          |                          | of contents instead of a  |
|                          |                          | full HTML document        |
+--------------------------+--------------------------+---------------------------+

Currently, the value of the ``toc`` argument is ignored. If it is present with any value, an XHTML *fragment* will be generated that contains a table of contents for the current document as a list of local links. Example::

  <?xml version="1.0" encoding="UTF-8"?>
  <ul xmlns="http://www.w3.org/1999/xhtml" class="toc">
    <li>
      <a href="#tg4.jmzc.0">Auf das schöne Geschlecht</a>
      <ul/>
    </li>
    <li>
      <a href="#tg5.jmxf.0">Auf Amorn</a>
      <ul/>
    </li>
    <!-- You see the point ... -->
    <li>
      <a href="#tg19.jmxp.0">[Venus, die die Göttinnen beherrschet]</a>
      <ul/>
    </li>
  </ul>

There will probably more options, and the HTML will probably be a little tidied up (empty ``<ul/>`` removed etc.).

Other options
^^^^^^^^^^^^^

+--------------------------+--------------------------+--------------------------+
| parameter                | value                    | description              |
+==========================+==========================+==========================+
| **sid**                  | string                   | Session ID to access     |
|                          |                          | protected resources      |
|                          |                          |                          |
+--------------------------+--------------------------+--------------------------+
