---
API
---

The Aggregator is a service to export and convert TextGrid documents. It is able to recursively process collections, editions, and other TextGrid aggregations (hence the name).

API overview
============

Currently, all available methods are simple ``HTTP GET`` requests that follow the pattern ``format/uris?options``, where ``format`` is the output format (all available formats are given below), ``uris`` is the TextGrid URI or a list of TextGrid URIs, separated by commas.

Any of the given URIs that refer to an aggregations of any kind will be followed, i.e., if you want an HTML that contains Goethe's *West-östlicher Divan* as a whole, use <http://textgridrep.de/services/aggregator/epub/textgrid:11fb8.0>. 

Available Converters
====================

.. toctree::
   html
   zip
   teicorpus
   epub

The PDF converter is currently out of order.

Informational Endpoints
-----------------------

Additionally to the converters, there are three more endpoints:

* `help <https://textgridlab.org/1.0/aggregator/help>`_ delivers a single-page but up-to-date reference documentation
* `?_wadl <https://textgridlab.org/1.0/aggregator?_wadl>`_ delivers the same info as a WADL document
* `version <https://textgridlab.org/1.0/aggregator/version>`_ delivers a HTML document with some version, config and status info.

Return codes
============

On a successful operation, you can expect an ``200 OK`` result that directly delivers the content. You will typically *not* get a ``Content-Length`` header since we try to start streaming you the result while still building it.

Following errors can typically be expected:

- 404 if one of the given resources has not been found

- 400 on broken metadata or if you try to export an object with a method that
  does not support this kind of object (e.g., call html with an image)

- 403 for any kind of permission problems. If you get this, you'll typically
  want to pass a ``?sid=`` parameter with a session ID for a user that has
  access. We *do not* support HTTP Authentication.

- 500 for processing errors. These will often be parser or stylesheet errors,
  e.g., if you call a transformation on a non-supported or non-wellformed XML
  document, see the error page for details.

All errors come with an HTML formatted error page that contains more details on the error that actually occured.


Up-to-date help
===============

At <https://textgridlab.org/1.0/aggregator/help>  you can find a short up-to-date API overview that is generated from the code that is actually deployed, refer to this to see which options are actually available on the deployed version. You can also have a look at the WADL (from which the help is generated) at <https://textgridlab.org/1.0/aggregator?_wadl>.


XSLT Stylesheets
================

Starting with version 1.4.6, the aggregator also exposes the XSLT stylesheets that
are used internally. You can use these using an external XSLT processor – e.g., using the TextGridLab XML editor's `Preview Page <https://dev2.dariah.eu/wiki/display/TextGrid/Preview>`_.

Here are probably the most interesting endpoints:

* `stylesheets/preview.xsl` creates a simulated view of the display on TextGridRep, like the ``?simulate=true`` option to the HTML converter.
* `stylesheets/tohtml.xsl` creates the HTML representation as generated by the HTML exporter.

All stylesheets require an XSLT 2.0 processor, and of course they don't run down aggregation trees but expect a TEI document as input.
