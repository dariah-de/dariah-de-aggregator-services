.. aggregator documentation master file, created by
   sphinx-quickstart on Thu May 21 17:59:43 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

TextGrid Aggregator
===================

The TextGrid Aggregator is the export and conversion tool for data from the TextGrid repository. While TG-crud allows you to get individual objects and their metadata in their original format, this tool allows you to collect a whole bunch of resources in one go (e.g., by walking down aggregation trees, hence the name) and convert them to popular output formats, e.g., HTML.

.. toctree::
   :maxdepth: 2

   api
   config


Up-To-Date short usage
----------------------
https://textgridlab.org/1.0/aggregator/help


Sources
-------
See aggregator_sources_


Bugtracking
-----------
See aggregator_bugtracking_


License
-------
See LICENCE_


.. _LICENCE: https://gitlab.gwdg.de/dariah-de/dariah-de-aggragator-services/-/blob/main/LICENSE.txt
.. _aggregator_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-aggragator-services/-/tree/main
.. _aggregator_bugtracking: https://gitlab.gwdg.de/dariah-de/dariah-de-aggragator-services/-/issues
