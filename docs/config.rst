Installing and Configuring the Service
======================================

..todo::
  via puppet. 


ConfServ and Configuration File
-------------------------------

The service can be configured using a configuration file that is looked for in the following places:

* ``/WEB-INF/aggregator.properties`` (i.e. built into the service)
* ``file:${user.home}/.aggregator.properties``
* ``file:/etc/textgrid/aggregator/aggregator.properties``

The last one wins. The configuration file is basically simply used to set up
the ConfServ endpoint from which the actual service endpoints are read. You can
also use it to override ConfServ values, e.g., if you would like to bypass
a proxy server.

Here is the default configuration file::

  # The final endpoint that should be displayed in the generated WADL
  aggregator.endpoint.published=https://textgridlab.org/1.0/aggregator

  # The confserv endpoint for the default endpoint, /aggregator
  aggregator.textgridrep.default=https://textgridlab.org/1.0/confserv

  # The confserv endpoint for the develop endpoint, /aggregator/dev
  aggregator.textgridrep.dev=https://textgridlab.org/dev/confserv

  # You can override the crud endpoint that is used in, eg., <img src="..."/>
  # aggregator.tgcrud.links=https://textgridlab.org/1.0/tgcrud/rest

  # You can override confserv entries by providing them here and prefixing them
  # with 'aggregator.rep.', e.g.,:
  # aggregator.rep.tgcrud=https://textgridlab.org/1.0/tgcrud/TGCrudService


The service will, by default, call out for the configured ConfServ instance and get its service endpoints from there. You can override confserv settings by prefixing it with ``aggregator.rep.`` and adding it to the configuration file. 


Please note that the service fetches the configuration once, when it is first used, and cache it. It will also instantiate service clients on first use, and compile and cache stylesheets. So (a) expect the first calls to be a little slow and (b) restart the service when you change configuration.
