EPUB export
-----------

Synopsis::

  /epub/{object}?stylesheet&title&sid

Converts the given TEI object or the aggregation of TEI objects to an E-Book in EPUB format.

When handled an aggregation or multiple TEI documents, the exporter will walk down the aggregation graph and build a TEIcorpus document from it which will then be transformed to EPUB (EPUB 2) using a customized version of the TEI stylesheets by Sebastian Rahtz. 

Depending on the number of documents to actually deliver (= size of aggregation graph) the tool may take some time before it actually starts delivering content.


+--------------------------+--------------------------+--------------------------+
| parameter                | value                    | description              |
+==========================+==========================+==========================+
| **object**               | string                   | The TextGrid URI(s) of   |
|                          |                          | the object(s) to         |
|                          |                          | convert, separated by    |
|                          |                          | commas. Should be either |
|                          |                          | TEI objects or           |
|                          |                          | aggregations of TEI (and |
|                          |                          | maybe other) objects     |
+--------------------------+--------------------------+--------------------------+


request query parameters
^^^^^^^^^^^^^^^^^^^^^^^^

+--------------------------+--------------------------+--------------------------+
| parameter                | value                    | description              |
+==========================+==========================+==========================+
| **stylesheet**           | URI                      | URL of an alternative    |
|                          |                          | stylesheet to use. Must  |
|                          |                          | be compatible.           |
+--------------------------+--------------------------+--------------------------+
| **title**                | string                   | Title if multiple root   |
|                          |                          | objects given            |
|                          |                          |                          |
+--------------------------+--------------------------+--------------------------+
| **sid**                  | string                   | Session ID for accessing |
|                          |                          | protected objects        |
|                          |                          |                          |
+--------------------------+--------------------------+--------------------------+

*available response representations:*

-  `application/epub+zip <#d43324e23>`__


