Goals
=====

Basically, we would like to have an interface as follows:

* the REST interface should live in a small single class that then instantiates
  the workers, so we could also setup a SOAP interface if we want
* most of the argument handling etc. should be the same for all types of
  export, so reuse it
* usually, request processing contains of the following steps:

	1. argument parsing & (offline) validation
	2. argument validation (online), e.g. check whether referred objects
	   actually exist. Usually this involves fetching at least one object's
	   metadata in order to determine its format & potentially title for
	   the results.
	3. Result header generation
	4. Result body generation.

    The first three steps should be as fast as possible, i.e. as few repository
    interaction as possible, so we can reply fast. The fourth step will usually
    be deferred to the time data is streamed (StreamingOutput's write method)

* There are typical recurring tasks:

	* caching of stylesheets
	* calculating of Cache headers
	* processing trees of documents
	* generating filenames both for the result & for parts of it
	* generating of TEIcorpus documents
	* rewriting links
	*
