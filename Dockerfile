FROM maven:3-jdk-8 as builder

COPY . /build
WORKDIR /build
# clone the tei stylesheets
RUN git submodule update --recursive --init
RUN mvn package -Dmaven.test.skip=true -P \!textgrid.deb


FROM tomcat:9-jdk8

# latex for PDF generation (TODO: test if PDF generation is really working, otherwise safe that space)
RUN apt-get update -y && apt-get upgrade -y && apt-get install --no-install-recommends -y \
    #texlive-xetex \
    texlive-latex-base \
    texlive-latex-extra \
    texlive-plain-generic \
    # save some space
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /build/target/aggregator-*.war /usr/local/tomcat/webapps/aggregator.war

