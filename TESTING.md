% Testing the Aggregator
% Thorsten Vitt
%


Automatic Tests
===============

Unit Tests
----------

Unit tests are run against individual classes in the source directory.
The complete service will *not* be set up.

Test classes are placed under `src/test` and named `*Test.java`. Tests will be
run using the surefire plugin when you run maven to any of the interesting
phases and can be skipped by passing the `-DskipTests` parameter.

Integration Tests
-----------------

Integration tests are tests against a fully setup service. A local
tomcat running the service will be set up, and the tests will be run in
a separate application. The system property `service.url` will contain
the root URL of the aggregator service to run.

Integration tests are executed using the failsafe plugin by running `mvn
verify`. The tests live in files called `*IT.java` under `src/test`.

Integration Tests with Authentication
-------------------------------------

These tests work like normal integration tests, but additionally they are
passed a session ID that is required to access non-public TextGrid resources in
the `sid` system property.

Implementors call the files containing the tests `*AT.java` (for
*a*uthenticated *t*ests) and place them under `src/test`. To test,

1.  [Get a session ID](https://textgridlab.org/1.0/WebAuthN/WebAuthN2.php?authZinstance=textgrid-ws3.sub.uni-goettingen.de) for the user *tgtest*
2.  run maven using `mvn -Dsid=<session ID> verify`

Interactive Testing
===================

You can run a local copy of the service using `mvn tomcat:run-war`. The
service will then be available at <http://localhost:13000/aggregator>
for you to test interactively.
